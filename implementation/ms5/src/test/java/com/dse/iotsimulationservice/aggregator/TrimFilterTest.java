package com.dse.iotsimulationservice.aggregator;

import com.dse.messagequeuemodel.AggregableValue;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class TrimFilterTest {

    private List<AggregableValue> testValues;

    private TrimFilter filter = new TrimFilter(1);

    @Test
    void itShouldFilterValues() {
        testValues = TestFixtures.getAggregableList();
        List<AggregableValue> result = filter.filterValues(testValues);
        assertThat(result)
                .isNotNull()
                .hasSize(3);
        assertThat(result.get(0)).extracting("value1", "value2", "value3").containsOnlyNulls();
        assertThat(result.get(2)).extracting("value1", "value2", "value3").containsOnlyNulls();
    }

    @Test
    void itShouldThrowExceptionWhenNoMinMaxFound() {
        testValues = TestFixtures.getAggregableListWithIdenticalNullField();
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> filter.filterValues(testValues));
    }

    @Test
    void itShouldThrowExceptionOnNullObjectInList() {
        testValues = TestFixtures.getAggregableListWithOnlyNullInstances();
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> filter.filterValues(testValues));
    }

    @Test
    void itShouldIgnoreNullValues(){
        testValues = TestFixtures.getAggregableListWithNull();
        List<AggregableValue> result = filter.filterValues(testValues);
        assertThat(result)
                .isNotNull()
                .hasSize(3);
        //all values have to be null, bc. value1 is null per input and value2 and value3 are min and max
        assertThat(result).flatExtracting("value1", "value2", "value3").containsOnlyNulls();

    }

    @Test
    void itShouldBreakOnTooShortList() {
        testValues = TestFixtures.getAggregableList();
        testValues.remove(0);
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> filter.filterValues(testValues));

    }

}
