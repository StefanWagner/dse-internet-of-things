package com.dse.resultoutputservice.network.converter;

import com.dse.messagequeuemodel.AirSensorPredictionDataRecord;
import com.dse.messagequeuemodel.AnomalyDataRecord;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.WaterSensorPredictionDataRecord;
import com.dse.resultoutputservice.datatypes.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

/**
 * The Converter class is responsible for converting message objects from the message queue and for converting resultSet objects from the database.
 */
public class Converter {
    /**
     * The messageToAnomaly method converts a message object with an anomalyDataRecord to an anomaly object.
     *
     * @param message object which should be converted
     * @return an anomaly object
     */
    public static Anomaly messageToAnomaly(Message message) {
        return new Anomaly(message.getTimestamp(),
                ((AnomalyDataRecord) message.getRecord()).getErrorType().getType(),
                ((AnomalyDataRecord) message.getRecord()).getErrorDescription());
    }

    /**
     * The messageToAirPrediction method converts a message object with a airSensorPredictionDataRecord to an airPrediction object.
     *
     * @param message object which should be converted
     * @return an airPrediction object
     */
    public static AirPrediction messageToAirPrediction(Message message) {
        return new AirPrediction(message.getTimestamp(),
                ((AirSensorPredictionDataRecord) message.getRecord()).getAirTemperature().doubleValue(),
                ((AirSensorPredictionDataRecord) message.getRecord()).getWindSpeed().doubleValue(),
                ((AirSensorPredictionDataRecord) message.getRecord()).getHumidity().doubleValue());
    }

    /**
     * The messageToWaterPrediction method converts a message object with a waterSensorPredictionDataRecord to an waterPrediction object.
     *
     * @param message object which should be converted
     * @return an waterPrediction object
     */
    public static WaterPrediction messageToWaterPrediction(Message message) {
        return new WaterPrediction(message.getTimestamp(),
                ((WaterSensorPredictionDataRecord) message.getRecord()).getWaterTemperature().doubleValue(),
                ((WaterSensorPredictionDataRecord) message.getRecord()).getTurbidity().doubleValue(),
                ((WaterSensorPredictionDataRecord) message.getRecord()).getWaveHeight().doubleValue());
    }

    /**
     * The databaseResultSetToAnomalies method converts a resultSet object from the database to an anomalies object.
     *
     * @param resultSet object which should be converted
     * @return an anomalies object
     */
    public static Anomalies databaseResultSetToAnomalies(ResultSet resultSet) {
        Anomalies anomalies = new Anomalies();
        try {
            while (resultSet.next()) {
                anomalies.addAnomaly(new Anomaly(Instant.parse(resultSet.getString("timestamp")),
                        resultSet.getString("errorType"),
                        resultSet.getString("errorDescription")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return anomalies;
    }

    /**
     * The databaseResultSetToAirPredictions method converts a resultSet object from the database to an airPredictions object.
     *
     * @param resultSet object which should be converted
     * @return an airPredictions object
     */
    public static AirPredictions databaseResultSetToAirPredictions(ResultSet resultSet) {
        AirPredictions airPredictions = new AirPredictions();
        try {
            while (resultSet.next()) {
                airPredictions.addAirPrediction(new AirPrediction(Instant.parse(resultSet.getString("timestamp")),
                        resultSet.getDouble("airTemperature"),
                        resultSet.getDouble("windSpeed"),
                        resultSet.getDouble("humidity")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return airPredictions;
    }

    /**
     * The databaseResultSetToWaterPredictions method converts a resultSet object from the database to an waterPredictions object.
     *
     * @param resultSet object which should be converted
     * @return an waterPredictions object
     */
    public static WaterPredictions databaseResultSetToWaterPredictions(ResultSet resultSet) {
        WaterPredictions waterPredictions = new WaterPredictions();
        try {
            while (resultSet.next()) {
                waterPredictions.addWaterPrediction(new WaterPrediction(Instant.parse(resultSet.getString("timestamp")),
                        resultSet.getDouble("waterTemperature"),
                        resultSet.getDouble("turbidity"),
                        resultSet.getDouble("waveHeight")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return waterPredictions;
    }

}
