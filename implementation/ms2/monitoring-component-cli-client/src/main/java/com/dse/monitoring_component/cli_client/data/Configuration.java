package com.dse.monitoring_component.cli_client.data;

public class Configuration {
    public static final String defaultMonitoringComponentURL = "http://127.0.0.1:5050";
    public static final int defaultRefreshRate = 2000;
    public static final int defaultScreenLength = 70;
}
