package com.dse.messagequeuemonitoringcomponent.managers;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.*;
import com.dse.messagequeuemonitoringcomponent.configuration.ApplicationConfiguration;
import com.dse.messagequeuemonitoringcomponent.exception.InvalidInputException;
import com.dse.messagequeuemonitoringcomponent.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Manages initialisation(topic subscription) and self termination (topic unsubscription)
 * Also sends termination messages to the message queue
 */
@Service
public class ApplicationManager {
    private static final Logger log = LoggerFactory.getLogger(ApplicationManager.class);
    private final String clientUrl;
    private final MessageQueueClient messageQueueClient;
    private DataManager dataManager;


    public ApplicationManager(
            @Value("${messagequeue.client.messagequeueurl}") String messageQueueUrl,
            @Value("${messagequeue.client.clienturl}") String clientUrl,
            @Value("${messagequeue.port}") int messageQueuePort,
            @Value("${server.port}") int clientPort,
            DataManager dataManager) {

        //check if optional ip addresses are set and use them instead
        if (!ApplicationConfiguration.messageQueueIPAddress.isEmpty()
                && !ApplicationConfiguration.clientIPAddress.isEmpty()) {

            messageQueueUrl = "http://" + ApplicationConfiguration.messageQueueIPAddress + ":" + messageQueuePort;
            clientUrl = "http://" + ApplicationConfiguration.clientIPAddress + ":" + clientPort;
        }

        this.messageQueueClient = new MessageQueueClient(clientUrl, messageQueueUrl);
        this.clientUrl = clientUrl;
        this.dataManager = dataManager;
    }

    @PostConstruct
    private void initialize() {
        new Thread(this::connectToMessageQueue).start();
    }

    /**
     * Tries to connect to the message queue to subscribe the needed Topics.
     * If the connection fails, the application waits the configured time and tries again until
     * the subscription was successful or the maximum amount of retries is reached
     */
    private void connectToMessageQueue() {
        boolean connected = false;
        int retryInterval = ApplicationConfiguration.CONNECTION_RETRY_INTERVAL;
        Set<TopicEnum> topicList = ApplicationConfiguration.TOPIC_LIST;

        while (!connected) {
            try {
                Thread.sleep(retryInterval);
                log.info("trying to connect to message queue");
                messageQueueClient.subscribeToTopics(topicList);
                dataManager.setConnected(true);
                connected = true;

                log.info("connection successful");

            } catch (InterruptedException e) {
                log.error("Thread was interrupted: " + e.getMessage(), e);
                System.exit(1);

            } catch (Exception e) {
                log.warn("Could not connect to the message queue: " + e.getMessage());
            }
        }


    }

    /**
     * Makes one attempt to unsubscribe all subscribed topics
     */
    @PreDestroy
    private void unsubscribeAllTopics() {
        try {
            messageQueueClient.unsubscribeFromTopics(ApplicationConfiguration.TOPIC_LIST);
            log.info("Unsubscribing successful!");
            for (Subscriber subscriber: dataManager.getServices().keySet()) {
                if (!subscriber.getUrl().equals(clientUrl)) {
                    SubscriberMeta data = new SubscriberMeta(subscriber, SubscriberMeta.Status.CREATED);
                    messageQueueClient.publishMessage(new Message(TopicEnum.METADATA, data, PriorityEnum.MEDIUM));
                }
            }
            log.info("Service messages published");
        } catch (RestClientException e) {
            //if mq was never connected this error will always occur
            if (dataManager.isConnectedToMQ()) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     *
     */
    public synchronized void increaseDelayMessageQueue() {
        if (dataManager.isConnectedToMQ()) {
            try {
                int delay = 0;
                if (messageQueueClient.getSubscriber().isPresent()){
                    delay = messageQueueClient.getSubscriber().get().getDelayInMillis();
                }
                log.debug("increasing delay to" + (delay + 50));


                int finalDelay = delay;
                new Thread(() -> messageQueueClient.delayMessages(finalDelay +50)).start();
                log.info("Message queue delay increased, is {}ms", delay+50);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Delays the message queue by given millis
     * Sleeps until the connection is fully established:
     * when this service subscribes, the message queue
     */
    public synchronized void decreaseDelayMessageQueue() {
        int delay = 0;
        if (messageQueueClient.getSubscriber().isPresent()){
            delay = messageQueueClient.getSubscriber().get().getDelayInMillis();
        }
        if (delay != 0 && dataManager.isConnectedToMQ()) {
            log.debug("decreasing delay");
            try {
                int finalDelay = delay;
                new Thread(() -> messageQueueClient.delayMessages(finalDelay -50)).start();
                log.info("Message queue delay decreased, is {}ms", delay);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
    }

    /**
     * Quits the program
     */
    public void terminateSelf() {
        System.exit(0);
    }

    /**
     * Sends a termination message with given targets to the message queue
     *
     * @param serviceURLs services to terminate
     */
    public synchronized void terminateOther(String[] serviceURLs) {
        dataManager.checkMQConnection();

        if (serviceURLs == null || serviceURLs.length == 0) {
            throw new InvalidInputException("Service definition is missing");
        }

        //create set with urls to terminate
        Set<String> serviceUrlSet = new HashSet<>(Arrays.asList(serviceURLs));

        //iterate all received services
        for (String service : serviceUrlSet) {
            if (dataManager.getServicesURLs().contains(service)) {
                //if at least one exists send the termination message with the whole list
                Message terminationMessage = new Message(TopicEnum.TERMINATION, new TerminationData(serviceUrlSet), PriorityEnum.HIGH);
                messageQueueClient.publishMessage(terminationMessage);
                return;
            }
        }
        //if not at least given service exists, throw
        throw new NotFoundException("All given services already terminated");
    }

    /**
     * Sends a error message to the message queue
     * <p>
     * The error message reports an internal error (error caused by this service)
     * <p>
     * Naming this service as origin and the fist stacktrace line as cause (where the error comes from)
     * and the exception message as error message
     *
     * @param e exception to report as error to the message queue
     */
    public synchronized void reportInternalErrorToMessageQueue(Exception e) {
        ErrorData errorData = new ErrorData(clientUrl, e.getStackTrace()[0].toString(), e.getMessage());
        Message errorMessage = new Message(TopicEnum.ERRORS, errorData, PriorityEnum.HIGH);
        messageQueueClient.publishMessage(errorMessage);
    }

    /**
     * Sends a error message to the message queue
     * <p>
     * Reports an external error
     * (an error or error indication caused by a other service but recognized by this service)
     * <p>
     * Naming this service as origin but filling cause and message with given values
     *
     * @param cause        info on which service caused the error
     * @param errorMessage error message
     */
    public synchronized void reportExternalErrorToMessageQueue(String cause, String errorMessage) {
        ErrorData errorData = new ErrorData(clientUrl, cause, errorMessage);
        Message message = new Message(TopicEnum.ERRORS, errorData, PriorityEnum.HIGH);
        messageQueueClient.publishMessage(message);
    }

    public String getClientUrl() {
        return clientUrl;
    }
}
