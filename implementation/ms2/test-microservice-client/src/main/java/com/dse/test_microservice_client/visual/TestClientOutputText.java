package com.dse.test_microservice_client.visual;

import io.github.lukas_krickl.cli_client.visual.OutputText;
import io.github.lukas_krickl.cli_client.visual.TextColor;

public class TestClientOutputText implements OutputText {
    public static String getMessageQueueUrlNotConfiguredMessage() {
        return "The message queue url is not configured yet!\n" +
                "use 'config' first or try 'help' for further information'";
    }

    @Override
    public String getApplicationLogo() {
        return " _            _                                            \n" +
                "| |_ ___  ___| |_                                          \n" +
                "| __/ _ \\/ __| __|                                         \n" +
                "| ||  __/\\__ \\ |_                                          \n" +
                " \\__\\___||___/\\__|                                         \n" +
                "           _                                    _          \n" +
                " _ __ ___ (_) ___ _ __ ___  ___  ___ _ ____   _(_) ___ ___ \n" +
                "| '_ ` _ \\| |/ __| '__/ _ \\/ __|/ _ \\ '__\\ \\ / / |/ __/ _ \\\n" +
                "| | | | | | | (__| | | (_) \\__ \\  __/ |   \\ V /| | (_|  __/\n" +
                "|_| |_| |_|_|\\___|_|  \\___/|___/\\___|_|    \\_/ |_|\\___\\___|\n" +
                "                                                           \n" +
                "  ___| (_) ___ _ __ | |_                                   \n" +
                " / __| | |/ _ \\ '_ \\| __|                                  \n" +
                "| (__| | |  __/ | | | |_                                   \n" +
                " \\___|_|_|\\___|_| |_|\\__|                                  \n";
    }

    @Override
    public String getLicence() {
        return "Free to use";
    }

    @Override
    public String getVersion() {
        return "1.3";
    }

    @Override
    public String initialisationText() {
        return "Use the Command:\n\n   'config http://message-queue-url:5000' " +
                "\n\nwith the correct Message Queue URL for configuration" +
                "\nYou can change it with the same command again"
                + "\n\nUse " + TextColor.YELLOW.apply("help") + " to see further information\n";
    }

    @Override
    public String getInputPrefix() {
        return "test-client > ";
    }
}
