package com.dse.monitoring_component.cli_client.test.commands;

import com.dse.monitoring_component.cli_client.commands.StatusCommand;
import com.dse.monitoring_component.cli_client.data.DAO;
import com.dse.monitoring_component.cli_client.visual.ClientOutputText;
import io.github.lukas_krickl.cli_client.builder.application.ApplicationManagerBuilder;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.commands.CommandRepository;
import io.github.lukas_krickl.cli_client.logic.ApplicationManager;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.ReflectionUtils;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.ByteArrayInputStream;
import java.util.NoSuchElementException;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;


public class StatusCommandTest {

    @Test()
    void mcNotAvailable_runStatus_displaysErrorAndStops() {
        DAO.getInstance().getConnector().setMonitoringComponentURL("http://not-a-valid-url:123");
        StatusCommand cmd = new StatusCommand();
        assertDoesNotThrow(()->{ ReflectionTestUtils.invokeMethod(cmd, "displayStatus"); });
    }

}