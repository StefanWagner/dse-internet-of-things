package io.github.lukas_krickl.cli_client.test;

import io.github.lukas_krickl.cli_client.builder.application.ApplicationManagerBuilder;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.visual.OutputText;
import io.github.lukas_krickl.cli_client.logic.ApplicationManager;

public class CreateTestObjects {
	public static ApplicationManager createTestApp(){
		return new ApplicationManagerBuilder()
				.setupOutputText(createOutputText())
				.addCommand(createCustomTestCommand())
				.build();
	}




	public static OutputText createOutputText(){
		return new OutputText() {
			@Override
			public String getApplicationLogo() {
				return "LOGO This is a Test Application LOGO";
			}

			@Override
			public String getLicence() {
				return "LICENCE This is a Test Application LICENCE";
			}

			@Override
			public String initialisationText() {
				return "This text is displayed at at startup";
			}

			@Override
			public String getInputPrefix() {
				return "testApp >";
			}

			@Override
			public String getVersion() {
				return "1.0";
			}
		};
	}

	public static Command createCustomTestCommand(){
		return new Command(new CommandInfoBuilder("TEST COMMAND")
				.addSyntax("testCommand")
				.addParameterList().addParameter("-p", "Test command description")
				.addParameter("-show", "Test command description")
				.addParameter("--delete", "Test param description Test param description\n" +
						" Test param description Test param description")
				.finishParameterList()
				.setDescription("Test command description Test command descriptionTest command d d " +
						"d d d d d d d dd d d dd d d d d d d d d d\n" +
						"\nTest \ncommand \ndescription Test command description Test command description" +
						"Test command description Test command description Test command description")
				.build()) {

			@Override
			public void run() {
				System.out.println(getCmdInfo().getName());
				System.out.println("PARAMS: "+getParams());
				System.out.println("DATA: "+getArgs());
			}
		};
	}
}
