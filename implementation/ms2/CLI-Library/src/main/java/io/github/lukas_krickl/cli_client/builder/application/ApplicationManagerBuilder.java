package io.github.lukas_krickl.cli_client.builder.application;


import io.github.lukas_krickl.cli_client.visual.OutputText;

/**
 * A builder to first add a OutputText class and then add commands one by one and returns a Application Manager
 */
public class ApplicationManagerBuilder {
	private Runnable initialisation = null;
	private Runnable shutdown = null;

	public ApplicationManagerBuilder() {
	}

	public ApplicationManagerBuilder addInitialisationRoutine(Runnable initialisation) {
		this.initialisation = initialisation;
		return this;
	}

	public ApplicationManagerBuilder addShutdownHook(Runnable shutdown) {
		this.shutdown = shutdown;
		return this;
	}

	public CommandListBuilder setupOutputText(OutputText outputText) {
		return new CommandListBuilder(outputText, initialisation, shutdown);
	}
}
