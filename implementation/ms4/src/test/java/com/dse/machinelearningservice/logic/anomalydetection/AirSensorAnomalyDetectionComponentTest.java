package com.dse.machinelearningservice.logic.anomalydetection;

import com.dse.machinelearningservice.logic.anomalydetection.AirSensorAnomalyDetectionComponent;
import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.AirSensorPredictionDataRecord;
import com.dse.messagequeuemodel.AirSensorValue;
import com.dse.messagequeuemodel.SensorTypeEnum;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AirSensorAnomalyDetectionComponentTest {

    private AirSensorPredictionDataRecord prediction;
    private MessageQueueClient client;

    @Before
    public void setUp() {
        prediction = new AirSensorPredictionDataRecord(BigDecimal.valueOf(20),
                BigDecimal.valueOf(5), BigDecimal.valueOf(12)); //temperature, windspeed, humidity
        client = null; //setting the message queue client to null --> no messages will be sent, only local testing
    }

    //Null value anomalies

    @Test
    public void testNullValueAnomalyWithCorrectInput() {
        AirSensorValue record = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "WeatherStation1",
                BigDecimal.valueOf(20), BigDecimal.valueOf(5), BigDecimal.valueOf(12));

        AirSensorAnomalyDetectionComponent airSensorAnomalyDetectionComponent =
                new AirSensorAnomalyDetectionComponent();
        airSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int nullValueAnomaliesFound = airSensorAnomalyDetectionComponent.findNullValueAnomaly();
        assertEquals(nullValueAnomaliesFound, 0);
    }

    @Test
    public void testNullValueAnomalyWithNullInput() {
        AirSensorValue record = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "WeatherStation1",
                null, null, null);

        AirSensorAnomalyDetectionComponent airSensorAnomalyDetectionComponent =
                new AirSensorAnomalyDetectionComponent();
        airSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int nullValueAnomaliesFound = airSensorAnomalyDetectionComponent.findNullValueAnomaly();
        assertEquals(nullValueAnomaliesFound, 3);
    }


    //Out of bounds anomalies

    @Test
    public void testOutOfBoundsAnomalyWithCorrectInput() {
        AirSensorValue record = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "WeatherStation1",
                BigDecimal.valueOf(20), BigDecimal.valueOf(5), BigDecimal.valueOf(22));

        AirSensorAnomalyDetectionComponent airSensorAnomalyDetectionComponent =
                new AirSensorAnomalyDetectionComponent();
        airSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int outOfBoundsAnomaliesFound = airSensorAnomalyDetectionComponent.findOutOfBoundsAnomaly();
        assertEquals(outOfBoundsAnomaliesFound, 0);
    }

    @Test
    public void testOutOfBoundsAnomalyWithInvalidInput() {
        AirSensorValue record = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "WeatherStation1",
                BigDecimal.valueOf(60), BigDecimal.valueOf(-5), BigDecimal.valueOf(112));

        AirSensorAnomalyDetectionComponent airSensorAnomalyDetectionComponent =
                new AirSensorAnomalyDetectionComponent();
        airSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int outOfBoundsAnomaliesFound = airSensorAnomalyDetectionComponent.findOutOfBoundsAnomaly();
        assertEquals(outOfBoundsAnomaliesFound, 3);
    }



    //Prediction anomalies

    @Test
    public void testPredictionAnomalyWithCorrectInput() {
        AirSensorValue record = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "WeatherStation1",
                BigDecimal.valueOf(29), BigDecimal.valueOf(6), BigDecimal.valueOf(11));

        AirSensorAnomalyDetectionComponent airSensorAnomalyDetectionComponent =
                new AirSensorAnomalyDetectionComponent();
        airSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int predictionAnomaliesFound = airSensorAnomalyDetectionComponent.findPredictionAnomaly();
        assertEquals(predictionAnomaliesFound, 0);
    }

    @Test
    public void testPredictionAnomalyWithInvalidInput() {
        AirSensorValue record = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "WeatherStation1",
                BigDecimal.valueOf(31), BigDecimal.valueOf(16), BigDecimal.valueOf(1));

        AirSensorAnomalyDetectionComponent airSensorAnomalyDetectionComponent =
                new AirSensorAnomalyDetectionComponent();
        airSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int predictionAnomaliesFound = airSensorAnomalyDetectionComponent.findPredictionAnomaly();
        assertEquals(predictionAnomaliesFound, 3);
    }




}
