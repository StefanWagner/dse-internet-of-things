package io.github.lukas_krickl.cli_client.test.commands;

import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.commands.CommandParser;
import io.github.lukas_krickl.cli_client.logic.Configuration;
import io.github.lukas_krickl.cli_client.test.CreateTestObjects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

class CommandParserTest {
    private CommandParser parser;

    @BeforeEach
    void setUp() {
        TreeSet<Command> cmdList = new TreeSet<>(Arrays.asList(Configuration.GENERAL_COMMANDS));
        cmdList.add(CreateTestObjects.createCustomTestCommand());
        this.parser = new CommandParser(cmdList);
    }



    @Test
    void parseCorrectTestCMDSyntax_TestCMDReturned() {
        //test just syntax
        assertDoesNotThrow(()->{parser.parseUserInput("testCommand");});
        Command parsedCommand = parser.parseUserInput("testCommand");
        assertEquals("TEST COMMAND", parsedCommand.getCmdInfo().getName());
        assertTrue(parsedCommand.getParams().isEmpty());
        assertTrue(parsedCommand.getArgs().isEmpty());

        //test just data
        assertDoesNotThrow(()->{parser.parseUserInput("testCommand data or more data");});
        parsedCommand = parser.parseUserInput("testCommand data or more data");
        assertEquals("TEST COMMAND", parsedCommand.getCmdInfo().getName());
        assertTrue(parsedCommand.getParams().isEmpty());
        assertEquals("data or more data", parsedCommand.getArgs());

        //test single parameter
        assertDoesNotThrow(()->{parser.parseUserInput("testCommand -p");});
        parsedCommand = parser.parseUserInput("testCommand -p");
        assertEquals("TEST COMMAND", parsedCommand.getCmdInfo().getName());
        assertTrue(parsedCommand.getParams().contains("-p"));
        assertTrue(parsedCommand.getArgs().isEmpty());

        //test single parameter and data
        assertDoesNotThrow(()->{parser.parseUserInput("testCommand -p datadata");});
        parsedCommand = parser.parseUserInput("testCommand -p datadata");
        assertEquals("TEST COMMAND", parsedCommand.getCmdInfo().getName());
        assertTrue(parsedCommand.getParams().contains("-p"));
        assertEquals("datadata", parsedCommand.getArgs());

        //test single parameter and data
        assertDoesNotThrow(()->{parser.parseUserInput("testCommand -p datadata");});
        parsedCommand = parser.parseUserInput("testCommand -p   3");
        assertEquals("TEST COMMAND", parsedCommand.getCmdInfo().getName());
        assertTrue(parsedCommand.getParams().contains("-p"));
        assertEquals("3", parsedCommand.getArgs());


        //test multiple parameters
        assertDoesNotThrow(()->{parser.parseUserInput("testCommand -p -show --delete");});
        parsedCommand = parser.parseUserInput("testCommand -p -show --delete");
        assertEquals("TEST COMMAND", parsedCommand.getCmdInfo().getName());
        assertTrue(parsedCommand.getParams().contains("-p"));
        assertTrue(parsedCommand.getParams().contains("-show"));
        assertTrue(parsedCommand.getParams().contains("--delete"));
        assertTrue(parsedCommand.getArgs().isEmpty());

    }
}