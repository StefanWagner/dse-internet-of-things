package com.dse.test_microservice_client.commands;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.Message;
import com.dse.test_microservice_client.application.TestClientRepository;
import com.dse.test_microservice_client.generators.MessageGenerator;
import com.dse.test_microservice_client.visual.TestClientOutputText;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;
import org.springframework.web.client.RestClientException;


public class SendErrorsCommand extends Command {

    public SendErrorsCommand() {
        super(new CommandInfoBuilder("SEND ERRORS")
                .addSyntax("errors")
                .addSyntax("err")
                .addParameterList()
                .addParameter("-r", "Repeatedly send errors one after the other")
                .addParameter("-t", "Repeatedly sends errors to the MQ with the given timeout in milliseconds")
                .finishParameterList()
                .setDescription("Sends the given amount of error messages to the MQ with a timeout of 1000 milliseconds between the messages.\n" +
                        "e.g. 'err 20' sends 20 error messages to the MQ.")
                .setRequiresArgs(ArgsRequired.OPTIONAL)
                .build());
    }

    @Override
    public void run() {
        UserInterface ui = UserInterface.getInstance();
        int timeout = 500;
        Thread sender;

        if (!TestClientRepository.getInstance().isMQUrlConfigured()) {
            UserInterface.printSection(TestClientOutputText.getMessageQueueUrlNotConfiguredMessage());
            return;
        }

        if (getParams().contains("-r")) {
            //no data parsing needed
            sender = createSendingThread(timeout);

        } else {
            //data parsing needed
            try {
                int userInputValue = Integer.parseUnsignedInt(getArgs());

                if (getParams().contains("-t")) {
                    sender = createSendingThread(userInputValue);
                } else {
                    sender = createSendingThread(timeout, userInputValue);
                }
            } catch (NumberFormatException e) {
                UserInterface.println("Please enter a valid number!");
                return;
            }
        }

        sender.start();
        ui.getUserConfirmation();

        if (sender.isAlive()) {
            sender.interrupt();
        }
    }

    private Thread createSendingThread(int timeout, int messageCount) {
        return new Thread(() -> {
            MessageQueueClient mqConnection = TestClientRepository.getInstance().getMqConnection();
            try {
                for (int i = 1; i <= messageCount; i++) {
                    UserInterface.println("Sending Error Message "+TextColor.CYAN.apply(String.format("[%d/%d]",i,messageCount)));
                    UserInterface.println(sendMessageToMessageQueue(MessageGenerator.getRandomErrorMessage())+"\n");
                    Thread.sleep(timeout);
                }
            } catch (InterruptedException e) {
                UserInterface.printSection("Stopped sending messages to the Message Queue");
            }
        });
    }

    private Thread createSendingThread(int timeout) {
        return new Thread(() -> {

            try {
                while (true) {
                    UserInterface.printSection(sendMessageToMessageQueue(MessageGenerator.getRandomErrorMessage()));
                    Thread.sleep(timeout);
                }
            } catch (InterruptedException e) {
                UserInterface.println("Stopped sending messages to the Message Queue");
            }
        });
    }

    private String sendMessageToMessageQueue(Message message) {
        try {
            TestClientRepository.getInstance().getMqConnection().publishMessage(message);
            return TextColor.GREEN.apply("SUCCESS: ")+ "Error message sent!";
        } catch (RestClientException e) {
            return TextColor.RED.apply("FAILED: ")+"Error code returned -  " + e.getMessage();
        } catch (Exception e) {
            return TextColor.RED.apply("FAILED: ")+"Could not send message: " + e.getMessage();
        }
    }

}
