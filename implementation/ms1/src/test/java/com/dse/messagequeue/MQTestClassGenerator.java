package com.dse.messagequeue;

import com.dse.messagequeue.controller.MessageQueueController;
import com.dse.messagequeue.data.MessageQueueRepository;
import com.dse.messagequeue.manager.MessageManager;
import com.dse.messagequeue.manager.SubscriptionManager;
import com.dse.messagequeuemodel.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.UUID;

public class MQTestClassGenerator {

    public static MessageQueueRepository generateMessageQueueRepository() {
        SubscriptionManager subscriptionManager = new SubscriptionManager(null);
        MessageQueueRepository repository = new MessageQueueRepository(new RestTemplate(), subscriptionManager);
        ReflectionTestUtils.setField(subscriptionManager, "messageQueueRepository", repository);

        return repository;
    }

    public static SubscriptionManager generateSubscriptionManager() {
        SubscriptionManager subscriptionManager = new SubscriptionManager(null);
        MessageQueueRepository repository = new MessageQueueRepository(new RestTemplate(), subscriptionManager);
        ReflectionTestUtils.setField(subscriptionManager, "messageQueueRepository", repository);
        return subscriptionManager;
    }

    public static MessageManager generateMessageManager() {
        return new MessageManager(generateMessageQueueRepository());
    }

    public static MessageQueueController generateMessageQueueController() {
        SubscriptionManager subMan = new SubscriptionManager(null);
        MessageQueueRepository repo = new MessageQueueRepository(new RestTemplate(), subMan);
        ReflectionTestUtils.setField(subMan, "messageQueueRepository", repo);

        MessageManager msgMan = new MessageManager(repo);

        return new MessageQueueController(msgMan, subMan);
    }

    public static Subscriber getRandomSubscriber(Set<TopicEnum> topics) {
        String url = "http://random-sub-" + UUID.randomUUID().toString() + ":8080";
        return new Subscriber(url, 0, topics);
    }

    public static Subscriber getRandomSubscriber() {
        Set<TopicEnum> topics = EnumSet.of(TopicEnum.METADATA, TopicEnum.TERMINATION, TopicEnum.ERRORS);

        return getRandomSubscriber(topics);
    }

    public static Message getRandomMessage(TopicEnum topic) {
        return new Message(topic, createDataRecord(topic));
    }

    public static Message getRandomMessage(TopicEnum topic, PriorityEnum prio) {
        return new Message(topic, createDataRecord(topic), prio);
    }

    private static DataRecord createDataRecord(TopicEnum topic) {
        switch (topic) {
            case ERRORS:
                return new ErrorData(
                        "localhost:8080",
                        "Something happened",
                        "Something happend somehow"
                );
            case ANOMALY:
                return new AnomalyDataRecord(ErrorTypeEnum.NULL, "Something was null");
            case METADATA:
                return new CacheMeta(TopicEnum.METADATA, 2);
            case PREDICTION:
                return new AirSensorPredictionDataRecord(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE);
            case TERMINATION:
                return new TerminationData(Collections.emptySet());
            case AIRSENSORDATA:
                return new AirSensorValue(Instant.MIN,
                        SensorTypeEnum.AIR,
                        "Weather Station 1",
                        BigDecimal.ONE,
                        BigDecimal.ONE,
                        BigDecimal.ONE
                );
            case WATERSENSORDATA:
                return new WaterSensorValue(Instant.MIN,
                        SensorTypeEnum.WATER,
                        "Beach 1",
                        BigDecimal.ONE,
                        BigDecimal.ONE,
                        BigDecimal.ONE
                );
            default:
                return null;
        }
    }
}
