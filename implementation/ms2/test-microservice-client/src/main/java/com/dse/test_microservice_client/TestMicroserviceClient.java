package com.dse.test_microservice_client;

import com.dse.test_microservice_client.application.Initialisation;
import com.dse.test_microservice_client.commands.*;
import com.dse.test_microservice_client.visual.TestClientOutputText;
import io.github.lukas_krickl.cli_client.builder.application.ApplicationManagerBuilder;
import io.github.lukas_krickl.cli_client.logic.ApplicationManager;

/**
 * needs to be started
 */
public class TestMicroserviceClient {
    public static void main(String[] args) {
        ApplicationManager applicationManager = new ApplicationManagerBuilder()
                .addInitialisationRoutine(new Initialisation())
                .setupOutputText(new TestClientOutputText())
                .addCommand(new ConfigMQUrlCommand())
                .addCommand(new SendErrorsCommand())
                .addCommand(new SubscriberCommand())
                .addCommand(new TopicsCommand())
                .addCommand(new TerminateCommand())
                .build();

        applicationManager.start();
    }
}
