package io.github.lukas_krickl.cli_client.builder.command;

import io.github.lukas_krickl.cli_client.exceptions.InvalidCommandConfigurationException;

import java.util.Hashtable;

/**
 * A builder for a parameter list (Hashtable<String, String>)
 */
public class ParameterBuilder {
	private CommandInfoBuilder cmdInfoBuilder;
	private Hashtable<String, String> parameters;

	public ParameterBuilder(CommandInfoBuilder cmdInfo) {
		this.cmdInfoBuilder = cmdInfo;
		parameters = new Hashtable<>();
	}

	public ParameterBuilder addParameter(String parameterSyntax, String description) {
		//null and empty checks
		if (parameterSyntax == null ||
				description == null ||
				parameterSyntax.isEmpty() ||
				description.isEmpty()) {
			throw new InvalidCommandConfigurationException("Parameter syntax or description is null or empty !");
		}
		//check if parameter already exists
		if (parameters.containsKey(parameterSyntax)) {
			throw new InvalidCommandConfigurationException("Cannot add two parameters with the same Syntax!");
		}
		parameters.put(parameterSyntax, description);
		return this;
	}

	public CommandInfoBuilder finishParameterList() {
		return cmdInfoBuilder.setParameterList(parameters);
	}


}
