package com.dse.messagequeue.manager;

import com.dse.messagequeue.data.MessageQueueRepository;
import com.dse.messagequeue.exception.InvalidInputException;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Service managing functionality regarding subscriptions such as subscribing or unsubscribing or changing subscriptions
 */
@Service
public class SubscriptionManager {

    private final MessageQueueRepository messageQueueRepository;

    public SubscriptionManager(MessageQueueRepository messageQueueRepository) {
        this.messageQueueRepository = messageQueueRepository;
    }

    /**
     * Updates a subscriber by subscribing it to new topics passed as a set inside the subscriber or newly subscribing it
     * if he is not subscribed yet.
     * <p>
     * At first checks if the subscriber exists and makes an update
     * If it is a new subscriber, the subscriber is added, a queue created and run.
     * <p>
     * In any case new subscribed topics are checked and if this sub is the first to subscribe a topic it gets the
     * cache queue added.
     *
     * @param subscriber The subscriber to be updated or added with its new topics
     * @return the subscriber which was updated
     */
    public Subscriber subscribeToTopics(Subscriber subscriber) {
        InputValidator.validateSubscriber(subscriber);

        if (messageQueueRepository.subscriberExists(subscriber.getUrl())) {
            //subscriber already exists
            Subscriber storedSubscriber = messageQueueRepository.getSubscriberByUrl(subscriber.getUrl());
            if(storedSubscriber.checkIdentity(subscriber)){
                return storedSubscriber;
            }
            InputValidator.validateSubscribingTopics(subscriber, storedSubscriber);

            storedSubscriber.update(subscriber);
            MetaManager.subscriberChanged(subscriber);
        } else {
            //new subscriber has to be created
            messageQueueRepository.createSubscriber(subscriber);
            MetaManager.subscriberAdded(subscriber);
        }

        //add cached messages to queue if new subscribed topics are cached
        acquireCachedTopics(subscriber.getUrl(), subscriber.getTopics());

        return subscriber;
    }


    /**
     * Unsubscribes the given subscriber from the given topics.
     * If he would unsubscribe all his topics, delete the subscriber instead
     * <p>
     * Restores cache queues if this was the last subscriber
     *
     * @param subscriber existing subscriber
     * @param topics     topics to unsubscribe
     * @return updated subscriber
     */
    public Subscriber unsubscribeFromTopics(Subscriber subscriber, Set<TopicEnum> topics) {
        //input checks
        InputValidator.validateSubscriber(subscriber);
        InputValidator.validateUnsubscribingTopics(subscriber, topics);
        messageQueueRepository.validateSubscriberConsistency(subscriber);

        Subscriber mqSubscriber = messageQueueRepository.getSubscriberByUrl(subscriber.getUrl());

        //if the subscriber unsubscribes all his topics
        if (topics.containsAll(mqSubscriber.getTopics())) {
            //delete subscriber
            messageQueueRepository.deleteSubscriber(mqSubscriber);
            //reset the last reference to the subscriber, so it is free for garbage collection
            // (not needed but safe is safe, in case the returned object of this method is stored elsewhere)
            mqSubscriber = new Subscriber(subscriber.getUrl(), subscriber.getDelayInMillis(), new HashSet<>());
            MetaManager.subscriberDeleted(subscriber);
        } else {
            //just remove topics from subscriber´s topic list
            mqSubscriber.removeTopics(topics);
            MetaManager.subscriberChanged(mqSubscriber);
        }

        messageQueueRepository.renewCacheQueue(topics);

        return mqSubscriber;
    }

    /**
     * Updates the delay of an subscriber
     *
     * @param subscriber unchanged subscriber
     * @param delayInMillis new delay
     * @return changed subscriber
     */
    public Subscriber updateDelay(Subscriber subscriber, int delayInMillis) {
        //validation
        InputValidator.validateSubscriber(subscriber);
        InputValidator.validateChangingDelay(subscriber, delayInMillis);
        messageQueueRepository.validateSubscriberConsistency(subscriber);

        //change delay
        Subscriber mqSubscriber = messageQueueRepository.getSubscriberByUrl(subscriber.getUrl());
        mqSubscriber.setDelayInMillis(delayInMillis);
        MetaManager.subscriberChanged(mqSubscriber);
        return mqSubscriber;
    }


    /**
     * Adds all messages of cached topics to the subscribers queue
     * if he newly subscribed the topic
     *
     *
     * @param subscriberUrl url of the subscriber
     * @param subscribedTopics topics which have been subscribed
     */
    private void acquireCachedTopics(String subscriberUrl, Set<TopicEnum> subscribedTopics){
        for (TopicEnum topic : subscribedTopics) {
            if (messageQueueRepository.isCached(topic)) {
                messageQueueRepository.addCachedMessagesToQueue(subscriberUrl, topic);
                MetaManager.cacheSizeUpdated(topic, -1);
            }
        }
    }
}
