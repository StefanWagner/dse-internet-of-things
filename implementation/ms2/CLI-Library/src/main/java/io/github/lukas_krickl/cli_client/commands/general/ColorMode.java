package io.github.lukas_krickl.cli_client.commands.general;

import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.visual.TextColor;

public class ColorMode extends Command {

	public ColorMode() {
		super(new CommandInfoBuilder("COLORMODE")
				.addSyntax("colormode")
				.setDescription("Use this command with 'on' or 'off' to toggle the displayed colors.\n" +
						"e.g. 'colormode off' or 'colormode on'")
				.setRequiresArgs(ArgsRequired.REQUIRED)
				.build());
	}

	@Override
	public void run() {
		String data = getArgs().trim().toLowerCase();
		if (data.equals("on")) {
			TextColor.setColorMode(true);
		} else if (data.equals("off")) {
			TextColor.setColorMode(false);
		}
	}
}
