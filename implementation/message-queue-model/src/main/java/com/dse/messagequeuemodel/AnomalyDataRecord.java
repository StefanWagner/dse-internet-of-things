package com.dse.messagequeuemodel;

import lombok.Value;

import java.beans.ConstructorProperties;


/**
 * DataRecord implementation describing an anomaly that was detected in the processed data.
 */
@Value
public class AnomalyDataRecord implements DataRecord {
    /**
     * describes the kind of error/anomaly that was detected
     */
    private ErrorTypeEnum errorType;

    /**
     * description of the error
     */
    private String errorDescription;

    @ConstructorProperties({"errorType", "errorDescription"})
    public AnomalyDataRecord(ErrorTypeEnum errorType, String errorDescription) {
        this.errorType = errorType;
        this.errorDescription = errorDescription;
    }
}
