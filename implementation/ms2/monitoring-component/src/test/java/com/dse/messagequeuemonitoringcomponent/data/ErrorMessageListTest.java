package com.dse.messagequeuemonitoringcomponent.data;

import com.dse.messagequeuemonitoringcomponent.TestDataGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ErrorMessageListTest {
	private ErrorMessageList errors;

	private void addErrors(int size) {
		for (int i = 0; i < size; i++) {
			errors.addError(TestDataGenerator.generateErrorMessage());
		}
	}

	@BeforeEach
	void setUp() {
		errors = new ErrorMessageList();
	}

	@AfterEach
	void tearDown() {
		errors = null;
	}

	@Test
	void addErrors_errorListContainsErrors() {
		assertEquals(0, errors.getErrorCount());
		addErrors(10);
		assertEquals(10, errors.getErrorCount());
	}

	@Test
	void clearAllErrors_errorListEmpty() {
		addErrors(10);
		errors.clearErrors();
		assertEquals(0, errors.getErrorCount());
	}

	@Test
	void clearErrorsToSize_errorCountIsSize() {
		addErrors(20);
		errors.clearErrors(4);
		assertEquals(4, errors.getErrorCount());
	}


}