package com.dse.resultoutputservice.datatypes;

import java.util.ArrayDeque;

/**
 * The AirPredictions class consists out of a set of airPrediction objects.
 * It is used for bundling the database data which will be sent to the browser.
 */
public class AirPredictions {
    private ArrayDeque<AirPrediction> airPredictions;

    public AirPredictions() {
        this.airPredictions = new ArrayDeque<AirPrediction>();
    }

    public ArrayDeque<AirPrediction> getAirPredictions() {
        return airPredictions;
    }

    public void addAirPrediction(AirPrediction airPrediction) {
        airPredictions.add(airPrediction);
    }
}
