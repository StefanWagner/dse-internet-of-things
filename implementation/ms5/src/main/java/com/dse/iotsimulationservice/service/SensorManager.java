package com.dse.iotsimulationservice.service;

import com.dse.messagequeuemodel.AggregableValue;

/**
 * Responsible for adding omitted values to the repository and invoking a value publication
 * after enough values have been omitted
 */
public interface SensorManager {

    /**
     * Method called if a new value is omitted
     *
     * @param aggregableValue the value to be omitted
     */
    void valueOmitted(AggregableValue aggregableValue);
}
