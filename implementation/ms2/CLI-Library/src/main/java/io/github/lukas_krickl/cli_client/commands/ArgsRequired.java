package io.github.lukas_krickl.cli_client.commands;

public enum ArgsRequired {
    REQUIRED,
    OPTIONAL,
    NOT_REQUIRED
}
