package com.dse.messagequeuemonitoringcomponent.managers.messages;

import com.dse.messagequeuemodel.CacheMeta;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemonitoringcomponent.data.StoredCacheData;
import com.dse.messagequeuemonitoringcomponent.managers.DataManager;

/**
 * A manager class which updates the cache information stored in the datamanager
 */
public class CacheMessageManager {
    private final DataManager dataManager;

    public CacheMessageManager(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    /**
     * Handles a Cache MetaData message.
     * Assumes that the message has been validated!
     * <p>
     * Checks if the received message represents a more recent state of the
     * cacheSize of the MQ and refreshes the stored data if so.
     *
     * @param message A validated message from the MetaData topic with a CacheMeta DataRecord
     */
    public void manageCache(Message message) {
        CacheMeta data = (CacheMeta) message.getRecord();
        StoredCacheData storedCacheData = dataManager.getCacheSize().get(data.getTopic());

        if (storedCacheData.getLastUpdate().isBefore(message.getTimestamp())) {
            //if the stored cacheSize is older than the received message, refresh the stored state
            storedCacheData.refresh(data.getCacheSize(), message.getTimestamp());
        }

    }
}
