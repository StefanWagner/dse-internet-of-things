package com.dse.messagequeuemodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.time.Instant;


/**
 * Message to be published to the message queue
 */
@Value
public class Message implements Comparable<Message> {
    /**
     * describes the topic of the message
     */

    private TopicEnum topic;

    /**
     * the time when the message was created
     */
    private Instant timestamp;

    /**
     * implementation of the DataRecord interface containing the actual message contents
     */
    private DataRecord record;

    /**
     * the priority of the message
     */
    private PriorityEnum priority;

    /**
     * creates a new Message instance with the given topic and record, and a standard priority of LOW
     *
     * @param topic  the topic to be assigned
     * @param record the record to be assigned
     */
    public Message(
            TopicEnum topic,
            DataRecord record) {
        this.topic = topic;
        this.record = record;
        this.timestamp = Instant.now();
        this.priority = PriorityEnum.LOW;
    }

    /**
     * creates a new Message instance with the given topic, record, and priority
     *
     * @param topic    the topic to be assigned
     * @param record   the record to be assigned
     * @param priority the priority to be assigned
     */
    @JsonCreator
    public Message(
            @JsonProperty("topic") TopicEnum topic,
            @JsonProperty("record")DataRecord record,
            @JsonProperty("priority")PriorityEnum priority) {
        this.topic = topic;
        this.record = record;
        this.timestamp = Instant.now();
        this.priority = priority != null ? priority : PriorityEnum.LOW;
    }


    @Override
    public int compareTo(Message msg) {
        //check priority first

        //priority of this is higher --> this is greater than message
        if (this.getPriority().getValue() > msg.getPriority().getValue()) {
            return -1;
        }
        //priority of this is lower --> this is smaller than message
        else if (this.getPriority().getValue() < msg.getPriority().getValue()) {
            return 1;
        }

        //check timestamp if priority is equal
        if (this.getTimestamp().isBefore(msg.getTimestamp())) {
            return -1;
        } else if (this.getTimestamp().isAfter(msg.getTimestamp())) {
            return 1;
        }
        return 0;
    }
}
