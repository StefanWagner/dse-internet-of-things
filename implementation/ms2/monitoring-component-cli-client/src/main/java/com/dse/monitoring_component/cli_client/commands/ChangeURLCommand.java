package com.dse.monitoring_component.cli_client.commands;

import com.dse.monitoring_component.cli_client.data.DAO;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

/**
 * A command for changing the monitoring component url
 */
public class ChangeURLCommand extends Command {
    public ChangeURLCommand() {
        super(new CommandInfoBuilder("CHANGE MONITORING COMPONENT URL")
                .addSyntax("changeurl")
                .addSyntax("config")
                .addSyntax("url")
                .setDescription("Changes the URL of the Monitoring Component to the given argument\n" +
                        "Displays the currently configured URL if no argument is provided.")
                .setRequiresArgs(ArgsRequired.OPTIONAL)
                .build());
    }

    @Override
    public void run() {
        if (getArgs().isEmpty()) {
            UserInterface.printSection("Monitoring Component URL: " +
                    TextColor.BRIGHT_BLUE.apply(DAO.getInstance().getConnector().getMonitoringComponentURL()));
        } else {
            try {
                DAO.getInstance().getConnector().setMonitoringComponentURL(getArgs());
            } catch (IllegalArgumentException e) {
                UserInterface.printSection(e.getMessage());
            }
        }
    }
}
