package com.dse.messagequeue.manager;

import com.dse.messagequeuemodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A utility class to report meta information and internal errors
 */
@Service
public class MetaManager {
    private static MessageManager messageManager;

    /**
     * A Setter for the message manager
     *
     * @param msgMan applications message manager object
     */
    public static void setMessageManager(MessageManager msgMan) {
        messageManager = msgMan;
    }

    /**
     * Creates a meta message with a SubscriberMeta DataRecord and directly queues the message via the MessageManager
     *
     * @param subscriber Subscriber data to report
     */
    public static void subscriberAdded(Subscriber subscriber) {
        reportSubscriber(subscriber, SubscriberMeta.Status.CREATED);
    }

    /**
     * Creates a meta message with a SubscriberMeta DataRecord and directly queues the message via the MessageManager
     *
     * @param subscriber Subscriber data to report
     */
    public static void subscriberChanged(Subscriber subscriber) {
        reportSubscriber(subscriber, SubscriberMeta.Status.CHANGED);
    }

    /**
     * Reports, that a subscriber has been deleted, as he unsubscribed all his topics
     * Creates a message with a SubscriberMeta DataRecord and directly queues the message via the MessageManager
     *
     * @param subscriber Subscriber data to report
     */
    public static void subscriberDeleted(Subscriber subscriber) {
        reportSubscriber(subscriber, SubscriberMeta.Status.DELETED);
    }

    /**
     * Creates a meta message with a SubscriberMeta DataRecord
     *
     * @param subscriber concerning subscriber
     * @param status     new status of the subscriber
     */
    private static void reportSubscriber(Subscriber subscriber, SubscriberMeta.Status status) {
        SubscriberMeta dataRecord = new SubscriberMeta(subscriber, status);
        Message message = new Message(TopicEnum.METADATA, dataRecord, PriorityEnum.MEDIUM);

        //spread the message to the queues
        messageManager.processMessage(message);
    }


    /**
     * Reports that the caching of a topic has changed.
     * Creates a message with a CacheMeta DataRecord which holds the current state
     * and directly queues the message via the MessageManager
     *
     * @param topic     regarding topic
     * @param cacheSize actual size of the cache and -1 if it is not cached
     */
    public static void cacheSizeUpdated(TopicEnum topic, int cacheSize) {
        CacheMeta dataRecord = new CacheMeta(topic, cacheSize);
        Message message = new Message(TopicEnum.METADATA, dataRecord, PriorityEnum.LOW);

        //spread the message to the queues
        messageManager.processMessage(message);
    }

    /**
     * Reports an error with a thrown exception.
     * Creates a message with a ErrorData DataRecord and directly queues the message via the MessageManager.
     *
     * @param e exception that was thrown
     */
    public static void reportError(Exception e) {
        ErrorData dataRecord = new ErrorData("MESSAGE_QUEUE", stackTraceToString(e.getStackTrace()), e.getMessage());
        Message message = new Message(TopicEnum.ERRORS, dataRecord, PriorityEnum.HIGH);

        //spread the message to the queues
        messageManager.processMessage(message);
    }

    /**
     * Reports an error, which was not an exception.
     * Can be used to report some inconsistency found, which smells a lot like there is something wrong
     *
     * @param cause   URL / class / or information, where the inconsistency comes from
     * @param message information of the error
     */
    public static void reportError(String cause, String message) {
        ErrorData dataRecord = new ErrorData("MESSAGE QUEUE", cause, message);
        Message errorMessage = new Message(TopicEnum.ERRORS, dataRecord, PriorityEnum.HIGH);

        //spread the message to the queues
        messageManager.processMessage(errorMessage);
    }


    /**
     * Converts a stacktrace Array to the String representation(maximum 10 lines)
     *
     * @param stackTrace from exception.getStackTrace()
     * @return String representation
     */
    private static String stackTraceToString(StackTraceElement[] stackTrace) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < Math.min(10, stackTrace.length); i++) {
            sb.append(stackTrace[i].toString()).append("\n  ");
        }

        return sb.toString();
    }

}
