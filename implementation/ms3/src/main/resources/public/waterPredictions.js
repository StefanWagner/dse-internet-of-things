var waterPredictions = {
    size: 0,
    maxTimestamp: "1970-01-01T00:00:00.000Z",
    minTimestamp: "1970-01-01T00:00:00.000Z",
    waterPredictions: new Array(),

    getWaterPredictions: function() {
        waterPredictions.size = waterPredictions.size + 1;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                waterPredictions.waterPredictions = waterPredictions.waterPredictions.concat((JSON.parse(this.responseText)).waterPredictions);
                waterPredictions.waterPredictions.sort(function(a,b) {return new Date(b.timestamp) - new Date(a.timestamp)});
                waterPredictions.createTable();
                waterPredictions.maxTimestamp = waterPredictions.waterPredictions[0].timestamp;
                waterPredictions.minTimestamp = waterPredictions.waterPredictions[waterPredictions.waterPredictions.length-1].timestamp;
            }
        };
        xmlhttp.open("GET", "/getWaterPredictions?size=" + this.size +"&maxTimestamp=" + this.maxTimestamp +"&minTimestamp=" + this.minTimestamp, true);
        xmlhttp.send();
    },

    createTable: function() {
        var i;
        var table="<tr><th>Timestamp</th><th>Water Temperature</th><th>Turbidity</th><th>Wave Height</th></tr>";
        for (i = 0; i < this.waterPredictions.length; i++) {
            table += "<tr><td>" +
                    this.waterPredictions[i].timestamp +
                    "</td><td>" +
                    this.waterPredictions[i].waterTemperature +
                    "</td><td>" +
                    this.waterPredictions[i].turbidity +
                    "</td><td>" +
                    this.waterPredictions[i].waveHeight +
                    "</td></tr>";
        }
        document.getElementById("waterPredictions").innerHTML = table;
    }
}