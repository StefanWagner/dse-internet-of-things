package com.dse.messagequeueclient;

import com.dse.messagequeuemodel.TopicEnum;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestClientException;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MessageQueueClientExceptionTest {
	private MessageQueueClient mqClient;


	@Test
	void mqNotStartedSubscribeToTopicsShouldThrowException() {
		mqClient = new MessageQueueClient("http://client-url:8080", "http://message-queue-url:8080");
		Set<TopicEnum> topics = new HashSet<>();
		topics.add(TopicEnum.TERMINATION);

		assertThrows(RestClientException.class, ()->{mqClient.subscribeToTopics(topics);});
	}

	@Test
	void showErrorMessageOnNotReachable() {
		mqClient = new MessageQueueClient("http://client-url:8080", "http://message-queue-url:8080");
		Set<TopicEnum> topics = new HashSet<>();
		topics.add(TopicEnum.TERMINATION);

		try {
			mqClient.subscribeToTopics(topics);
		} catch (RestClientException e) {
			System.out.println(e.getMessage());
		}
	}


}