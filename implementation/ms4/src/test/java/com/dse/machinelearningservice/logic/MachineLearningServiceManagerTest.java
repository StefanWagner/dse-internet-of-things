package com.dse.machinelearningservice.logic;

import com.dse.messagequeuemodel.*;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;

import static org.junit.Assert.assertEquals;

public class MachineLearningServiceManagerTest {

    //cf. https://stackoverflow.com/questions/29076981/how-to-intercept-slf4j-with-logback-logging-via-a-junit-test
    // for testing logging output

    @Test
    public void testMessageProcessing() throws InterruptedException {
        // get Logback Logger
        Logger logger = (Logger) LoggerFactory.getLogger(MachineLearningServiceManager.class);

        // create and start a ListAppender
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();

        // add the appender to the logger
        logger.addAppender(listAppender);

        //setting the client to null --> only local testing, no messages are sent
        MachineLearningServiceManager manager = new MachineLearningServiceManager(null);
        manager.subscribeToTopics(); //not actually doing anything here since client is null

        //create new message
        DataRecord record = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "station1",
                BigDecimal.valueOf(5), BigDecimal.valueOf(5), BigDecimal.valueOf(5));
        Message message = new Message(TopicEnum.AIRSENSORDATA, record);

        //start processing
        manager.processMessage(message);

        //wait for 5 seconds to make sure all the asynchronous threads have finished
        Thread.sleep(5000);

        // get all the logs
        List<ILoggingEvent> logsList = listAppender.list;

        //check that the message was passed to the anomaly search
        int anomalySearches = 0;
        for (ILoggingEvent event: logsList) {
            if (event.getMessage().contains("searching for anomaly")) anomalySearches++;
        }
        assertEquals(anomalySearches, 1);
    }


    @Test
    public void testPredictionCreation() throws InterruptedException {
        // get Logback Logger
        Logger logger = (Logger) LoggerFactory.getLogger(MachineLearningServiceManager.class);

        // create and start a ListAppender
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();

        // add the appender to the logger
        logger.addAppender(listAppender);

        //setting the client to null --> only local testing, no messages are sent
        MachineLearningServiceManager manager = new MachineLearningServiceManager(null);
        manager.subscribeToTopics(); //not actually doing anything here since client is null

        //create 10 new messages
        for (int i = 0; i < 10; ++i) {
            //create new message
            DataRecord record = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "station1",
                    BigDecimal.valueOf(i), BigDecimal.valueOf(i), BigDecimal.valueOf(i));
            Message message = new Message(TopicEnum.AIRSENSORDATA, record);

            //start processing
            manager.processMessage(message);
        }

        //wait for 5 seconds to make sure all the asynchronous threads have finished
        Thread.sleep(5000);

        // get all the logs
        List<ILoggingEvent> logsList = listAppender.list;

        //there should have been 10 anomaly searches and 1 prediction creation
        int anomalySearches = 0;
        int predictionCreations = 0;
        for (ILoggingEvent event: logsList) {
            if (event.getMessage().contains("searching for anomaly")) anomalySearches++;
            if (event.getMessage().contains("calculating prediction")) predictionCreations++;
        }
        assertEquals(anomalySearches, 10);
        assertEquals(predictionCreations, 1);
    }


    @Test
    public void testSensorTypeDifferentiation() throws InterruptedException {
        // get Logback Logger
        Logger logger = (Logger) LoggerFactory.getLogger(MachineLearningServiceManager.class);

        // create and start a ListAppender
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();

        // add the appender to the logger
        logger.addAppender(listAppender);

        //setting the client to null --> only local testing, no messages are sent
        MachineLearningServiceManager manager = new MachineLearningServiceManager(null);
        manager.subscribeToTopics(); //not actually doing anything here since client is null

        //create new message
        DataRecord airRecord1 = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "station1",
                BigDecimal.valueOf(5), BigDecimal.valueOf(5), BigDecimal.valueOf(5));
        Message airMessage1 = new Message(TopicEnum.AIRSENSORDATA, airRecord1);

        //start processing
        manager.processMessage(airMessage1);

        //create new message
        DataRecord waterRecord = new WaterSensorValue(Instant.now(), SensorTypeEnum.WATER, "beach1",
                BigDecimal.valueOf(5), BigDecimal.valueOf(5), BigDecimal.valueOf(5));
        Message waterMessage = new Message(TopicEnum.WATERSENSORDATA, waterRecord);

        //start processing
        manager.processMessage(waterMessage);

        //create new message
        DataRecord airRecord2 = new AirSensorValue(Instant.now(), SensorTypeEnum.AIR, "station2",
                BigDecimal.valueOf(5), BigDecimal.valueOf(5), BigDecimal.valueOf(5));
        Message airMessage2 = new Message(TopicEnum.AIRSENSORDATA, airRecord2);

        //start processing
        manager.processMessage(airMessage2);

        //wait for 5 seconds to make sure all the asynchronous threads have finished
        Thread.sleep(5000);

        // get all the logs
        List<ILoggingEvent> logsList = listAppender.list;

        //check that the message was passed to the anomaly search
        int airMessages = 0;
        int waterMessages = 0;
        for (ILoggingEvent event: logsList) {
            if (event.getMessage().contains("air sensor data")) airMessages++;
            if (event.getMessage().contains("water sensor data")) waterMessages++;
        }
        assertEquals(airMessages, 2);
        assertEquals(waterMessages, 1);
    }


}
