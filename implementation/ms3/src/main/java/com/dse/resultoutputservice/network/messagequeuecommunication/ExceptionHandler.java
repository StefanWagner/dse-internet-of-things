package com.dse.resultoutputservice.network.messagequeuecommunication;

import com.dse.resultoutputservice.exceptions.InvalidDataRecordException;
import com.dse.resultoutputservice.network.webcommunication.WebRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ExceptionHandler {
    private final static Logger log = LoggerFactory.getLogger(WebRestController.class);

    public ResponseEntity handleInvalidMessageTypeException(InvalidDataRecordException e) {
        log.warn("Message with wrong DataRecord received!");
        return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build();
    }
}
