package com.dse.messagequeuemodel.requestwrapper;

import com.dse.messagequeuemodel.Subscriber;
import lombok.Value;

@Value
public class ChangeDelayRequestWrapper implements RequestObject {
    Subscriber subscriber;
    int delayInMillis;
}
