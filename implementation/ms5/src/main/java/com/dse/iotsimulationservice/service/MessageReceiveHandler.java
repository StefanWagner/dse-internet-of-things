package com.dse.iotsimulationservice.service;

import com.dse.iotsimulationservice.service.exceptions.InvalidMessageTypeException;
import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeueclient.MessageReceiver;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.TerminationData;
import com.dse.messagequeuemodel.TopicEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

/**
 * Handler for termination messages from the message queue.
 */
@Component
@Slf4j
public class MessageReceiveHandler implements MessageReceiver {

    @Value("${messagequeue.client.clienturl}")
    private String clientUrl;

    private MessageQueueClient messageQueueClient;

    public MessageReceiveHandler(MessageQueueClient client) {
        this.messageQueueClient = client;
    }


    /**
     * Subscribes to the Termination messages, the only topic relevant ot the IOT-Simulation-Service
     */
    @PostConstruct
    public void subscribeToTermination() {
        boolean isSubscribed = false;
        Set<TopicEnum> topics = new HashSet<>();
        topics.add(TopicEnum.TERMINATION);
        while (!isSubscribed) {
            try {
                this.messageQueueClient.subscribeToTopics(topics);
                isSubscribed = true;
            } catch (HttpClientErrorException e) {
                log.info("4xx received. Probably already subscribed");
                isSubscribed = true;
            } catch (RestClientException e) {
                log.info("Could not subscribe to topics successfully. Retrying in 3 seconds", e);
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }

    /**
     * Shuts down the application if a termination message is sent and ignores every other type
     *
     * @param message a message from the message queue
     */
    @Override
    public void processMessage(Message message) {
        log.info("Message received");
        if (message.getTopic() == TopicEnum.TERMINATION &&
                ((TerminationData) message.getRecord()).getTargets().contains(clientUrl)) {
            log.info("Termination message received. Unsubscribing and shutting down");
            Set<TopicEnum> topics = new HashSet<>();
            topics.add(TopicEnum.TERMINATION);
            try {
                messageQueueClient.unsubscribeFromTopics(topics);
            } catch (RestClientException e) {
                log.warn("Could not unsubscribe form Termination topic! Shutting down anyway!");
            } catch (IllegalArgumentException e) {
                log.warn("Already unsubscribed, shutting down now");
            }
            System.exit(0);
        }
        if(message.getTopic() != TopicEnum.TERMINATION) {
            throw new InvalidMessageTypeException("Message of type " + message.getTopic().getTopic());
        }
    }
}
