package com.dse.messagequeue.exception;

public class InvalidInputException extends RuntimeException {
    private String subscriberURL;

    public InvalidInputException(String message, String subscriberURL) {
        super(message);
        this.subscriberURL = subscriberURL;
    }

    public InvalidInputException(String message, Throwable cause, String subscriberURL) {
        super(message, cause);
        this.subscriberURL = subscriberURL;
    }

    public String getSubscriberURL() {
        return subscriberURL;
    }
}
