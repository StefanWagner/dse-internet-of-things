var anomalies = {
    size: 0,
    maxTimestamp: "1970-01-01T00:00:00.000Z",
    minTimestamp: "1970-01-01T00:00:00.000Z",
    anomalies: new Array(),

    getAnomalies: function() {
        anomalies.size = anomalies.size + 10;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                anomalies.anomalies = anomalies.anomalies.concat((JSON.parse(this.responseText)).anomalies);
                anomalies.anomalies.sort(function(a,b) {return new Date(b.timestamp) - new Date(a.timestamp)});
                anomalies.createTable();
                anomalies.maxTimestamp = anomalies.anomalies[0].timestamp;
                anomalies.minTimestamp = anomalies.anomalies[anomalies.anomalies.length-1].timestamp;
            }
        };
        xmlhttp.open("GET", "/getAnomalies?size=" + this.size +"&maxTimestamp=" + this.maxTimestamp +"&minTimestamp=" + this.minTimestamp, true);
        xmlhttp.send();
    },

    createTable: function() {
        var i;
        var table="<tr><th>Timestamp</th><th>Error Type</th><th>Error Description</th></tr>";
        for (i = 0; i < this.anomalies.length; i++) {
            table += "<tr><td>" +
                    this.anomalies[i].timestamp +
                    "</td><td>" +
                    this.anomalies[i].errorType +
                    "</td><td>" +
                    this.anomalies[i].errorDescription +
                    "</td></tr>";
        }
        document.getElementById("anomalies").innerHTML = table;
    }
}