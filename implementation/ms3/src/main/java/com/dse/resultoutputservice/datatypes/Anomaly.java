package com.dse.resultoutputservice.datatypes;

import java.time.Instant;

/**
 * The Anomaly class is used for micro service internal information handling.
 * This class simulates a table row of the database table anomalies.
 * So the messages can be converted into anomaly and easily inserted into the database.
 */
public class Anomaly {
    private Instant timestamp;
    private String errorType;
    private String errorDescription;

    public Anomaly(Instant timestamp, String errorType, String errorDescription) {
        this.timestamp = timestamp;
        this.errorType = errorType;
        this.errorDescription = errorDescription;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getErrorType() {
        return errorType;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
