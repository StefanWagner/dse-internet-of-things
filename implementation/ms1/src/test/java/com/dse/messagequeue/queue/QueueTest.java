package com.dse.messagequeue.queue;

import com.dse.messagequeue.MQTestClassGenerator;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.PriorityEnum;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class QueueTest {

    @Mock
    RestTemplate restTemplate;

    private Queue queue;
    ArgumentCaptor<Message> argument = ArgumentCaptor.forClass(Message.class);

    Executor testExecutor = Executors.newSingleThreadExecutor();

    @BeforeEach
    public void setup() {
        Subscriber sub = new Subscriber("test", 500, Collections.emptySet());
        MockitoAnnotations.initMocks(this);
        queue = new Queue(200, sub, restTemplate, MQTestClassGenerator.generateSubscriptionManager());
    }

    @Test
    void priorityIsRespected() throws InterruptedException {
        Message highPrio = MQTestClassGenerator.getRandomMessage(TopicEnum.ANOMALY, PriorityEnum.HIGH);
        Message medPrio = MQTestClassGenerator.getRandomMessage(TopicEnum.AIRSENSORDATA, PriorityEnum.MEDIUM);
        Message lowPrio = MQTestClassGenerator.getRandomMessage(TopicEnum.PREDICTION, PriorityEnum.LOW);

        queue.addMessage(lowPrio);
        queue.addMessage(highPrio);
        queue.addMessage(medPrio);

        testExecutor.execute(queue);
        Thread.sleep(3000);
        verify(restTemplate, times(3)).postForObject(any(String.class), argument.capture(), eq(Message.class));
        assertThat(argument.getAllValues()).containsExactly(highPrio, medPrio, lowPrio);

    }

    @Test
    void timeStampOrderingIsRespected() throws InterruptedException {
        Message first = MQTestClassGenerator.getRandomMessage(TopicEnum.ANOMALY, PriorityEnum.MEDIUM);
        Thread.sleep(300);
        Message second = MQTestClassGenerator.getRandomMessage(TopicEnum.AIRSENSORDATA, PriorityEnum.MEDIUM);
        Thread.sleep(300);
        Message third = MQTestClassGenerator.getRandomMessage(TopicEnum.PREDICTION, PriorityEnum.MEDIUM);
        Thread.sleep(300);

        queue.addMessage(third);
        queue.addMessage(second);
        queue.addMessage(first);

        testExecutor.execute(queue);
        Thread.sleep(3000);
        verify(restTemplate, times(3)).postForObject(any(String.class), argument.capture(), eq(Message.class));
        assertThat(argument.getAllValues()).containsExactly(first, second, third);

    }

}