package com.dse.monitoring_component.cli_client.test.commands;

import com.dse.monitoring_component.cli_client.commands.StatusCommand;
import com.dse.monitoring_component.cli_client.commands.TerminateCommand;
import com.dse.monitoring_component.cli_client.data.DAO;
import io.github.lukas_krickl.cli_client.commands.Command;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class TerminateCommandTest {


    @Test
    void targetNotValid_displaysFail() {
        Command cmd = new TerminateCommand();
        cmd = cmd.makeCallable(new ArrayList<>(),"not-a-valid-url");
        assertDoesNotThrow(cmd::run);
    }

    @Test
    void targetIsValidUrl_doesNotDisplayFail() {
        Command cmd = new TerminateCommand();
        cmd = cmd.makeCallable(new ArrayList<>(),"http://valid-syntax-url:8080");
        assertDoesNotThrow(cmd::run);
    }

    @Test
    void mcNotAvailable_displaysErrorMessage() {
        DAO.getInstance().getConnector().setMonitoringComponentURL("http://not-a-valid-url:123");
        Command cmd = new TerminateCommand();
        cmd = cmd.makeCallable(new ArrayList<>(),"http://valid-syntax-url:8080");

        assertDoesNotThrow(cmd::run);
    }


    @Test
    void mcAvailable_sendTerminationMessage(){
        Command cmd = new TerminateCommand();
        cmd = cmd.makeCallable(new ArrayList<>(),"http://valid-syntax-url:8080");
        cmd.run();
    }
}
