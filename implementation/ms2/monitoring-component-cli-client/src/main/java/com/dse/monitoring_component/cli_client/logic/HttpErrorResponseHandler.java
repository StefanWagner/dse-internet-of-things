package com.dse.monitoring_component.cli_client.logic;

import com.dse.monitoring_component.cli_client.exceptions.BadRequestException;
import com.dse.monitoring_component.cli_client.exceptions.MCNotReachableException;
import com.dse.monitoring_component.cli_client.exceptions.MQNotReachableException;
import com.dse.monitoring_component.cli_client.exceptions.NotFoundException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.util.Scanner;

/**
 * Error response handler for Network Connector.
 * When a error code is returned by the Monitoring Component to the RestTemplate,
 * this handler throws the corresponding exception
 */
public class HttpErrorResponseHandler implements ResponseErrorHandler {

    public static IOException handleRestClientException(RestClientException e) {
        Throwable nestedException = e.getCause();
        if (nestedException instanceof ConnectException) {
            return new MCNotReachableException();
        }

        return new IOException(nestedException.getMessage());
    }

    /**
     * Only return true if code is a 2xx
     * 1xx, 3xx and above 5xx are not expected and 4xx and 5xx are errors
     */
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return !response.getStatusCode().is2xxSuccessful();
    }

    /**
     * Handles http error response codes
     *
     * @param response response from the rest template
     * @throws IOException corresponding exception
     */
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        String message = bodyToString(response.getBody());

        switch (response.getStatusCode()) {
            case BAD_REQUEST: throw new BadRequestException(message);
            case NOT_FOUND: throw new NotFoundException(message);
            case SERVICE_UNAVAILABLE: throw new MQNotReachableException(message);
            case INTERNAL_SERVER_ERROR: throw new IOException(
                    "The monitoring component reported a internal server error: " + message+
                        "\n Try to restart the service");
            default: throw new IOException("Unexpected status code received: "
                    +response.getRawStatusCode() + message);
        }
    }

    /**
     * Http Response body to string helper method
     * @param inputStream error code response body
     * @return String with the error message
     */
    String bodyToString(InputStream inputStream) {
        Scanner s = new Scanner(inputStream).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
