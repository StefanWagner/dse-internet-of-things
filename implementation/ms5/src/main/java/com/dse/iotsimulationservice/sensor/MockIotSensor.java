package com.dse.iotsimulationservice.sensor;

import com.dse.iotsimulationservice.service.SensorManager;
import com.dse.messagequeuemodel.AggregableValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.Map;
import java.util.Random;


/**
 * Abstract superclass for a MockIotDevice
 * Make a custom child implementation to publish your own values to an observer
 */
@Slf4j
public abstract class MockIotSensor {

    /**
     * The object which should be notified about new omitted values
     */
    private SensorManager observer;

    public MockIotSensor(SensorManager observer) {
        this.observer = observer;
        log.info("creating mockiotSensor");
    }

    /**
     * The identifier which is used to publish values.
     */
    @Value("${sensor.station}")
    protected String identifier;
    protected Map<String, List<AggregableValue>> srcValues;
    private Random rand = new Random();

    /**
     * Notifies the observer about a new omitted value
     */
    void notifyObserver() {
        log.info("Publishing Value!");
        List<AggregableValue> values = this.srcValues.get(identifier);
        AggregableValue valueToPublish = values.get(rand.nextInt(values.size()));
        this.observer.valueOmitted(valueToPublish);

    }
}
