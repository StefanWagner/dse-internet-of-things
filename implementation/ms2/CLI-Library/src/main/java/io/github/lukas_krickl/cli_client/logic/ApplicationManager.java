package io.github.lukas_krickl.cli_client.logic;

import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.commands.CommandParser;
import io.github.lukas_krickl.cli_client.commands.CommandRepository;
import io.github.lukas_krickl.cli_client.visual.OutputText;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 * The "AppMan" is responsible for command execution and managing resources at program startup and termination.
 */
public class ApplicationManager {
	private CommandParser cmdParser;
	private UserInterface ui;
	private Runnable initialisation;
	private Runnable shutdown;

	/**
	 * Has to be configured by a program using this library.
	 * {@link io.github.lukas_krickl.cli_client.builder.application.ApplicationManagerBuilder}
	 *  is highly recommended
	 *
	 * @param commandList list of commands of the application using this library
	 * @param outputText application specific output text
	 * @param initialisation Runnable to run at startup, after all classes are initialized
	 * @param shutdown to add as shutdown hook, run before the java vm terminates
	 */
	public ApplicationManager(Set<Command> commandList, OutputText outputText, Runnable initialisation, Runnable shutdown) {
		//define all commands
		TreeSet<Command> definedCommands = new TreeSet<>(commandList);
		definedCommands.addAll(Arrays.asList(Configuration.GENERAL_COMMANDS));
		CommandRepository cmdRepo = new CommandRepository(definedCommands);

		ui = UserInterface.setUp(outputText, cmdRepo);
		cmdParser = new CommandParser(cmdRepo.getDefinedCommands());
		this.initialisation = initialisation;
		this.shutdown = shutdown;
	}

	public ApplicationManager(Set<Command> commandList, OutputText outputText) {
		this(commandList,outputText, null, null);
	}

	/**
	 * Initializes all classes and then runs commands
	 */
	public void start() {
		//initialize shutdown hooks
		Runtime.getRuntime().addShutdownHook(new Thread(()->{
			if(shutdown!=null) {
				shutdown.run();
			}
			ui.exit();
		}));

		//run initialisation
		ui.initialize();

		if (initialisation!=null) {
			initialisation.run();
		}

		runCommands();
	}

	/**
	 * Listens to user commands via CLI
	 * after a valid command was inserted executes the command
	 */
	private void runCommands() {

		Command userCommand;
		do {
			//get user command
			userCommand = listenForUserCommand();
			if (userCommand.isCallable()) {
				try {
					userCommand.run();
				} catch (Exception e){
					UserInterface.printSection(e.getMessage());
					e.printStackTrace();
				}
			}

		} while (!userCommand.getCmdInfo().getName().equals("QUIT"));
	}

	/**
	 * Listens for a command
	 * @return
	 */
	private Command listenForUserCommand() {
		String inputLine;

		while (true) {
			inputLine = ui.getNextLine();

			if (!inputLine.isEmpty()) {
				try {
					return cmdParser.parseUserInput(inputLine);
				} catch (IllegalArgumentException e) {
					UserInterface.println(e.getMessage());
				}
			}
		}
	}
}
