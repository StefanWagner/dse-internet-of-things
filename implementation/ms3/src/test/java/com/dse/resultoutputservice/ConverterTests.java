package com.dse.resultoutputservice;

import com.dse.messagequeuemodel.*;
import com.dse.resultoutputservice.datatypes.AirPrediction;
import com.dse.resultoutputservice.datatypes.Anomaly;
import com.dse.resultoutputservice.datatypes.WaterPrediction;
import com.dse.resultoutputservice.network.converter.Converter;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

public class ConverterTests {
    @Test
    void messageToAnomaly_shouldConvertMessageToAnomaly() {
        Message message = new Message(TopicEnum.ANOMALY, new AnomalyDataRecord(ErrorTypeEnum.NULL, "errorDescription Test"));
        Anomaly anomaly = Converter.messageToAnomaly(message);
        assertEquals("NULL", anomaly.getErrorType());
        assertEquals("errorDescription Test", anomaly.getErrorDescription());
    }

    @Test
    void messageToAirPrediction_shouldConvertMessageToAirPrediction() {
        Message message = new Message(TopicEnum.PREDICTION, new AirSensorPredictionDataRecord(new BigDecimal(1), new BigDecimal(1), new BigDecimal(1)));
        AirPrediction airPrediction = Converter.messageToAirPrediction(message);
        assertEquals(1, airPrediction.getAirTemperature());
        assertEquals(1, airPrediction.getWindSpeed());
        assertEquals(1, airPrediction.getHumidity());
    }

    @Test
    void messageToWaterPrediction_shouldConvertMessageToWaterPrediction() {
        Message message = new Message(TopicEnum.PREDICTION, new WaterSensorPredictionDataRecord(new BigDecimal(1), new BigDecimal(1), new BigDecimal(1)));
        WaterPrediction waterPrediction = Converter.messageToWaterPrediction(message);
        assertEquals(1, waterPrediction.getWaterTemperature());
        assertEquals(1, waterPrediction.getTurbidity());
        assertEquals(1, waterPrediction.getWaveHeight());
    }
}
