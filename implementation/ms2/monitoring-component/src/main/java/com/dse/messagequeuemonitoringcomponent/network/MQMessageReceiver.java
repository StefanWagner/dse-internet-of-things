package com.dse.messagequeuemonitoringcomponent.network;

import com.dse.messagequeueclient.MessageReceiver;
import com.dse.messagequeuemodel.CacheMeta;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.SubscriberMeta;
import com.dse.messagequeuemodel.TerminationData;
import com.dse.messagequeuemonitoringcomponent.exception.InvalidMessageReceivedException;
import com.dse.messagequeuemonitoringcomponent.managers.ApplicationManager;
import com.dse.messagequeuemonitoringcomponent.managers.DataManager;
import com.dse.messagequeuemonitoringcomponent.managers.messages.CacheMessageManager;
import com.dse.messagequeuemonitoringcomponent.managers.messages.SubscriberMessageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Processes a incoming message from the message queue.
 */
@Component
public class MQMessageReceiver implements MessageReceiver {
    private final DataManager dataManager;
    private final ApplicationManager applicationManager;
    private final SubscriberMessageManager subscriberMessageManager;
    private final CacheMessageManager cacheMessageManager;
    private static final ThreadPoolExecutor threadPool =
            (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

    private Logger log = LoggerFactory.getLogger(MQMessageReceiver.class);

    @Autowired
    public MQMessageReceiver(DataManager dataManager, ApplicationManager applicationManager) {
        this.dataManager = dataManager;
        this.applicationManager = applicationManager;
        this.subscriberMessageManager = new SubscriberMessageManager(dataManager, applicationManager);
        this.cacheMessageManager = new CacheMessageManager(dataManager);
    }

    /**
     * Implementation from the{@link MessageReceiver} interface
     * <p>
     * redirects the received message to the corresponding message processing method,
     * depending on its topic
     *
     * Checks if all possible threads are use and increases the delay if so
     * decreases delay again if only half of the available threads are used
     *
     * @param message message from the message queue
     */
    @Override
    public void processMessage(Message message) {

        if(threadPool.getActiveCount() == threadPool.getMaximumPoolSize()) {
            log.debug("threadpool active count {}", threadPool.getActiveCount());
            applicationManager.increaseDelayMessageQueue();

        } else if(threadPool.getLargestPoolSize()>3
                && threadPool.getActiveCount() < threadPool.getMaximumPoolSize()/2){
            applicationManager.decreaseDelayMessageQueue();
            log.debug("threadpool active count {}", threadPool.getActiveCount());
        }

        switch (message.getTopic()) {
            case METADATA: {
                MessageValidator.checkMetaMessage(message);
                threadPool.submit(()->processMetaMessage(message));
                break;
            }
            case ERRORS: {
                MessageValidator.checkErrorMessage(message);
                processErrorMessage(message);
                break;
            }

            case TERMINATION: {
                MessageValidator.checkTerminationMessage(message);
                processTerminationMessage(message);
                break;
            }
            default:
                throw new InvalidMessageReceivedException("Topic not subscribed",message);
        }
    }


    /**
     * The MetaData topic can contain messages with subscriberMeta DataRecord
     * or CacheMeta DataRecord.
     * Processes a message received in the MetaData Topic by passing it to the
     * designated MessageManager
     *
     * @param message Message Received in the MetaData Topic
     */
    private void processMetaMessage(Message message) {
        if (message.getRecord() instanceof SubscriberMeta) {
            log.debug("Metamessage: Subscriber meta received");
            subscriberMessageManager.manageSubscribers(message);

        } else if (message.getRecord() instanceof CacheMeta) {
            log.debug("Metamessage: Cache meta received");
            cacheMessageManager.manageCache(message);
        }
    }


    /**
     * Processes a error message, by appending the error message to the list of collected error messages.
     *
     * @param message A message received in the Errors topic
     */
    private void processErrorMessage(Message message) {
        log.debug("Error message received");
        dataManager.getErrorMessageList().addError(message);
    }

    /**
     * Processes a termination message
     * identifies if this service is mentioned in the targets of the termination message
     * and terminates if so
     *
     * @param message A message received in the Termination topic
     */
    private void processTerminationMessage(Message message) {
        log.info("Termination message received");
        //if this clients url is contained -> shut down
        TerminationData data = (TerminationData) message.getRecord();
        if (data.getTargets().contains(applicationManager.getClientUrl())) {
            applicationManager.terminateSelf();
        }
    }


    /**
     * converts a long into a int safely
     *
     * Math.toIntExact with throw exception
     *
     * @param value long
     * @return value or int.MAX_VALUE if value is bigger than int
     */
    private static int toInt(long value) {
        return value>Integer.MAX_VALUE ? Integer.MAX_VALUE: Math.toIntExact(value);
    }
}
