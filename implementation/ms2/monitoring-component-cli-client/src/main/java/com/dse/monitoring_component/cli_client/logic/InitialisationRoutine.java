package com.dse.monitoring_component.cli_client.logic;

import com.dse.messagequeuemodel.Status;
import com.dse.monitoring_component.cli_client.data.DAO;

import com.dse.monitoring_component.cli_client.visual.Display;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

/**
 * Runnable passed to application manager of the CLI-Library
 * which is run after initialisation finished.
 *
 * Makes a single status request
 */
public class InitialisationRoutine implements Runnable {
    @Override
    public void run() {
        try {
            Status status = DAO.getInstance().getConnector().requestStatus();
            UserInterface.printSection(Display.formatStatus(status));

        } catch (Exception e) {
            UserInterface.printSection(TextColor.YELLOW.apply("Waring: ")+e.getMessage());
        }
    }
}
