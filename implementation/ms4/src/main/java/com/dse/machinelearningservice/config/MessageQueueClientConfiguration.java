package com.dse.machinelearningservice.config;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeueclient.MessageQueueClientReceiveController;
import com.dse.messagequeueclient.MessageReceiver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration of the MessageQueueclient
 */
@Configuration
public class MessageQueueClientConfiguration {

    /**
     * Creates a message queue client that is used for the communication with the message queue.
     * @param clientUrl URL where the machine learning service is running
     * @param messageQueueUrl URL of the message queue
     * @return a client for handling communication between the machine learning service and the message queue
     */
    @Bean
    public MessageQueueClient messageQueueClient(
            @Value("${messagequeue.client.clienturl}") String clientUrl,
            @Value("${messagequeue.client.messagequeueurl}") String messageQueueUrl
    ) {
        return new MessageQueueClient(clientUrl, messageQueueUrl);
    }

    /**
     * Creates a receive controller to receive messages from the message queue.
     * @param messageReceiver the implementation of the MessageReceiver interface that should receive messages
     * @return a new message queue client receive controller
     */
    @Bean
    public MessageQueueClientReceiveController messageQueueClientReceiveController(MessageReceiver messageReceiver) {
        return new MessageQueueClientReceiveController(messageReceiver);
    }
}