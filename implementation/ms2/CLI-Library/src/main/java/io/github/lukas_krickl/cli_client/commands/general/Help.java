package io.github.lukas_krickl.cli_client.commands.general;

import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.commands.CommandInfo;
import io.github.lukas_krickl.cli_client.logic.Configuration;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.TreeSet;

public class Help extends Command {
	private final int screenWidth = Configuration.SCREEN_WIDTH;
	private TreeSet<Command> applicationCommands = null;

	public Help() {
		super(new CommandInfoBuilder("HELP")
				.addSyntax("help")
				.addSyntax("?")
				.setDescription("Displays further information on usage of this application.\n" +
						"If a command is given, displays only information of the single command.\n" +
						" e.g.'help colormode' displays description of the command 'colormode'")
				.setRequiresArgs(ArgsRequired.OPTIONAL)
				.build());
	}

	/**
	 * Command execution
	 */
	@Override
	public void run() {
		UserInterface ui = UserInterface.getInstance();

		if (applicationCommands == null) {
			setApplicationCommandList();
		}

		if (getArgs().isEmpty()) {
			UserInterface.println(ui.getOutputText().getHelpDialog());

			//print general command
			printHeadline("GENERAL-COMMANDS");
			for (Command generalCommand : Configuration.GENERAL_COMMANDS) {
				UserInterface.println(printCommand(generalCommand.getCmdInfo()));
			}

			//print application command
			printHeadline("APPLICATION-COMMANDS");
			for (Command applicationCommand : applicationCommands) {
				UserInterface.println(printCommand(applicationCommand.getCmdInfo()));
			}

			UserInterface.print("\n");
		} else {
			TreeSet<Command> definedCommands = UserInterface.getInstance().getCmdRepo().getDefinedCommands();
			for (Command cmd:definedCommands) {
				if (cmd.getCmdInfo().getSyntax().contains(getArgs())) {
					UserInterface.printSection(printCommand(cmd.getCmdInfo()));
					return;
				}
			}
			UserInterface.printSection("Command '"+getArgs()+"' not found!");
		}
	}

	private String printCommand(CommandInfo cmdInfo) {
		StringBuilder sb = new StringBuilder();
		// print line ----------------------------------------------------
		sb.append(String.format("%"+screenWidth+"s","").replace(' ','-')).append("\n");
		//append name
		sb.append(TextColor.CYAN.apply(cmdInfo.getName())).append('\n');

		//append Description
		sb.append(printWithOffset(2 , cmdInfo.getDescription(), true)).append("\n");

		//append syntax
		sb.append(TextColor.YELLOW.apply("  SYNTAX: "));
		for (String syntax : cmdInfo.getSyntax()) {
			sb.append("'").append(syntax).append("' ");
		}
		sb.append('\n');

		//append parameters
		sb.append(printParameters(cmdInfo.getParameters()));

		//display requires data
		sb.append(TextColor.MAGENTA.apply("  REQUIRES ARGUMENT: "));
		sb.append(cmdInfo.getArgsRequired().name());


		return sb.toString();
	}

	private String printParameters(Hashtable<String, String> parameters) {
		StringBuilder sb = new StringBuilder((TextColor.GREEN.apply("  PARAMETERS:\n")));

		for (String param : parameters.keySet()) {
			sb.append(printWithOffset(4, param+": "+parameters.get(param), false)).append("\n");
		}

		return sb.toString();
	}

	private String printWithOffset(int offset, String text, boolean offsetFirstLine) {
		StringBuilder sb = new StringBuilder();

		//process all new line chars
		LinkedList<String> lines = new LinkedList<>(Arrays.asList(text.split("\n")));
		//split over length lines
		for (int i = 0; i<lines.size(); i++){
			//text to long for line
			if(lines.get(i).length()>screenWidth-offset*2) {
				//first line
				String first = lines.get(i).substring(0, screenWidth-offset*2);
				String second = lines.get(i).substring(screenWidth-offset*2);

				String[] split = first.split(" ");
				String lastWordFragment = split[split.length-1];
				//erase last word fragment from first string
				first = first.substring(0, first.length()-lastWordFragment.length());
				lines.set(i, first);
				//add last word fragment to second line
				lines.add(i+1, lastWordFragment+second);
			}

		}

		for (int i = 0; i<lines.size(); i++) {
			if (i==0 && !offsetFirstLine) {
				sb.append(String.format("%"+offset+"s","")).append(lines.get(0));
			} else {
				sb.append(String.format("%"+offset*2+"s","")).append(lines.get(i));
			}
			sb.append("\n");
		}

		return sb.toString();
	}

	private void setApplicationCommandList(){
		this.applicationCommands = new TreeSet<>(UserInterface.getInstance().getCmdRepo().getDefinedCommands());
		this.applicationCommands.removeAll(Arrays.asList(Configuration.GENERAL_COMMANDS));
	}

	private void printHeadline(String text){
		UserInterface.println(String.format("%"+screenWidth+"s","").replace(' ','='));
		UserInterface.println(String.format("%"+(screenWidth/2+text.length()/2)+"s",text));
	}


}
