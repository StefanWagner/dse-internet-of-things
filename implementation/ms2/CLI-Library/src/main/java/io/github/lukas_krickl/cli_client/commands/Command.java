package io.github.lukas_krickl.cli_client.commands;

import io.github.lukas_krickl.cli_client.exceptions.IllegalCommandStateException;
import io.github.lukas_krickl.cli_client.exceptions.InvalidCommandConfigurationException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * An abstract command class from which every application command should be derived.
 * To implement an application command a separate class has to inherit this one.
 * <p>
 * The new class defines and passes the specification in the constructor using the CommandInfo;
 * The execution code of the new command is specified in the run method.
 * <p>
 * To call a command
 */
public abstract class Command implements Runnable, Cloneable, Comparable<Command> {
	private final CommandInfo cmdInfo;
	private ArrayList<String> params;
	private String args;
	private boolean isCallable;

	public Command(CommandInfo cmdInfo) {
		this.cmdInfo = cmdInfo;
		params = null;
		args = null;
		isCallable = false;
	}

	/**
	 * Clones this object and makes it callable with the makeCallable method. (assigns user input parameters and data to it)
	 * The returned command is "callable" in a sense, that all needed user input is provided, not null and
	 * ready to be handed over to a thread for execution.
	 *
	 * @param params parameters for this command provided by the user
	 * @param args   data value provided by the user
	 * @return a "callable" command
	 */
	public Command makeCallable(ArrayList<String> params, String args) {
		try {
			Command callable = (Command) this.clone();
			callable.setUserInput(params, args);
			return callable;

		} catch (CloneNotSupportedException e) {
			throw new InvalidCommandConfigurationException("Making command callable failed: Command not Cloneable", e, this);

		} catch (Exception e) {
			//catch any exception to not completely crash the program
			throw new InvalidCommandConfigurationException("Making command callable failed: Exception returned", e, this);
		}
	}

	public boolean matchesSyntax(String input) {
		return cmdInfo.getSyntax().contains(input);
	}

	public boolean hasParameter(String parameter) {
		return cmdInfo.getParameters().containsKey(parameter);
	}

	// GETTER AND SETTER

	/**
	 * Assigns user input to the command and sets callable to true.
	 *
	 * @param params not null user parameters
	 * @param args   not null user data
	 * @throws IllegalCommandStateException if it is already callable.
	 */
	private void setUserInput(ArrayList<String> params, String args) throws IllegalCommandStateException {
		if (this.isCallable) {
			throw new IllegalCommandStateException("Command is already callable");
		}

		this.params = params;
		this.args = args;
		this.isCallable = true;
	}

	public CommandInfo getCmdInfo() {
		return cmdInfo;
	}

	public ArrayList<String> getParams() {
		return params;
	}

	public String getArgs() {
		return args;
	}

	public boolean isCallable() {
		return isCallable;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Command)) return false;
		Command command = (Command) o;
		return isCallable() == command.isCallable() &&
				getCmdInfo().equals(command.getCmdInfo()) &&
				Objects.equals(getParams(), command.getParams()) &&
				Objects.equals(getArgs(), command.getArgs());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getCmdInfo(), getParams(), getArgs(), isCallable());
	}

	@Override
	public int compareTo(Command o) {
		return getCmdInfo().getName().compareTo(o.getCmdInfo().getName());
	}
}
