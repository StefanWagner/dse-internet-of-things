package com.dse.test_microservice_client.commands;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.test_microservice_client.application.TestClientRepository;
import com.dse.test_microservice_client.generators.MessageGenerator;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;
import org.springframework.web.client.RestClientException;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class SubscriberCommand extends Command {
    private TestClientRepository repo;

    public SubscriberCommand() {
        super(new CommandInfoBuilder("SUBSCRIBER")
                .addSyntax("subscriber")

                .addParameterList()
                .addParameter(
                        "-show",
                        "Displays a list of all subscriber with their corresponding number")

                .addParameter(
                        "-delay",
                        "Changes the delay of the subscriber with the given number to the given milliseconds.\n" +
                                "e.g. 'subscriber -delay 2 3000ms' changes the delay of subscriber number 2 to 3000 milliseconds")
                .addParameter(
                        "-topics",
                        "Usage: 'subscriber <number> -topics <topic-name> <topic-name> ...' \n" +
                                "Select a subscriber by its number and select topics by their names.\n" +
                                "Adds the given topic if not subscribed, removes the given topic if subscribed.\n" +
                                "\nUse command 'topics' to display topic names\n" +
                                "NOTE: if all topics are unsubscribed, the subscriber is deleted. ")

                .finishParameterList()
                .setDescription("Command to test subscribers. Without a parameter and argument, a new subscriber is created." +
                        " Use parameters to change/show subscribers. For termination use the 'terminate' command.")
                .setRequiresArgs(ArgsRequired.OPTIONAL)
                .build());
    }

    @Override
    public void run() {
        repo = TestClientRepository.getInstance();

        if (getParams().isEmpty()) {
            if (getArgs().isEmpty()) {
                createNewTestSubscriber();
            } else {
                createNewTestSubscriber(getArgs());
            }
            UserInterface.printSection(TextColor.GREEN.apply("SUCCESS: ")+" New Subscriber created");
        } else if (getParams().contains("-show")) {
            showSubscribers();

        } else if (getParams().contains("-delay")) {
            UserInterface.printSection("This command is not implemented yet");

        } else if (getParams().contains("-topics")) {
            if (getArgs().trim().split(" ").length < 2) {
                UserInterface.println("Not a valid command, try 'help' for information!");
            } else {
                changeTopic();
            }

        } else {
            UserInterface.println("Not a valid command, try 'help' for information!");
        }
    }

    /**
     * Command: 'subscriber'
     */
    private void createNewTestSubscriber(String url) {
        //new subscriber is created and subscribed
        Subscriber testSub = new Subscriber(url, 0, EnumSet.of(TopicEnum.TERMINATION));
        MessageQueueClient subscriberClient = new MessageQueueClient(testSub.getUrl(), repo.getMessageQueueURL());

        //send to MQ
        try {
            subscriberClient.subscribeToTopics(EnumSet.of(TopicEnum.TERMINATION));
            repo.getSubscribers().put(testSub, subscriberClient);
        } catch (RestClientException e) {
            UserInterface.printSection(e.getMessage());
        }
    }

    private void createNewTestSubscriber() {
        createNewTestSubscriber(MessageGenerator.getRandomURL());
    }

    /**
     * Command: 'subscriber -show'
     * <p>
     * Displays a list of all test subscribers
     */
    private void showSubscribers() {
        Set<Subscriber> subscribers = repo.getSubscribers().keySet();

        int i = 0;
        for (Subscriber sub : subscribers) {
            UserInterface.println("[" + i + "] " + sub.getUrl());
            UserInterface.println("    Topics: " + sub.getTopics().toString());
            i++;
        }
    }

    /**
     * The subscriber command is used with parameter -topics
     * like 'subscriber 2 -topics METADATA TERMINATION' which should toggle topics Metadata and Termination
     * of subscriber nr 2 in the list of subscribers
     * <p>
     * Toggle means to subscribe unsubscribed topics and unsubscribe subscribed topics.
     */
    private void changeTopic() {
        String[] entities = getArgs().split(" ");

        //check if subscriber number is given
        if (!entities[0].matches("[0-9]+")) {
            UserInterface.println("Subscriber number missing");
            return;
        }

        //find subscriber
        Subscriber subscriber = repo.findSubscriberByNumber(Integer.parseInt(entities[0]));
        if (subscriber == null) {
            UserInterface.println("Subscriber not found!");
            return;
        }
        MessageQueueClient subscriberClient = repo.getSubscribers().get(subscriber);

        //find given topics
        Set<TopicEnum> subscribeTopics = new HashSet<>();
        Set<TopicEnum> unsubscribeTopics = new HashSet<>();

        for (int i = 1; i < entities.length; i++) {
            try {
                TopicEnum topic = TopicEnum.valueOf(entities[i].toUpperCase());
                //sort into correct set
                if (subscriber.getTopics().contains(topic)) {
                    //topic is subscribed -> unsubscribe
                    unsubscribeTopics.add(topic);
                } else {
                    //topic is not subscribed -> subscribe
                    subscribeTopics.add(topic);
                }
            } catch (IllegalArgumentException e) {
                UserInterface.println("Did not recognise topic: " + entities[i]);
            }
        }

        //toggle topics
        try {
            if (!subscribeTopics.isEmpty()) {
                subscriberClient.subscribeToTopics(subscribeTopics);
                Set<TopicEnum> newTopics = subscriber.getTopics();
                //set topics of subscriber saved outside of subscriberclient
                subscribeTopics.addAll(subscriber.getTopics());
                subscriber.setTopics(subscribeTopics);

            }

            if (!unsubscribeTopics.isEmpty()) {
                subscriberClient.unsubscribeFromTopics(unsubscribeTopics);
                //set topics of subscriber saved outside of subscriberclient
                Set<TopicEnum> newTopics = subscriber.getTopics();
                newTopics.removeAll(unsubscribeTopics);
                subscriber.setTopics(newTopics);
            }
        } catch (RuntimeException e) {
            UserInterface.printSection("ERROR response: " + e.getMessage());
        }

    }
}
