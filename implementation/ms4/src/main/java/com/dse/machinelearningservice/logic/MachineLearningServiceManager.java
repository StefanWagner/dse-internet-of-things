package com.dse.machinelearningservice.logic;

import com.dse.machinelearningservice.logic.anomalydetection.AirSensorAnomalyDetectionComponent;
import com.dse.machinelearningservice.logic.anomalydetection.AnomalyDetectionComponent;
import com.dse.machinelearningservice.logic.anomalydetection.WaterSensorAnomalyDetectionComponent;
import com.dse.machinelearningservice.logic.prediction.AirSensorPredictionComponent;
import com.dse.machinelearningservice.logic.prediction.PredictionComponent;
import com.dse.machinelearningservice.logic.prediction.WaterSensorPredictionComponent;
import com.dse.machinelearningservice.model.MessageRepository;

import com.dse.machinelearningservice.model.SimpleMessageRepository;
import com.dse.messagequeueclient.MessageReceiver;
import com.dse.messagequeuemodel.*;
import com.dse.messagequeueclient.MessageQueueClient;
import org.springframework.beans.factory.annotation.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Component
public class MachineLearningServiceManager implements MessageReceiver
{
    @Value("${messagequeue.client.clienturl}")
    private String clientUrl;

    /**
     * a new prediction is created after this many messages have been received for the same topic
     */
    private static final int PREDICTION_INTERVAL = 10;

    /**
     * number of threads that are available for concurrent execution of machine learning tasks
     */
    private static final int NUM_THREADS_IN_THREADPOOL = 8;

    /**
     * Repository containing the air sensor messages that have been received so far
     */
    private MessageRepository airMessageRepository;

    /**
     * Repository containing the water sensor messages that have been received so far
     */
    private MessageRepository waterMessageRepository;

    /**
     * Repository containing the previously created predictions
     */
    private MessageRepository airPredictionRepository;

    private MessageRepository waterPredictionRepository;

    /**
     * Threadpool for the concurrent execution of machine learning tasks
     */
    private ExecutorService threadPool;

    /**
     * topics that this service should subscribe to
     */
    private Set<TopicEnum> myTopics;


    /**
     * Client that handles the communication with the message queue
     */
    private MessageQueueClient messageQueueClient;


    public MachineLearningServiceManager(MessageQueueClient client) {
        airMessageRepository = new SimpleMessageRepository();
        waterMessageRepository = new SimpleMessageRepository();
        airPredictionRepository = new SimpleMessageRepository();
        waterPredictionRepository = new SimpleMessageRepository();

        this.messageQueueClient = client;

        //thread pool for asynchronous ML tasks
        threadPool = Executors.newFixedThreadPool(NUM_THREADS_IN_THREADPOOL);

        //MQ topics
        myTopics = new HashSet<TopicEnum>();
        myTopics.add(TopicEnum.AIRSENSORDATA);
        myTopics.add(TopicEnum.WATERSENSORDATA);
        myTopics.add(TopicEnum.TERMINATION);
    }

    /**
     * Subscribes the service to the following topics: AIRSENSORDATA, WATERSENSORDATA, TERMINATION.
     * This method is automatically executed after creation.
     */
    @PostConstruct
    public void subscribeToTopics(){

        if (messageQueueClient != null) {
            boolean isSubscribed = false;
            while (!isSubscribed) {
                try {
                    log.info("Subscribing to topics: " + myTopics);
                    messageQueueClient.subscribeToTopics(myTopics);
                    isSubscribed = true;
                    log.info("Connected successfully!");
                } catch (RestClientException e) {
                    log.info("Could not connect to message queue, retrying in 3 seconds");
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }

            }
        }

    }

    /**
     * unsubscribe from topics if the application is shut down
     */
    @PreDestroy
    public void unsubscribeFromTopicsOnShutdown(){
        try {
            log.info("Unsubscribing from topics");
            messageQueueClient.unsubscribeFromTopics(myTopics);
        } catch(IllegalArgumentException e) {
            log.info("No subscription available, shutting down anyways");
        } catch(RestClientException e) {
            log.info("Could not unsubscribe from topics, shutting down anyways");
        }

    }

    /**
     * Process an incoming message.
     * Each incoming message with sensor values is checked for anomalies.
     * Additionally, if a predefined number of messages for a topic has been received, a new prediction is created.
     * If the received message is a termination message containing the service's URL, the service will shut down.
     * @param message the message that should be processed
     */
    @Override
    public void processMessage(Message message) {
        log.info("Processing new message");

        MessageRepository messageRepository = null;
        MessageRepository predictionRepository = null;
        PredictionComponent predictionComponent = null;
        AnomalyDetectionComponent anomalyDetectionComponent = null;

        //get the message's topic
        //possible values are: airsensordata, watersensordata, termination
        TopicEnum topic = message.getTopic();

        if (topic == TopicEnum.AIRSENSORDATA) {
            log.info("Incoming message contained air sensor data");

            messageRepository = airMessageRepository;
            predictionRepository = airPredictionRepository;
            predictionComponent = new AirSensorPredictionComponent();
            anomalyDetectionComponent = new AirSensorAnomalyDetectionComponent();
        }
        else if (topic == TopicEnum.WATERSENSORDATA) {
            log.info("Incoming message contained water sensor data");

            messageRepository = waterMessageRepository;
            predictionRepository = waterPredictionRepository;
            predictionComponent = new WaterSensorPredictionComponent();
            anomalyDetectionComponent = new WaterSensorAnomalyDetectionComponent();
        }
        else if (topic == TopicEnum.TERMINATION &&
                ((TerminationData) message.getRecord()).getTargets().contains(clientUrl)) {
            log.info("Received termination message - shutting down");
            System.exit(0); //will unsubscribe from topics via preDestroy
        }
        else { //should not be subscribed to any other subjects
            throw new IllegalArgumentException("I did not subscribe to this!");
        }

        //add message to the repository
        int messageCount = messageRepository.addMessage(message);

        //if the message count is a multiple of the predictionInterval, create a new prediction
        if (messageCount % PREDICTION_INTERVAL == 0) {
            log.info("calculating prediction");

            predictionComponent.setUp(messageQueueClient, messageRepository.getAllMessages(),
                            predictionRepository);
            threadPool.execute(predictionComponent);
        }

        //search for anomalies
        log.info("searching for anomaly");
        anomalyDetectionComponent.setUp(message.getRecord(), predictionRepository.getMostRecentDataRecord(),
                messageQueueClient);
        threadPool.execute(anomalyDetectionComponent);

    }
}
