package io.github.lukas_krickl.cli_client.exceptions;

public class InputCanceledException extends Exception {
	public InputCanceledException() {
	}

	public InputCanceledException(String message) {
		super(message);
	}

	public InputCanceledException(String message, Throwable cause) {
		super(message, cause);
	}
}
