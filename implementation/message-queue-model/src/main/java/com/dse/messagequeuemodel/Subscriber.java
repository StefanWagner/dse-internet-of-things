package com.dse.messagequeuemodel;

import com.dse.messagequeuemodel.requestwrapper.RequestObject;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Abstraction of a subscribing message queue. Contains all information needed to identify a subscriber
 * The subscriber is designed fully threadsafe:
 * setters set the internal object to a copy of the given objects
 * getters return a copy of the internal objects
 *
 * This should prevent ConcurrentModificationExceptions and race conditions,
 * as the internal subscriber could be modified from outside if references are returned
 *
 * NOTE: any manipulation on objects returned by getters take no effect on the real subscriber
 */
public class Subscriber implements RequestObject {

	/**
	 * The url of the subscriber in the form "http://my.url.com:portNumber" or "http://192.168.0.1:portNumber"
	 */
	@NotBlank(message = "Subscriber url must not be null or blank string")
	private String url;

	/**
	 * delay in milliseconds between two messages to be sent to this subscriber
	 */
	@PositiveOrZero(message = "Delay must not be negative")
	private int delayInMillis;

	/**
	 * subscribed topics
	 */
	@NotNull(message = "Subscriber topic list must not be null")
	private Set<TopicEnum> topics;

	@JsonCreator
	public Subscriber(
			@JsonProperty("url") @NotNull(message = "Subscriber url must not be null") String url,
            @JsonProperty("delayInMillis") @PositiveOrZero(message = "Delay must not be negative") int delayInMillis,
            @JsonProperty("topics") @NotNull(message = "Subscriber topic list must not be null") Set<TopicEnum> topics
	) {
		this.url = url;
		this.delayInMillis = delayInMillis;
		this.topics = new HashSet<>(topics);
	}

	/**
	 * Compares the URL of two subscriber
	 *
	 * Used to identify if two subscriber objects belong to the same service.
	 *
	 * @param o other subscriber
	 * @return true if the URL of both subscribers match
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Subscriber)) return false;
		Subscriber that = (Subscriber) o;
		return getUrl().equals(that.getUrl());
	}

	/**
	 * Compares all values of two subscribers
	 *
	 * Used to identify if two subscriber objects are identical in a sense that all stored values are equal.
	 *
	 *
	 * @param other subscriber
	 * @return true if values of all variables match
	 */
	public boolean checkIdentity(Subscriber other) {
		return (this.url.equals(other.getUrl())
				&& this.topics.equals(other.getTopics())
				&& this.delayInMillis == other.getDelayInMillis());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getUrl());
	}

	//GETTER & SETTER

	public String getUrl() {
		return url;
	}

	public int getDelayInMillis() {
		return delayInMillis;
	}

	/**
	 * Decouples the internal object reference from the returned object
	 *
	 * @return copy of the internal topic set
	 */
	public Set<TopicEnum> getTopics() {
		return new HashSet<>(topics);
	}

	/**
	 * Decouples the given object reference from the internal object
	 *
	 * @param topics new topics
	 */
	public synchronized void setTopics(Set<TopicEnum> topics) {
		this.topics.clear();
		this.topics.addAll(topics);
	}

	/**
	 * Safely adds new topics to the list
	 * @param topics topics to add
	 * @return true if the set has changed
	 */
	public synchronized boolean addTopics(Set<TopicEnum> topics) {
		return this.topics.addAll(topics);
	}

	/**
	 * Safely removes topics from the list
	 * @param topics topics to remove
	 * @return true if the set changed
	 */
	public synchronized boolean removeTopics(Set<TopicEnum> topics) {
		return this.topics.removeAll(topics);
	}

	/**
	 * Updates the internal values(except the url) to the given subscriber ones
	 * @param subscriber new subscriber
	 */
	public synchronized void update(Subscriber subscriber) {
		setDelayInMillis(subscriber.getDelayInMillis());
		setTopics(subscriber.getTopics());
	}

	public synchronized void setDelayInMillis(int delayInMillis) {
		this.delayInMillis = delayInMillis;
	}


}
