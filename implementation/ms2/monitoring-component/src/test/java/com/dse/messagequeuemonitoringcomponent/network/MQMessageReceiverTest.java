package com.dse.messagequeuemonitoringcomponent.network;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.SubscriberMeta;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.messagequeuemonitoringcomponent.managers.DataManager;
import com.dse.messagequeuemonitoringcomponent.TestDataGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.EnumSet;


@SpringBootTest
public class MQMessageReceiverTest {

	@Autowired
	private MQMessageReceiver messageReceiver;

	@Autowired
	private DataManager dataManager;


	@Test
	public void processErrorMessage_dataManagerContainsErrorMessage() {
		Message errorMessage = TestDataGenerator.generateErrorMessage();

		Assertions.assertFalse(dataManager.getErrorMessageList().getAllErrors().contains(errorMessage));
		messageReceiver.processMessage(errorMessage);
		Assertions.assertTrue(dataManager.getErrorMessageList().getAllErrors().contains(errorMessage));
	}

	@Test
	void processSubscriberMeta_dataManagerContainsInformation() throws InterruptedException {
		//TEST ADD SUBSCRIBER
		Subscriber testSub = new Subscriber("http://test-sub:8080", 0, EnumSet.allOf(TopicEnum.class));
		Subscriber changedTestSub = new Subscriber(testSub.getUrl(), 1323, EnumSet.of(TopicEnum.ANOMALY));

		Message createMsg = TestDataGenerator.generateSubscriberMetaMsg(testSub, SubscriberMeta.Status.CREATED);
		Thread.sleep(1000);
		Message changeMsg = TestDataGenerator.generateSubscriberMetaMsg(changedTestSub, SubscriberMeta.Status.CHANGED);
		Thread.sleep(1000);
		Message deleteMsg = TestDataGenerator.generateSubscriberMetaMsg(changedTestSub, SubscriberMeta.Status.DELETED);

		Assertions.assertTrue(createMsg.getTimestamp().isBefore(changeMsg.getTimestamp()));
		Assertions.assertTrue(changeMsg.getTimestamp().isBefore(deleteMsg.getTimestamp()));

		Assertions.assertFalse(dataManager.getServices().containsKey(testSub));
		messageReceiver.processMessage(createMsg);
		Thread.sleep(1000);
		Assertions.assertTrue(dataManager.getServices().containsKey(testSub));

		//TEST CHANGE SUBSCRIBER

		messageReceiver.processMessage(changeMsg);

		//find changed sub in datamanger
		Subscriber changedTestSubOfDataManager = null;
		for (Subscriber sub : dataManager.getServices().keySet()) {
			if (changedTestSub.getUrl().equals(sub.getUrl())) {
				changedTestSubOfDataManager = sub;
			}
		}
		Assertions.assertNotNull(changedTestSubOfDataManager);

		Assertions.assertFalse(changedTestSubOfDataManager.checkIdentity(testSub));
		Assertions.assertTrue(changedTestSubOfDataManager.checkIdentity(changedTestSub));


		//TEST DELETE SUBSCRIBER
		Assertions.assertTrue(dataManager.getServices().containsKey(changedTestSub));
		messageReceiver.processMessage(deleteMsg);
		Thread.sleep(1000);
		Assertions.assertFalse(dataManager.getServices().containsKey(changedTestSub));
	}
}