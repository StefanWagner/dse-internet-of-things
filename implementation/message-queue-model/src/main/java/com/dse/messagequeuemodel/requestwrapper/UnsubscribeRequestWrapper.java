package com.dse.messagequeuemodel.requestwrapper;

import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import lombok.Value;

import java.util.Set;

@Value
public class UnsubscribeRequestWrapper implements RequestObject {
    Subscriber subscriber;
    Set<TopicEnum> topics;
}

