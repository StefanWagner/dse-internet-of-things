package com.dse.monitoring_component.cli_client.exceptions;

import java.io.IOException;

/**
 * Exception thrown when the HttpErrorResponseHandler received a code 503 from the Monitoring Component
 */
public class MQNotReachableException extends IOException {
    public MQNotReachableException(String message) {
        super(message);
    }

    public MQNotReachableException(String message, Throwable cause) {
        super(message, cause);
    }
}
