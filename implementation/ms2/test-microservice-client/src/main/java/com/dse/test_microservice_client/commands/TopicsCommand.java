package com.dse.test_microservice_client.commands;

import com.dse.messagequeuemodel.TopicEnum;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;

public class TopicsCommand extends Command {
    public TopicsCommand() {
        super(new CommandInfoBuilder("TOPICS")
                .addSyntax("topics")
                .setDescription("Displays a list of all topics!")
                .setRequiresArgs(ArgsRequired.NOT_REQUIRED)
                .build());
    }

    @Override
    public void run() {
        for (TopicEnum topic : TopicEnum.values()) {
            UserInterface.println("  " + topic.name());
        }
    }
}
