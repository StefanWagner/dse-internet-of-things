package com.dse.iotsimulationservice.aggregator;

import com.dse.messagequeuemodel.AggregableValue;
import com.dse.messagequeuemodel.SensorTypeEnum;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

class TestFixtures {

    /**
     * @return a list of Aggregable values with 3 values where each field has the same value 1, 2 and 3
     */
    static List<AggregableValue> getAggregableList() {
        List<AggregableValue> values = new ArrayList<>();
        AggregableValue.AggregableValueBuilder builder = AggregableValue.builder();
        builder.value1(BigDecimal.ONE)
                .value2(BigDecimal.ONE)
                .value3(BigDecimal.ONE)
                .identifier("ONE")
                .type(SensorTypeEnum.WATER)
                .timeStamp(Instant.MIN);
        values.add(builder.build());
        builder.value1(BigDecimal.ONE.add(BigDecimal.ONE))
                .value2(BigDecimal.ONE.add(BigDecimal.ONE))
                .value3(BigDecimal.ONE.add(BigDecimal.ONE))
                .identifier("TWO");
        values.add(builder.build());
        builder.value1(BigDecimal.ONE.add(BigDecimal.valueOf(2)))
                .value2(BigDecimal.ONE.add(BigDecimal.valueOf(2)))
                .value3(BigDecimal.ONE.add(BigDecimal.valueOf(2)))
                .identifier("THREE");
        values.add(builder.build());
        return values;
    }

    /**
     * @return a list of Aggregable values with 3 values where value has one field which is null
     */
    static List<AggregableValue> getAggregableListWithNull() {
        List<AggregableValue> values = new ArrayList<>();
        AggregableValue.AggregableValueBuilder builder = AggregableValue.builder();
        builder.value1(null)
                .value2(BigDecimal.ONE)
                .value3(BigDecimal.ONE)
                .identifier("ONE")
                .type(SensorTypeEnum.WATER)
                .timeStamp(Instant.MIN);
        values.add(builder.build());
        builder.value1(BigDecimal.ONE.add(BigDecimal.ONE))
                .value2(null)
                .value3(BigDecimal.ONE.add(BigDecimal.ONE))
                .identifier("TWO");
        values.add(builder.build());
        builder.value1(BigDecimal.ONE.add(BigDecimal.valueOf(2)))
                .value2(BigDecimal.ONE.add(BigDecimal.valueOf(2)))
                .value3(null)
                .identifier("THREE");
        values.add(builder.build());
        return values;
    }

    /**
     * @return a list of Aggregable values with 3 values where each has the same field have a null value
     */
    static List<AggregableValue> getAggregableListWithIdenticalNullField() {
        List<AggregableValue> values = new ArrayList<>();
        AggregableValue.AggregableValueBuilder builder = AggregableValue.builder();
        builder.value1(null)
                .value2(BigDecimal.ONE)
                .value3(BigDecimal.ONE)
                .identifier("ONE")
                .type(SensorTypeEnum.WATER)
                .timeStamp(Instant.MIN);
        values.add(builder.build());
        builder.value1(null)
                .value2(BigDecimal.ONE.add(BigDecimal.ONE))
                .value3(BigDecimal.ONE.add(BigDecimal.ONE))
                .identifier("TWO");
        values.add(builder.build());
        builder.value1(null)
                .value2(BigDecimal.ONE.add(BigDecimal.valueOf(2)))
                .value3(BigDecimal.ONE.add(BigDecimal.valueOf(2)))
                .identifier("THREE");
        values.add(builder.build());
        return values;
    }


    /**
     * @return a list of Aggregable values with 3 values where all are null.
     */
    static List<AggregableValue> getAggregableListWithOnlyNullInstances() {
        List<AggregableValue> values = new ArrayList<>();
        values.add(null);
        values.add(null);
        values.add(null);
        return values;
    }


}
