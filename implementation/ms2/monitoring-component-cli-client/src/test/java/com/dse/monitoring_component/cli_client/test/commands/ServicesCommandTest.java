package com.dse.monitoring_component.cli_client.test.commands;

import com.dse.monitoring_component.cli_client.commands.ServicesCommand;
import com.dse.monitoring_component.cli_client.data.DAO;
import io.github.lukas_krickl.cli_client.commands.Command;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class ServicesCommandTest {
    @Test
    void mcNotReachable_doesDisplayErrorMessage() {
        DAO.getInstance().getConnector().setMonitoringComponentURL("http://not-a-valid-url:123");
        Command cmd = new ServicesCommand().makeCallable(new ArrayList<>(), "");

        assertDoesNotThrow(cmd::run);
    }
}
