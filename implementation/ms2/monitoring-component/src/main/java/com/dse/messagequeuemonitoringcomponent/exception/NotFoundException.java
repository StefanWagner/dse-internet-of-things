package com.dse.messagequeuemonitoringcomponent.exception;

/**
 * Meant to be thrown if a requested resource is not available.
 * Handler will return a Code 404
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
