package com.dse.iotsimulationservice.sensor;

import com.dse.iotsimulationservice.service.SensorManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class MockFileAirSensorTest {

    @Mock
    SensorManager sensorManager;

    private MockFileAirSensor mockFileAirSensor;

    @BeforeEach
    void setup(){
        sensorManager = mock(SensorManager.class);

    }

    @Test
    void itShouldLoadDataCorrectly() {
        mockFileAirSensor = new MockFileAirSensor(sensorManager, "testairdata.csv");
        assertThat(mockFileAirSensor.srcValues).isNotNull().containsOnlyKeys("Oak Street Weather Station");
        assertThat(mockFileAirSensor.srcValues.get("Oak Street Weather Station")).hasSize(3);
        assertThat(mockFileAirSensor.srcValues.get("Oak Street Weather Station").get(0))
                .extracting("value1", "value2", "value3")
                .containsExactly(null, BigDecimal.valueOf(55), BigDecimal.valueOf(1.9));
    }

    @Test
    void itShouldOmitValuesCorrectly() {
        mockFileAirSensor = new MockFileAirSensor(sensorManager, "testairdata.csv");
        ReflectionTestUtils.setField(mockFileAirSensor, "identifier", "Oak Street Weather Station");
        mockFileAirSensor.notifyForValueChange();
        mockFileAirSensor.notifyForValueChange();
        verify(sensorManager, times(2)).valueOmitted(any());
    }


}