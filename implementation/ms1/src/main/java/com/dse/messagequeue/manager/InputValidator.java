package com.dse.messagequeue.manager;

import com.dse.messagequeue.exception.InconsistentSubscriberException;
import com.dse.messagequeue.exception.InvalidInputException;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.messagequeuemodel.requestwrapper.UnsubscribeRequestWrapper;

import java.util.Set;

public class InputValidator {
	/**
	 * Validates the given subscriber with:
	 * 	not null
	 * 	url valid
	 * 	delay not negative
	 * 	topics not empty
	 *
	 * @param subscriber subscriber to validate
	 */
	public static void validateSubscriber(Subscriber subscriber) {
		if (subscriber == null)
			throw new InvalidInputException("Given subscriber is null", "null");

		if (!subscriber.getUrl().matches("((http://)|(https://))([a-zA-Z]|[0-9]|-|\\.|_|\\+)+:[0-9]+"))
			throw new InvalidInputException("URL of given subscriber is not valid", subscriber.getUrl());

		if (subscriber.getDelayInMillis()<0)
			throw new InvalidInputException("Delay of given subscriber is not valid", subscriber.getUrl());

		if (subscriber.getTopics().isEmpty())
			throw new InvalidInputException("Topics of given subscriber are empty", subscriber.getUrl());
	}


	/**
	 * Validates an existing received subscriber on new topic subscription
	 *
	 * @param receivedSub subscriber of a microservice
	 * @param storedSub subscriber of the internal repo
	 */
	public static void validateSubscribingTopics(Subscriber receivedSub, Subscriber storedSub) {
		//check that delay is equal
		if (storedSub.checkIdentity(receivedSub)) {
			throw new InvalidInputException("Sent subscriber should contain new topics to subscribe, but instead was identical!", receivedSub.getUrl());
		}

		if (storedSub.getDelayInMillis() != receivedSub.getDelayInMillis()) {
			throw new InconsistentSubscriberException("Delay does not match with sent subscriber", storedSub, receivedSub);
		}

		Set<TopicEnum> storedTopics = storedSub.getTopics();
		Set<TopicEnum> sentTopics = receivedSub.getTopics();

		if (storedTopics.size() > sentTopics.size() || !sentTopics.containsAll(storedTopics)) {
			throw new InvalidInputException("Invalid topics in subscriber: there are subscribed or new to subscribe topics missing", receivedSub.getUrl());
		}
	}

	/**
	 * Validates the given Parameters when unsubscribing
	 *
	 * @param subscriber unchanged subscriber
	 * @param topics     topics to unsubscribe
	 */
	public static void validateUnsubscribingTopics(Subscriber subscriber, Set<TopicEnum> topics) {
		if (topics == null) {
			throw new InvalidInputException("Given topics are null", subscriber.getUrl());
		}

		//report error if subscriber didn't subscribe a topic he wants to unsubscribe
		if (!subscriber.getTopics().containsAll(topics)) {
			MetaManager.reportError(
					subscriber.getUrl()
					, "Subscriber wanted to unsubscribe topics, he didn't subscribed. " +
							"\nThis could be an indication that the microservice isn't functioning properly");
		}

		//report error if topics is empty
		if (topics.isEmpty()) {
			MetaManager.reportError(
					subscriber.getUrl()
					, "Subscriber wanted to unsubscribe topics, but the list of topics to unsubscribe is empty. " +
							"\nThis could be an indication that the microservice isn't functioning properly");
		}
	}

	/**
	 * Validates input when changing the delay of a subscriber
	 *
	 * @param subscriber received subscriber
	 * @param delayInMillis new delay
	 */
	public static void validateChangingDelay(Subscriber subscriber, int delayInMillis) {
		if (delayInMillis < 0) {
			throw new InvalidInputException("Given delay is negative", subscriber.getUrl());
		}

		//report error if the new delay is the same as the old one
		if (subscriber.getDelayInMillis() == delayInMillis) {
			MetaManager.reportError(
					subscriber.getUrl()
					, "Subscriber wanted to change his delay, but old and new are the same!" +
							"\nThis could be an indication that the microservice isn't functioning properly");
		}
	}

}
