package com.dse.iotsimulationservice.util;

import java.security.Permission;

//Implementation that throws a custom exception when the code tries to call System.exit(). Used to test if System.exit is correctly
//called in Unit tests.
//Idea and code found at: http://neverfear.org/blog/view/157/Testing_code_that_calls_System_exit_in_Java
public class ExitDeniedSecurityManager extends SecurityManager {

    public static final class ExitSecurityException extends SecurityException {
        private final int status;

        ExitSecurityException(final int status) {
            this.status = status;
        }

        public int getStatus() {
            return this.status;
        }
    }

    @Override
    public void checkExit(final int status) {
        throw new ExitSecurityException(status);
    }

    @Override
    public void checkPermission(final Permission perm) {
    }

}
