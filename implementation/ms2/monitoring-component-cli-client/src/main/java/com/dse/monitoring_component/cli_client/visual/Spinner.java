package com.dse.monitoring_component.cli_client.visual;

/**
 * A mechanism to print a visual spinner animation in a cli interface
 */
public enum Spinner {
    FIRST("(/)"),
    SECOND("(-)"),
    THIRD("(\\)");

    private final String symbol;

    Spinner(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    /**
     * @return returns the next state
     */
    public Spinner getNext() {
        switch (this) {
            case FIRST:
                return Spinner.SECOND;
            case SECOND:
                return Spinner.THIRD;
            default:
                return Spinner.FIRST;
        }
    }
}
