package io.github.lukas_krickl.cli_client.commands;


import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Responsible for parsing user input to the corresponding command
 */
public class CommandParser {
	private final TreeSet<Command> commandList;

	public CommandParser(TreeSet<Command> commandList) {
		this.commandList = commandList;
	}

	public Command parseUserInput(String inputLine) {
		String[] inputEntities = inputLine.split(" ");

		String inputSyntax = inputEntities[0];
		ArrayList<String> inputParams = parseInputParams(inputEntities);
		String inputArgs = parseInputArgs(inputEntities);

		for (Command cmd : commandList) {
			//check if the syntax keyword matches a command
			if (cmd.matchesSyntax(inputSyntax)) {
				//check if all parameters are valid
				for (String inputParameter : inputParams) {
					//throw exception if input parameter is not valid with this command
					if (!cmd.hasParameter(inputParameter)) {
						throw new IllegalArgumentException("Parameter: " + inputParameter + " is not valid with command: " + cmd.getCmdInfo().getName());
					}
				}

				//check if this command requires arguments
				if (cmd.getCmdInfo().getArgsRequired()==ArgsRequired.REQUIRED && inputArgs.isEmpty()) {
					throw new IllegalArgumentException("This command requires one or more argument at the end!");
				}

				if (cmd.getCmdInfo().getArgsRequired()==ArgsRequired.NOT_REQUIRED && !inputArgs.isEmpty()) {
					throw new IllegalArgumentException("This command does not require arguments at the end!");
				}

				//make command callable, ready for execution
				return cmd.makeCallable(inputParams, inputArgs);
			}
		}
		throw new IllegalArgumentException("Command: '" + inputSyntax + "' was not found! Try 'help' for further information.\n");
	}

	/**
	 * Parses an input string and returns and list with the parameters found(signaled with an leading -) eg. '-p' or '-t'
	 *
	 * @param inputEntities user input separated with whitespaces
	 * @return list of found parameters
	 */
	private ArrayList<String> parseInputParams(String[] inputEntities) {
		ArrayList<String> inputParams = new ArrayList<>();
		for (String entity : inputEntities) {
			if (entity.startsWith("-")) {
				inputParams.add(entity);
			}
		}
		return inputParams;
	}

	/**
	 * Takes the input entities and returns the value/data string from the end of the input
	 *
	 * @param inputEntities
	 * @return
	 */
	private String parseInputArgs(String[] inputEntities) {
		StringBuilder argsBuilder = new StringBuilder();

		for (int i = 1; i < inputEntities.length; i++) {
			if (!inputEntities[i].startsWith("-")) {
				argsBuilder.append(inputEntities[i]).append(" ");
			}
		}

		return argsBuilder.toString().trim();
	}
}
