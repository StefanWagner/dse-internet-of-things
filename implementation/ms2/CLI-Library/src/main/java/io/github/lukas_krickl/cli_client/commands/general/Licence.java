package io.github.lukas_krickl.cli_client.commands.general;

import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;

public class Licence extends Command {
	public Licence() {
		super(new CommandInfoBuilder("LICENCE")
				.addSyntax("licence")
				.setDescription("Displays the licence information")
				.setRequiresArgs(ArgsRequired.NOT_REQUIRED)
				.build());
	}

	@Override
	public void run() {
		UserInterface ui = UserInterface.getInstance();
		UserInterface.printSection(ui.getOutputText().getLicence());
	}
}
