package com.dse.resultoutputservice.datatypes;

import java.util.ArrayDeque;

/**
 * The WaterPredictions class consists out of a set of waterPrediction objects.
 * It is used for bundling the database data which will be sent to the browser.
 */
public class WaterPredictions {
    private ArrayDeque<WaterPrediction> waterPredictions;

    public WaterPredictions() {
        this.waterPredictions = new ArrayDeque<WaterPrediction>();
    }

    public ArrayDeque<WaterPrediction> getWaterPredictions() {
        return waterPredictions;
    }

    public void addWaterPrediction(WaterPrediction waterPrediction) {
        waterPredictions.add(waterPrediction);
    }
}
