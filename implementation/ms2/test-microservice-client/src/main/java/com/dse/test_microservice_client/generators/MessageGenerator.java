package com.dse.test_microservice_client.generators;

import com.dse.messagequeuemodel.ErrorData;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.TerminationData;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.test_microservice_client.application.Configuration;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class MessageGenerator {
    public static String getRandomURL() {
        return "http://" + UUID.randomUUID() + ":8080";
    }

    public static Message getRandomErrorMessage() {
        String errorMessage = "Test Error Message: " + UUID.randomUUID().toString();
        ErrorData data = new ErrorData(Configuration.TEST_CLIENT_URL, "not a real error, just a test message", errorMessage);
        return new Message(TopicEnum.ERRORS, data);
    }

    /**
     * Returns a termination message with one random target URL
     *
     * @return termination message
     */
    public static Message getRandomTerminationMessage() {
        HashSet<String> targets = new HashSet<>();
        targets.add("http://test-termination-target-" + UUID.randomUUID().toString());

        TerminationData data = new TerminationData(targets);
        return new Message(TopicEnum.TERMINATION, data);
    }

    /**
     * Returns a termination message with the given amount of targets
     * URLs are created randomly
     *
     * @param amountOfTargets amount of targets
     * @return termination message
     */
    public static Message getRandomTerminationMessage(int amountOfTargets) {
        HashSet<String> targets = new HashSet<>();

        for (int i = 0; i < amountOfTargets; i++) {
            targets.add("http://test-termination-target-" + UUID.randomUUID().toString());
        }

        TerminationData data = new TerminationData(targets);
        return new Message(TopicEnum.TERMINATION, data);
    }

    /**
     * Returns a termination message with one target, the given url
     *
     * @param targetURL target
     * @return termination message
     */
    public static Message getTerminationMessage(String targetURL) {
        HashSet<String> targets = new HashSet<>();
        targets.add(targetURL);

        TerminationData data = new TerminationData(targets);
        return new Message(TopicEnum.TERMINATION, data);
    }

    /**
     * Returns a termination message with one target, the given url
     *
     * @param targets targets
     * @return termination message
     */
    public static Message getTerminationMessage(Set<String> targets) {
        TerminationData data = new TerminationData(targets);
        return new Message(TopicEnum.TERMINATION, data);
    }


}
