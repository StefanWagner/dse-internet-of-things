package com.dse.monitoring_component.cli_client.test.logic;

import com.dse.monitoring_component.cli_client.exceptions.MCNotReachableException;
import com.dse.monitoring_component.cli_client.logic.NetworkConnector;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;

class NetworkConnectorTest {

    @ParameterizedTest
    @ValueSource(strings = {
            "http://localhost:8080",
            "http://127.0.0.1:4567",
            "https://255.255.255.255:123456",
            "https://more-urls.com:3467"})
    void validUrl_checkURL_returnsTrue(String url) {
        assertTrue(NetworkConnector.checkURL(url));

    }

    @ParameterizedTest
    @ValueSource(strings = {
            "htt://localhost:8080",
            "http//127.0.0.1:4567",
            "https:255.255.255.255:123456",
            "more-urls.com",
            ""," ",
            "www.google.com",
            "http://localhost",
            "https://local öüäö:4567"})
    void notValidUrl_checkURL_returnsFalse(String url) {
        assertFalse(NetworkConnector.checkURL(url));
    }

    @Test
    void mcNotReachable_throwsMCNotReachableException() {
        NetworkConnector connector = new NetworkConnector("http://not-the-real-url:8080");

        assertThrows(IOException.class, connector::requestStatus);
        assertThrows(IOException.class, connector::requestCache);
        assertThrows(IOException.class, ()->{connector.requestErrors(10);});
        assertThrows(IOException.class, connector::requestAllErrors);
        assertThrows(IOException.class, ()->{connector.requestClearErrors(10);});
        assertThrows(IOException.class, connector::requestClearAllErrors);
        assertThrows(IOException.class, connector::requestSubscribers);
        assertThrows(IOException.class, ()->{connector.requestTermination(new String[]{"http://target:8080"});});
    }
}