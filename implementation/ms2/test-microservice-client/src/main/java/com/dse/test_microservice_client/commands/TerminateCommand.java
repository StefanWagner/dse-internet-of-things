package com.dse.test_microservice_client.commands;

import com.dse.messagequeuemodel.Subscriber;
import com.dse.test_microservice_client.application.TestClientRepository;
import com.dse.test_microservice_client.generators.MessageGenerator;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;

import java.util.HashSet;
import java.util.Set;


public class TerminateCommand extends Command {
    private TestClientRepository repo;

    public TerminateCommand() {
        super(new CommandInfoBuilder("TERMINATE")
                .addSyntax("terminate")
                .setDescription("Terminate one or more subscribers.\n" +
                        "Target subscribers are given as an argument by either their URL\n" +
                        " or by their number in the 'subscribers -show' listing.\n" +
                        "Multiple targets are separated with a whitespace")
                .addParameterList()
                .addParameter("-all", "Terminate all subscribers created by this this client")
                .finishParameterList()
                .setRequiresArgs(ArgsRequired.OPTIONAL)
                .build());
    }

    @Override
    public void run() {
        repo = TestClientRepository.getInstance();
        if (getParams().isEmpty()) {
            //terminate external subs
            Set<String> urlInputSubs = getURLsFromInput(getArgs());
            Set<Integer> numberInputSubs = getNumbersFromInput(getArgs());

            for (int i : numberInputSubs) {
                Subscriber foundSub = repo.findSubscriberByNumber(i);
                if (foundSub != null) {
                    urlInputSubs.add(foundSub.getUrl());
                }
            }

            terminateSubscribers(urlInputSubs);
            UserInterface.printSection("Terminated subscribers: \n" + urlInputSubs.toString());

        } else if (getParams().contains("-all")) {

            terminateAllSubs();
            UserInterface.println("All test subscribers terminated!");
        } else {
            UserInterface.println("Invalid command usage, try 'help' for more information.");
        }
    }

    private void terminateSubscribers(Set<String> subs) {
        repo.getMqConnection().publishMessage(MessageGenerator.getTerminationMessage(subs));

        for (String subUrl : subs) {
            Subscriber foundSub = repo.findSubscriberByURL(subUrl);
            if (foundSub != null) {
                repo.getSubscribers().get(foundSub).unsubscribeFromTopics(foundSub.getTopics());
                repo.getSubscribers().remove(foundSub);
            }
        }
    }

    private void terminateAllSubs() {
        //unsubscribe all topics of all subscribers
        repo.getSubscribers().forEach(((subscriber, messageQueueClient) ->
                messageQueueClient.unsubscribeFromTopics(subscriber.getTopics())));

        repo.getSubscribers().clear();
    }

    private Set<Integer> getNumbersFromInput(String input) {
        String[] entities = input.split(" ");
        HashSet<Integer> subscriberNumbers = new HashSet<>();

        for (String e : entities) {
            if (e.matches("[0-9]+")) {
                subscriberNumbers.add(Integer.parseInt(e));
            }
        }

        return subscriberNumbers;
    }

    private Set<String> getURLsFromInput(String input) {
        Set<String> urls = new HashSet<>();
        String[] entities = input.split(" ");

        for (String e : entities) {
            if (e.matches("((http://)|(https://))([a-zA-Z]|[0-9]|\\.|-|_|\\+)+:[0-9]+")) {
                urls.add(e);
            }
        }
        return urls;
    }
}
