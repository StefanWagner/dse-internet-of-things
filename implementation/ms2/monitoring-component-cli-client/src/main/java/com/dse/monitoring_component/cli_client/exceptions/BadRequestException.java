package com.dse.monitoring_component.cli_client.exceptions;

import java.io.IOException;

/**
 * Exception thrown when the HttpErrorResponseHandler received a code 400 from the Monitoring Component
 */
public class BadRequestException extends IOException {
    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
