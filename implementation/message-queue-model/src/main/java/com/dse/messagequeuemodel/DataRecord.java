package com.dse.messagequeuemodel;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Classes implementing this interface can be set as the record field inside a Message instance.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "class")
public interface DataRecord {
}
