package com.dse.monitoring_component.cli_client.commands;

import com.dse.monitoring_component.cli_client.data.DAO;
import com.dse.monitoring_component.cli_client.logic.NetworkConnector;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

import java.io.IOException;

/**
 * Command for requesting the monitoring component to sent a termination message
 */
public class TerminateCommand extends Command {
    public TerminateCommand() {
        super(new CommandInfoBuilder("TERMINATE")
                .addSyntax("terminate")
                .setDescription("Tries to terminate all given services. Services are specified by their URL.")
                .setRequiresArgs(ArgsRequired.REQUIRED)
                .build());
    }

    @Override
    public void run() {
        String[] targets = getArgs().split(" ");
        //check validity of all urls
        for (String url : targets) {
            if (!NetworkConnector.checkURL(url)) {
                UserInterface.printSection(TextColor.RED.apply("FAILED: ") + url + " is not a valid URL!");
                return;
            }
        }

        try {
            DAO.getInstance().getConnector().requestTermination(targets);
            UserInterface.printSection(TextColor.GREEN.apply("SUCCESS: ") + "Termination message sent!");
        } catch (IOException e) {
            UserInterface.printSection(e.getMessage());
        }
    }
}
