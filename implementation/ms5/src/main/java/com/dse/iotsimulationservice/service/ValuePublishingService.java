package com.dse.iotsimulationservice.service;

import com.dse.iotsimulationservice.aggregator.Aggregator;
import com.dse.iotsimulationservice.aggregator.Filter;
import com.dse.iotsimulationservice.data.SensorValueRepository;
import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service responsible for preparing and publishing values to the message queue
 */
@Service
@Slf4j
public class ValuePublishingService {

    private final SensorValueRepository sensorValueRepository;

    private final Filter filter;

    private final Aggregator aggregator;

    private final MessageQueueClient messageQueueClient;

    private final TopicEnum topic;

    public ValuePublishingService(SensorValueRepository sensorValueRepository,
                                  Filter filter,
                                  Aggregator aggregator,
                                  MessageQueueClient messageQueueClient,
                                  @Value("${sensor.type}") final String type) {
        this.sensorValueRepository = sensorValueRepository;
        this.filter = filter;
        this.aggregator = aggregator;
        this.messageQueueClient = messageQueueClient;
        this.topic = getSensorType(type);

    }

    /**
     * Retrieves list of cached values from the repository, filters and aggregates them by a configurable strategy to a single value
     * and publishes this value to the message queue
     */
    void publishValues() {
        List<AggregableValue> cachedValues = this.sensorValueRepository.getAllCachedValues();
        this.sensorValueRepository.clearCache();
        List<AggregableValue> filteredValues = this.filter.filterValues(cachedValues);
        AggregableValue resultValue = this.aggregator.aggregateValues(filteredValues);
        SensorValue convertedResultValue;
        if (resultValue.getType().equals(SensorTypeEnum.AIR)) {
            convertedResultValue = AirSensorValue.fromAggregable(resultValue);
        } else if (resultValue.getType().equals(SensorTypeEnum.WATER)) {
            convertedResultValue = WaterSensorValue.fromAggregable(resultValue);
        } else {
            throw new IllegalArgumentException("Invalid type for Aggregable Value: " + resultValue.getType().getType());
        }

        Message msg = this.buildMessage(convertedResultValue);
        try {
            this.messageQueueClient.publishMessage(msg);
        } catch (ResourceAccessException e) {
            log.warn("Message queue could not be reached, trying to reconnect.");
            attemptReconnect();
        } catch (RestClientException e) {
            log.warn("Could not publish message. Discarding value and continuing. For error see next log entry");
            log.warn("Reason for publication error: " + e.getMessage());
        }
    }

    private TopicEnum getSensorType(String type) {
        if ("air".equals(type)) {
            return TopicEnum.AIRSENSORDATA;
        } else if ("water".equals(type)) {
            return TopicEnum.WATERSENSORDATA;
        } else {
            throw new IllegalArgumentException("Unknown type passed!");
        }
    }

    private Message buildMessage(SensorValue resultValue) {
        return new Message(topic, resultValue);
    }

    private void attemptReconnect() {
        Set<TopicEnum> topicEnums = new HashSet<>();
        topicEnums.add(TopicEnum.TERMINATION);
        boolean isSubscribed = false;
        while (!isSubscribed) {
            try {
                messageQueueClient.subscribeToTopics(topicEnums);
                isSubscribed = true;
            } catch (IllegalArgumentException e) {
                isSubscribed = true;
            } catch (RestClientException e) {
                log.warn("Error resubscribing to queue: " + e.getMessage());
            }
        }
    }
}
