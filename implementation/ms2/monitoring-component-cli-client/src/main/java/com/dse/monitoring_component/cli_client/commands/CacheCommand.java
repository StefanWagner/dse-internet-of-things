package com.dse.monitoring_component.cli_client.commands;

import com.dse.messagequeuemodel.TopicEnum;
import com.dse.monitoring_component.cli_client.data.DAO;
import com.dse.monitoring_component.cli_client.logic.NetworkConnector;
import com.dse.monitoring_component.cli_client.visual.Display;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

import java.io.IOException;
import java.util.Map;

/**
 * A Command for displaying cache information.
 */
public class CacheCommand extends Command {
    public CacheCommand() {
        super(new CommandInfoBuilder("CACHE")
                .addSyntax("cache")
                .setDescription("Displays detailed information about all cached messages of the Message Queue.\n" +
                        "Display is refreshed at configured refresh rate")
                .addParameterList()
                .addParameter("-r","Continuously refreshes the output" +
                        " at configured refresh rate")
                .finishParameterList()
                .setRequiresArgs(ArgsRequired.NOT_REQUIRED)
                .build());
    }

    @Override
    public void run() {
        if(getParams().contains("-r")) {
            displayCacheContinuously();

        } else if (getParams().isEmpty()) {
            displayCacheOnce();
        }
    }

    /**
     * Requests information about cache size of the monitoring component and
     * displays the output once
     */
    private void displayCacheOnce(){
        NetworkConnector connector = DAO.getInstance().getConnector();
        try {
            Map<TopicEnum, Integer> cache = connector.requestCache();
            UserInterface.print(Display.formatCache(cache));

        } catch (IOException e) {
            UserInterface.printSection(TextColor.RED.apply("ERROR: ")+e.getMessage());
        }
    }

    /**
     * Starts a thread displaying output and stops when enter is pressed
     */
    private void displayCacheContinuously(){
        UserInterface ui = UserInterface.getInstance();
        Thread displayCache = createDisplayingThread();

        //Start thread to continuously display status
        displayCache.start();
        //listen for user to press enter to stop the thread
        ui.getUserConfirmation();
        //stop the thread
        displayCache.interrupt();

    }

    /**
     * Requests and displays cache information until the thread is interrupted
     * @return thread which displays cache info
     */
    private Thread createDisplayingThread(){
        return new Thread(()->{
            NetworkConnector connector = DAO.getInstance().getConnector();
            int refreshRate = DAO.getInstance().getRefreshRate();
            try {
                while (true) {
                    Map<TopicEnum, Integer> cache = connector.requestCache();
                    UserInterface.print(Display.formatToNewPage());
                    UserInterface.print(Display.formatCache(cache));

                    Thread.sleep(refreshRate);
                }

            } catch (IOException e) {
                UserInterface.printSection(TextColor.RED.apply("ERROR: ")+e.getMessage());
            } catch (InterruptedException ignored) {
            }
        });
    }
}
