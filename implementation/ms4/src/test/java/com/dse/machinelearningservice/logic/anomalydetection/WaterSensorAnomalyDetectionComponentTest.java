package com.dse.machinelearningservice.logic.anomalydetection;

import com.dse.machinelearningservice.logic.anomalydetection.WaterSensorAnomalyDetectionComponent;
import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.WaterSensorPredictionDataRecord;
import com.dse.messagequeuemodel.WaterSensorValue;
import com.dse.messagequeuemodel.SensorTypeEnum;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static org.junit.Assert.assertEquals;

public class WaterSensorAnomalyDetectionComponentTest {

    private WaterSensorPredictionDataRecord prediction;
    private MessageQueueClient client; 

    @Before
    public void setUp() {
        prediction = new WaterSensorPredictionDataRecord(BigDecimal.valueOf(20),
                BigDecimal.valueOf(5), BigDecimal.valueOf(12)); //temperature, turbidity, waveHeight
        client = null; //setting the message queue client to null --> no messages will be sent, only local testing
    }

    //Null value anomalies

    @Test
    public void testNullValueAnomalyWithCorrectInput() {
        WaterSensorValue record = new WaterSensorValue(Instant.now(), SensorTypeEnum.WATER, "BeachName1",
                BigDecimal.valueOf(20), BigDecimal.valueOf(5), BigDecimal.valueOf(12));

        WaterSensorAnomalyDetectionComponent waterSensorAnomalyDetectionComponent =
                new WaterSensorAnomalyDetectionComponent();
        waterSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int nullValueAnomaliesFound = waterSensorAnomalyDetectionComponent.findNullValueAnomaly();
        assertEquals(nullValueAnomaliesFound, 0);
    }

    @Test
    public void testNullValueAnomalyWithNullInput() {
        WaterSensorValue record = new WaterSensorValue(Instant.now(), SensorTypeEnum.WATER, "BeachName1",
                null, null, null);

        WaterSensorAnomalyDetectionComponent waterSensorAnomalyDetectionComponent =
                new WaterSensorAnomalyDetectionComponent();
        waterSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int nullValueAnomaliesFound = waterSensorAnomalyDetectionComponent.findNullValueAnomaly();
        assertEquals(nullValueAnomaliesFound, 3);
    }


    //Out of bounds anomalies

    @Test
    public void testOutOfBoundsAnomalyWithCorrectInput() {
        WaterSensorValue record = new WaterSensorValue(Instant.now(), SensorTypeEnum.WATER, "BeachName1",
                BigDecimal.valueOf(20), BigDecimal.valueOf(5), BigDecimal.valueOf(12));

        WaterSensorAnomalyDetectionComponent waterSensorAnomalyDetectionComponent =
                new WaterSensorAnomalyDetectionComponent();
        waterSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int outOfBoundsAnomaliesFound = waterSensorAnomalyDetectionComponent.findOutOfBoundsAnomaly();
        assertEquals(outOfBoundsAnomaliesFound, 0);
    }

    @Test
    public void testOutOfBoundsAnomalyWithInvalidInput() {
        WaterSensorValue record = new WaterSensorValue(Instant.now(), SensorTypeEnum.WATER, "BeachName1",
                BigDecimal.valueOf(60), BigDecimal.valueOf(-5), BigDecimal.valueOf(1112));

        WaterSensorAnomalyDetectionComponent waterSensorAnomalyDetectionComponent =
                new WaterSensorAnomalyDetectionComponent();
        waterSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int outOfBoundsAnomaliesFound = waterSensorAnomalyDetectionComponent.findOutOfBoundsAnomaly();
        assertEquals(outOfBoundsAnomaliesFound, 3);
    }



    //Prediction anomalies

    @Test
    public void testPredictionAnomalyWithCorrectInput() {
        WaterSensorValue record = new WaterSensorValue(Instant.now(), SensorTypeEnum.WATER, "BeachName1",
                BigDecimal.valueOf(29), BigDecimal.valueOf(6), BigDecimal.valueOf(11));

        WaterSensorAnomalyDetectionComponent waterSensorAnomalyDetectionComponent =
                new WaterSensorAnomalyDetectionComponent();
        waterSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int predictionAnomaliesFound = waterSensorAnomalyDetectionComponent.findPredictionAnomaly();
        assertEquals(predictionAnomaliesFound, 0);
    }

    @Test
    public void testPredictionAnomalyWithInvalidInput() {
        WaterSensorValue record = new WaterSensorValue(Instant.now(), SensorTypeEnum.WATER, "BeachName1",
                BigDecimal.valueOf(31), BigDecimal.valueOf(66), BigDecimal.valueOf(80));

        WaterSensorAnomalyDetectionComponent waterSensorAnomalyDetectionComponent =
                new WaterSensorAnomalyDetectionComponent();
        waterSensorAnomalyDetectionComponent.setUp(record, prediction, client);

        int predictionAnomaliesFound = waterSensorAnomalyDetectionComponent.findPredictionAnomaly();
        assertEquals(predictionAnomaliesFound, 3);
    }




}
