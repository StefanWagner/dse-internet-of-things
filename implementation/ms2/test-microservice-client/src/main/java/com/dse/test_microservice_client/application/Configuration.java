package com.dse.test_microservice_client.application;

public class Configuration {
    public static final String TEST_CLIENT_URL = "http://test-client-url:8080";
    public static final String DEFAULT_MQ_URL = "http://127.0.0.1:4000";
}
