package com.dse.resultoutputservice;

import com.dse.messagequeuemodel.*;
import com.dse.resultoutputservice.database.DBManager;
import com.dse.resultoutputservice.network.messagequeuecommunication.MessageQueueCommunication;
import com.dse.resultoutputservice.network.webcommunication.WebRestController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

@SpringBootTest
public class MessageQueueCommunicationTests {
    @Autowired
    private MessageQueueCommunication messageQueueCommunication;
    private final static String databaseUrl = "jdbc:sqlite:../ms3/database/testDatabase.db";
    private DBManager dbManager = new DBManager();

    @BeforeEach
    void testDatabasePreparation() {
        ReflectionTestUtils.setField(dbManager, "databaseUrl", "jdbc:sqlite:../ms3/database/testDatabase.db");
        ArrayList<String> sqlStatements = new ArrayList<String>(Arrays.asList(
                "drop table anomalies;",
                "drop table airPredictions;",
                "drop table waterPredictions;",
                "create table anomalies(\n" +
                        "  id integer primary key autoincrement,\n" +
                        "  timestamp text,\n" +
                        "  errorType text,\n" +
                        "  errorDescription text\n" +
                        ");",
                "create table airPredictions(\n" +
                        "  id integer primary key autoincrement,\n" +
                        "  timestamp text,\n" +
                        "  airTemperature integer,\n" +
                        "  windSpeed integer,\n" +
                        "  humidity integer\n" +
                        ");",
                "create table waterPredictions(\n" +
                        "  id integer primary key autoincrement,\n" +
                        "  timestamp text,\n" +
                        "  waterTemperature integer,\n" +
                        "  turbidity integer,\n" +
                        "  waveHeight integer\n" +
                        ");"
        ));
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            for (String sql : sqlStatements) {
                boolean resultSet = statement.execute(sql);
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    int executeQuery(String sql) {
        Integer rowcount = null;
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            rowcount = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowcount;
    }

    @Test
    void processMessage_shouldInsertAnAnomalyToTestDatabase() {
        Message message = new Message(TopicEnum.ANOMALY, new AnomalyDataRecord(ErrorTypeEnum.NULL, "errorDescription Test"));
        messageQueueCommunication.processMessage(message);
        String sql = "select count(id) as rowcount from anomalies;";
        assertEquals(1, this.executeQuery(sql));
    }

    @Test
    void processMessage_shouldInsertAnAirPredictionToTestDatabase() {
        Message message = new Message(TopicEnum.PREDICTION, new AirSensorPredictionDataRecord(new BigDecimal(1), new BigDecimal(1), new BigDecimal(1)));
        messageQueueCommunication.processMessage(message);
        String sql = "select count(id) as rowcount from airPredictions;";
        assertEquals(1, this.executeQuery(sql));
    }

    @Test
    void processMessage_shouldInsertAWaterPredictionToTestDatabase() {
        Message message = new Message(TopicEnum.PREDICTION, new WaterSensorPredictionDataRecord(new BigDecimal(1), new BigDecimal(1), new BigDecimal(1)));
        messageQueueCommunication.processMessage(message);
        String sql = "select count(id) as rowcount from waterPredictions;";
        assertEquals(1, this.executeQuery(sql));
    }

}
