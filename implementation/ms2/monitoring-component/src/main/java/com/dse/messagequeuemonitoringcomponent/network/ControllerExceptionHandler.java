package com.dse.messagequeuemonitoringcomponent.network;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemonitoringcomponent.exception.InvalidInputException;
import com.dse.messagequeuemonitoringcomponent.exception.InvalidMessageReceivedException;
import com.dse.messagequeuemonitoringcomponent.exception.MQConnectionFailedException;
import com.dse.messagequeuemonitoringcomponent.exception.NotFoundException;
import com.dse.messagequeuemonitoringcomponent.managers.ApplicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception handler for the CLIClientController
 *
 */
@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    @Autowired
    ApplicationManager applicationManager;
    private Logger log = LoggerFactory.getLogger(ControllerExceptionHandler.class);


    /**
     * Message Queue is not available
     *
     * @param e Exception with information
     * @return Code 503 Service unavailable
     */
    @ExceptionHandler({MQConnectionFailedException.class})
    public ResponseEntity<String> handleMQConnectionFailedException(MQConnectionFailedException e) {
        log.error("Returning mq connection failed", e);
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.SERVICE_UNAVAILABLE);
    }

    /**
     * Input parameter nor legal
     *
     * @param e Exception with information
     * @return 400 Bad Request
     */
    @ExceptionHandler({InvalidInputException.class})
    public ResponseEntity<String> handleInvalidInputException(InvalidInputException e) {
        log.warn(e.getMessage(), e);
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Requested resource not found
     *
     * @param e Exception with information
     * @return code 404 Not Found
     */
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<String> handleNotFoundException(NotFoundException e) {
        log.warn(e.getMessage(), e);
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND); //FIXME testwise with message
    }

    /**
     * Reports an inconsistency with an error message to the message queue
     * Message is in the wrong topic
     *
     * @param e Exception with information
     * @return Unsupported media type
     */
    @ExceptionHandler({InvalidMessageReceivedException.class})
    public ResponseEntity<String> handleInvalidMessageReceivedException(InvalidMessageReceivedException e) {
        log.warn(e.getMessage(), e);
        Message message = e.getMqMessage();
        applicationManager.reportExternalErrorToMessageQueue(message.toString(), e.getMessage());
        return new ResponseEntity<>(
                e.getMessage()+e.getMqMessage().toString()
                , new HttpHeaders()
                , HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     * Handle unexpected exception
     *
     * @param e Exception with information
     * @return Internal server error
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<String> handleOtherException(Exception e) {
        log.error("Internal exception: {}", e.getMessage(), e);
        applicationManager.reportInternalErrorToMessageQueue(e);
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
