package com.dse.resultoutputservice.network.messagequeuecommunication;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeueclient.MessageQueueClientReceiveController;
import com.dse.messagequeueclient.MessageReceiver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The MessageQueueClientConfiguration class is needed for setting up the communication between message queue and the micro service.
 */
@Configuration
public class MessageQueueClientConfiguration {
    /**
     * The messageQueueClient method will be executed by the spring framework whenever a new messageQueueClient object is needed.
     *
     * @param clientUrl       string is defined in the application.properties file
     * @param messageQueueUrl string is defined in the application.properties file
     * @return an MessageQueueClient object with the right class variables
     */
    @Bean
    public MessageQueueClient messageQueueClient(@Value("${messagequeue.client.clienturl}") String clientUrl,
                                                 @Value("${messagequeue.client.messagequeueurl}") String messageQueueUrl) {
        return new MessageQueueClient(clientUrl, messageQueueUrl);
    }

    /**
     * The messageQueueClientReceiveController method will be executed by the spring framework whenever a new
     * messageQueueClientReceiveController object is needed.
     *
     * @param messageReceiver an object from the imported message-queue-client project
     * @return an object from the imported message-queue-client project
     */
    @Bean
    public MessageQueueClientReceiveController messageQueueClientReceiveController(MessageReceiver messageReceiver) {
        return new MessageQueueClientReceiveController(messageReceiver);
    }

}
