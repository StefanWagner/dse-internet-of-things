package com.dse.messagequeueclient;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.messagequeuemodel.requestwrapper.ChangeDelayRequestWrapper;
import com.dse.messagequeuemodel.requestwrapper.UnsubscribeRequestWrapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class MessageQueueClientTest {

    @Mock
    private RestTemplate restTemplate;

    private MessageQueueClient messageQueueClient;

    @BeforeEach
    void setUp() {
        //publish message call
        MockitoAnnotations.initMocks(this);
        when(restTemplate.exchange(any(), any(), any(), eq(Subscriber.class)))
                .thenAnswer(func -> {
                    Object body = ((RequestEntity) func.getArgument(2)).getBody();
                    if (body instanceof UnsubscribeRequestWrapper) {
                        Subscriber responseBody = ((UnsubscribeRequestWrapper) body).getSubscriber();
                        Set<TopicEnum> topics = responseBody.getTopics();
                        topics.removeAll(((UnsubscribeRequestWrapper) body).getTopics());
                        responseBody.setTopics(topics);
                        return ResponseEntity.ok(responseBody);
                    } else if (body instanceof ChangeDelayRequestWrapper) {
                        Subscriber responseBody = ((ChangeDelayRequestWrapper) body).getSubscriber();
                        responseBody.setDelayInMillis(((ChangeDelayRequestWrapper) body).getDelayInMillis());
                        return ResponseEntity.ok(responseBody);
                    } else {
                        return null;
                    }

                });


        when(restTemplate.postForObject(anyString(), any(Subscriber.class), any()))
                .thenAnswer(func -> func.getArgument(1));
        messageQueueClient = new MessageQueueClient(
                "http://my-url.com:8080",
                "http://message-queue-url.com:8080",
                restTemplate);

    }

    @Test
    void subscribeToTopicsIfNoSubExists() {
        assumeThat(!messageQueueClient.getSubscriber().isPresent());
        Set<TopicEnum> topics = getTopicSet(TopicEnum.PREDICTION, TopicEnum.AIRSENSORDATA);

        messageQueueClient.subscribeToTopics(topics);
        assertThat(messageQueueClient.getSubscriber()).isPresent().get().hasFieldOrPropertyWithValue("topics", topics);
        verify(restTemplate, times(1)).postForObject(anyString(), any(Subscriber.class), eq(Subscriber.class));
    }

    @Test
    void subscribeToTopicsIfSubExists() {
        assumeThat(!messageQueueClient.getSubscriber().isPresent());
        Set<TopicEnum> topics = getTopicSet(TopicEnum.PREDICTION, TopicEnum.AIRSENSORDATA);
        messageQueueClient.subscribeToTopics(topics);
        assumeThat(messageQueueClient.getSubscriber()).isPresent();

        Set<TopicEnum> updatedTopics = getTopicSet(TopicEnum.TERMINATION);
        messageQueueClient.subscribeToTopics(updatedTopics);

        verify(restTemplate, times(2)).postForObject(anyString(), any(Subscriber.class), eq(Subscriber.class));
        assertThat(messageQueueClient.getSubscriber()).isPresent();
        assertThat(messageQueueClient.getSubscriber()
                .get()
                .getTopics())
                .containsExactlyInAnyOrder(
                        TopicEnum.PREDICTION,
                        TopicEnum.AIRSENSORDATA,
                        TopicEnum.TERMINATION
                );

    }


    @Test
    void unsubscribeFromTopics() {
        //setup
        Set<TopicEnum> topics = getTopicSet(TopicEnum.PREDICTION, TopicEnum.AIRSENSORDATA);
        messageQueueClient.subscribeToTopics(topics);
        assumeThat(messageQueueClient.getSubscriber()).isPresent();

        Set<TopicEnum> toUnsubscribe = getTopicSet(TopicEnum.AIRSENSORDATA);
        messageQueueClient.unsubscribeFromTopics(toUnsubscribe);
        assertThat(messageQueueClient.getSubscriber()).isPresent();
        assertThat(messageQueueClient.getSubscriber()
                .get()
                .getTopics())
                .containsOnly(TopicEnum.PREDICTION);
        verify(restTemplate, times(1)).exchange(any(), any(), any(), eq(Subscriber.class));

    }

    @Test
    void unsubscribeWithoutSubscriberThrows() {
        assumeThat(!messageQueueClient.getSubscriber().isPresent());

        Set<TopicEnum> topics = getTopicSet(TopicEnum.PREDICTION, TopicEnum.AIRSENSORDATA);
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> messageQueueClient.unsubscribeFromTopics(topics));
        verify(restTemplate, times(0)).exchange(any(), any(), any(), eq(Subscriber.class));

    }

    @Test
    void unsubscribeFromAllTopicsMakesOptionalEmpty() {
        Set<TopicEnum> topics = getTopicSet(TopicEnum.PREDICTION, TopicEnum.AIRSENSORDATA);
        messageQueueClient.subscribeToTopics(topics);
        assumeThat(messageQueueClient.getSubscriber()).isPresent();

        Set<TopicEnum> toUnsubscribe = getTopicSet(TopicEnum.AIRSENSORDATA, TopicEnum.PREDICTION);
        messageQueueClient.unsubscribeFromTopics(toUnsubscribe);
        assertThat(messageQueueClient.getSubscriber()).isEmpty();
        verify(restTemplate, times(1)).exchange(any(), any(), any(), eq(Subscriber.class));
    }

    @Test
    void delayMessages() {
        Set<TopicEnum> topics = getTopicSet(TopicEnum.PREDICTION, TopicEnum.AIRSENSORDATA);
        messageQueueClient.subscribeToTopics(topics);
        assumeThat(messageQueueClient.getSubscriber()).isPresent();

        messageQueueClient.delayMessages(1111);

        assertThat(messageQueueClient.getSubscriber())
                .isPresent()
                .get()
                .hasFieldOrPropertyWithValue("delayInMillis", 1111);
        verify(restTemplate, times(1)).exchange(any(), any(), any(), eq(Subscriber.class));

    }

    @Test
    void delayMessagesThrowsIfNoSubscriberExists() {
        assumeThat(messageQueueClient.getSubscriber()).isEmpty();
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> messageQueueClient.delayMessages(1111));
        verify(restTemplate, times(0)).exchange(any(), any(), any(), eq(Subscriber.class));

    }

    @Test
    void publishMessage() {
        messageQueueClient.publishMessage(new Message(TopicEnum.TERMINATION, null));
        verify(restTemplate, times(1)).postForObject(anyString(), any(Message.class), eq(String.class));
    }


    private Set<TopicEnum> getTopicSet(TopicEnum... topics) {
        return Arrays.stream(topics)
                .collect(Collectors.toSet());
    }
}