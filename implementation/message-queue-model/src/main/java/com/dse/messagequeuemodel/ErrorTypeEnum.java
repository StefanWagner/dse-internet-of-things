package com.dse.messagequeuemodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * Enum to specify the reason for an AnomalyDataRecord
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ErrorTypeEnum {
    NULL("NULL"),
    VALUEOFFLIMIT("VALUEOFFLIMIT"),
    PREDICTION("PREDICTION");

    private String type;

    /**
     * @param type The case-insensitive string that will be mapped to the Enum
     * @return the enum corresponding to the string, or null if no match was found
     */
    @JsonCreator
    public static ErrorTypeEnum fromType(@JsonProperty("type") String type) {
        return Arrays.stream(ErrorTypeEnum.values())
                .filter(e -> e.type.equalsIgnoreCase(type))
                .findFirst().orElse(null);
    }
}