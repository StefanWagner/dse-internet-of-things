package com.dse.messagequeuemodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * DataRecord implementation describing data recorded by a water sensor.
 */
@Value
@EqualsAndHashCode(callSuper = true)
public class WaterSensorValue extends SensorValue implements DataRecord{
    private String beachName;
    private BigDecimal waterTemperature;
    private BigDecimal turbidity;
    private BigDecimal waveHeight;

    @Builder
    @JsonCreator
    @ConstructorProperties({"timeStamp", "sensorType", "beachName", "waterTemperature", "turbidity", "waveHeight"})
    public WaterSensorValue(Instant timeStamp,
                            SensorTypeEnum sensorType,
                            String beachName,
                            BigDecimal waterTemperature,
                            BigDecimal turbidity,
                            BigDecimal waveHeight) {
        super(timeStamp, sensorType);
        this.beachName = beachName;
        this.waterTemperature = waterTemperature;
        this.turbidity = turbidity;
        this.waveHeight = waveHeight;

    }

    public static AggregableValue toAggregable(WaterSensorValue waterSensorValue) {
        return AggregableValue.builder()
                .identifier(waterSensorValue.getBeachName())
                .timeStamp(waterSensorValue.getTimeStamp())
                .value1(waterSensorValue.getWaterTemperature())
                .value2(waterSensorValue.getTurbidity())
                .value3(waterSensorValue.getWaveHeight())
                .type(waterSensorValue.getSensorType())
                .build();
    }

    public static WaterSensorValue fromAggregable(AggregableValue aggregableValue) {
        return WaterSensorValue.builder()
                .beachName(aggregableValue.getIdentifier())
                .timeStamp(aggregableValue.getTimeStamp())
                .waterTemperature(aggregableValue.getValue1())
                .turbidity(aggregableValue.getValue2())
                .waveHeight(aggregableValue.getValue3())
                .sensorType(aggregableValue.getType())
                .build();
    }
}
