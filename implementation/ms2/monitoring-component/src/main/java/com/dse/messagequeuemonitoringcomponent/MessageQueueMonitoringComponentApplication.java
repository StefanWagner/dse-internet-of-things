package com.dse.messagequeuemonitoringcomponent;

import com.dse.messagequeuemonitoringcomponent.configuration.ApplicationConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.util.Properties;

/**
 * Can optionally set the ip addresses with
 * ip address of message queue server and ip address of this machine
 * Runs and listens on localhost otherwise
 *
 */
@SpringBootApplication
public class MessageQueueMonitoringComponentApplication {
    public static void main(String[] args) {
        checkArguments(args);

        SpringApplication.run(MessageQueueMonitoringComponentApplication.class, args);
    }

    /**
     * Checks if the given args contain mq and mc server ip address
     * Validates ip addresses only at a formal level, so 999.999.999.999 would be formally valid
     * @param args vm args
     */
    private static void checkArguments(String[] args){
        //validate and set arguments
        if(args.length==2) {
            boolean valid = true;

            for(int i = 0; i<2; i++) {
                if (!args[i].matches("([0-9]{1,3}\\.){3}[0-9]{1,3}")) {
                    valid = false;
                    System.out.println("\nGiven arguments should be ip address of the message queue server\n" +
                            "and ip address of this machine!");
                }
            }
            if (valid) {
                ApplicationConfiguration.messageQueueIPAddress = args[0];
                ApplicationConfiguration.clientIPAddress = args[1];
            }
        }
    }

}
