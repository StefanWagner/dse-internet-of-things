package com.dse.monitoring_component.cli_client;
import com.dse.monitoring_component.cli_client.commands.*;
import com.dse.monitoring_component.cli_client.data.DAO;
import com.dse.monitoring_component.cli_client.logic.InitialisationRoutine;
import com.dse.monitoring_component.cli_client.visual.ClientOutputText;
import io.github.lukas_krickl.cli_client.builder.application.ApplicationManagerBuilder;
import io.github.lukas_krickl.cli_client.logic.ApplicationManager;
import io.github.lukas_krickl.cli_client.logic.UserInterface;


/**
 * Application can be run optionally with the monitoring component in the starting arguments
 */
public class MainApp {
    public static void main(String[] args) {
        if (args.length == 1) {
            String url = args[0];
            try {
                DAO.getInstance().getConnector().setMonitoringComponentURL(url);
                UserInterface.printSection("Monitoring Component URL set to " + url);

            } catch (IllegalArgumentException e) {
                UserInterface.printSection(
                        e.getMessage() +
                                "\nProceeding with default configuration.");
            }
        }

        //Configures all commands to the CLI-Library application manager
        ApplicationManager applicationManager = new ApplicationManagerBuilder()
                .addInitialisationRoutine(new InitialisationRoutine())
                .setupOutputText(new ClientOutputText())
                .addCommand(new CacheCommand())
                .addCommand(new ChangeURLCommand())
                .addCommand(new ErrorsCommand())
                .addCommand(new RefreshRateCommand())
                .addCommand(new ServicesCommand())
                .addCommand(new StatusCommand())
                .addCommand(new TerminateCommand())
                .build();

        applicationManager.start();
    }
}
