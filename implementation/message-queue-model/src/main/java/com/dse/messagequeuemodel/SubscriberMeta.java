package com.dse.messagequeuemodel;

import lombok.Value;

/**
 * Metadata about a new subscription
 */
@Value
public class SubscriberMeta implements DataRecord {
    private Subscriber sub;
    private Status status;

    public enum Status {
        CREATED,
        CHANGED,
        DELETED
    }

}
