package com.dse.resultoutputservice.datatypes;

import java.time.Instant;

/**
 * The WaterPrediction class is used for micro service internal information handling.
 * This class simulates a table row of the database table waterPredictions.
 * So the messages can be converted into waterPrediction and easily inserted into the database.
 */
public class WaterPrediction {
    private Instant timestamp;
    private double waterTemperature;
    private double turbidity;
    private double waveHeight;

    public WaterPrediction(Instant timestamp, double waterTemperature, double turbidity, double waveHeight) {
        this.timestamp = timestamp;
        this.waterTemperature = waterTemperature;
        this.turbidity = turbidity;
        this.waveHeight = waveHeight;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public double getWaterTemperature() {
        return waterTemperature;
    }

    public double getTurbidity() {
        return turbidity;
    }

    public double getWaveHeight() {
        return waveHeight;
    }
}
