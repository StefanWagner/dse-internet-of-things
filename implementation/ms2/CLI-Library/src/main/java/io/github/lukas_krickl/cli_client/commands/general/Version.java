package io.github.lukas_krickl.cli_client.commands.general;

import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;

public class Version extends Command {
	public Version() {
		super(new CommandInfoBuilder("VERSION")
				.addSyntax("version")
				.addSyntax("v")
				.setDescription("Displays the version of this application.")
				.setRequiresArgs(ArgsRequired.NOT_REQUIRED)
				.build());
	}

	@Override
	public void run() {
		UserInterface.printSection(UserInterface.getInstance().getOutputText().getVersion());
	}
}
