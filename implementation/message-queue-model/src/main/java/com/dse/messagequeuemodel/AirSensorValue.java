package com.dse.messagequeuemodel;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * DataRecord implementation describing data recorded by an air sensor.
 */
@Value
@EqualsAndHashCode(callSuper = true)
public class AirSensorValue extends SensorValue implements DataRecord{
    private String weatherStation;
    private BigDecimal airTemperature;
    private BigDecimal windSpeed;
    private BigDecimal humidity;

    @Builder
    @ConstructorProperties({"timeStamp", "sensorType", "weatherStation", "airTemperature", "windSpeed", "humidity"})
    public AirSensorValue(Instant timeStamp,
                          SensorTypeEnum sensorType,
                          String weatherStation,
                          BigDecimal airTemperature,
                          BigDecimal windSpeed,
                          BigDecimal humidity) {
        super(timeStamp, sensorType);
        this.weatherStation = weatherStation;
        this.airTemperature = airTemperature;
        this.windSpeed = windSpeed;
        this.humidity = humidity;
    }

    public static AggregableValue toAggregable(AirSensorValue waterSensorValue) {
        return AggregableValue.builder()
                .identifier(waterSensorValue.getWeatherStation())
                .timeStamp(waterSensorValue.getTimeStamp())
                .value1(waterSensorValue.getAirTemperature())
                .value2(waterSensorValue.getHumidity())
                .value3(waterSensorValue.getWindSpeed())
                .type(waterSensorValue.getSensorType())
                .build();
    }

    public static AirSensorValue fromAggregable(AggregableValue aggregableValue) {
        return AirSensorValue.builder()
                .weatherStation(aggregableValue.getIdentifier())
                .timeStamp(aggregableValue.getTimeStamp())
                .airTemperature(aggregableValue.getValue1())
                .humidity(aggregableValue.getValue2())
                .windSpeed(aggregableValue.getValue3())
                .sensorType(aggregableValue.getType())
                .build();
    }

}
