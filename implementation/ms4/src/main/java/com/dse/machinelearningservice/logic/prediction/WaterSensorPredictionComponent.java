package com.dse.machinelearningservice.logic.prediction;

import com.dse.machinelearningservice.model.MessageRepository;
import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.*;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestClientException;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Creates a new prediction for expected water sensor values.
 */
@Slf4j
public class WaterSensorPredictionComponent extends PredictionComponent
{
    private MessageQueueClient messageQueueClient;

    /**
     * messages containing the data that should be used to create predictions
     */
    @Getter private List<Message> messageList;

    /**
     * To store newly created predictions
     */
    @Getter private MessageRepository predictionRepository;

    private static final int movingAverageWindow = 10;


    /**
     * Set up the data the prediction component should work with.
     * @param messageQueueClient client for communicating with the message queue
     * @param messageList list of previously received messages containing water sensor values
     * @param predictionRepository repository for storing the newly created prediction locally
     */
    public void setUp(MessageQueueClient messageQueueClient, List<Message> messageList,
              MessageRepository predictionRepository) {
        this.messageQueueClient = messageQueueClient;
        this.messageList = messageList;
        this.predictionRepository = predictionRepository;
    }

    private void sendPredictionMessage(Message message){
        log.info("Sending prediction message");

        //for local tests messageQueueClient can be set to null in the constructor
        try {
            if (messageQueueClient != null) messageQueueClient.publishMessage(message);
        } catch(RestClientException e) {
            log.info("Could not send message");
            e.printStackTrace();
        }
    }

    /**
     * sorts the list of messages by their timestamp
     */
    public void sortMessageListByDates() {
        Collections.sort(messageList, new Comparator<Message>() {
            @Override
            public int compare(Message m1, Message m2) {
                return m1.getTimestamp().compareTo(m2.getTimestamp());
            }
        });
    }

    /**
     * Creates a list of sensor values that contain only the values that should be used for the creation of the prediction.
     * @return the list of messages that fall in the moving average window
     */
    public List<WaterSensorValue> getSensorValuesFromMovingAverageWindow() {
        //sort messageList by date
        sortMessageListByDates();
        //messages should now be sorted in ascending order, i.e. the most recent entries are at the end of the list

        //get the data records from the list
        List<WaterSensorValue> waterSensorValues = messageList.stream()
                .map(message -> (WaterSensorValue) message.getRecord())
                .collect(Collectors.toList());

        //get only as many entries as fit inside the moving average window
        if (waterSensorValues.size() > movingAverageWindow) {
            waterSensorValues = waterSensorValues.stream()
                    .skip(waterSensorValues.size() - movingAverageWindow)
                    .collect(Collectors.toList());
        }

        return waterSensorValues;
    }

    /**
     * Creates a new prediction.
     * @param waterSensorValues the water sensor values that should be used for creating a prediction.
     * @return the new prediction
     */
    public WaterSensorPredictionDataRecord calculatePrediction(List<WaterSensorValue> waterSensorValues) {
        double waterTemperatureAverage = 0.;
        double waveHeightAverage = 0.;
        double turbidityAverage = 0.;

        //sum up all entries
        for (WaterSensorValue value: waterSensorValues) {
            waterTemperatureAverage += value.getWaterTemperature().doubleValue();
            waveHeightAverage += value.getWaveHeight().doubleValue();
            turbidityAverage += value.getTurbidity().doubleValue();
        }
        //divide by the number of entries
        waterTemperatureAverage /= waterSensorValues.size();
        waveHeightAverage /= waterSensorValues.size();
        turbidityAverage /= waterSensorValues.size();

        //info log
        log.info("Calculated water sensor prediction: water temperature = " + waterTemperatureAverage
                + ", wave height = " + waveHeightAverage + ", turbidity = " + turbidityAverage);

        //create prediction
        WaterSensorPredictionDataRecord prediction =
                new WaterSensorPredictionDataRecord(BigDecimal.valueOf(waterTemperatureAverage),
                        BigDecimal.valueOf(turbidityAverage), BigDecimal.valueOf(waveHeightAverage));

        return prediction;
    }

    @Override
    public void run() {
        log.info("Water sensor prediction started on thread " + Thread.currentThread().getId());

        //make sure that there actually are messages to work with
        if (messageList.isEmpty()) return;

        //get the data records that should be used for calculating the moving average
        List<WaterSensorValue> waterSensorValues = getSensorValuesFromMovingAverageWindow();

        //for each property, calculate the average over the last movingAverageWindow entries
        //(if there are less than the required number of messages in the list, just calculate the average over all of
        // them)
        //create a new prediction with the calculated values
        WaterSensorPredictionDataRecord prediction = calculatePrediction(waterSensorValues);

        //create a message containing the new prediction
        Message predictionMessage = new Message(TopicEnum.PREDICTION, prediction);

        //send the prediction
        sendPredictionMessage(predictionMessage);

        //add the new prediction to the prediction repository
        predictionRepository.addMessage(predictionMessage);
    }
}
