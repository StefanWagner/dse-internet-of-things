package com.dse.resultoutputservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResultOutputServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ResultOutputServiceApplication.class, args);
    }
}
