package com.dse.monitoring_component.cli_client.commands;

import com.dse.messagequeuemodel.Message;
import com.dse.monitoring_component.cli_client.data.DAO;
import com.dse.monitoring_component.cli_client.logic.NetworkConnector;
import com.dse.monitoring_component.cli_client.visual.Display;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

import java.io.IOException;
import java.util.List;

/**
 * Command for displaying and clearing errors
 */
public class ErrorsCommand extends Command {
    public ErrorsCommand() {
        super(new CommandInfoBuilder("ERRORS")
                .addSyntax("errors")
                .setDescription("Displays error messages collected by the Message Queue: \n" +
                        "A given positive argument determines the maximum amount of messages displayed.\n" +
                        "If no argument is given, all errors are displayed.\n" +
                        "Use '-c' or '-clear' parameter to clear errors.")
                .addParameterList()
                .addParameter("-clear", "Deletes all errors if used without an argument.\n" +
                        "A given positive number determines the amount of newest errors to keep.\n")

                .addParameter("-c", "Shorthand for -clear. \n" +
                        "Deletes all errors if used without an argument.\n" +
                        "A given positive number determines the amount of newest errors kept.")
                .finishParameterList()
                .setRequiresArgs(ArgsRequired.OPTIONAL)
                .build());
    }

    @Override
    public void run() {
        if (getParams().isEmpty()) {
            //used without parameters -> display all or given amount of errors
            if (getArgs().isEmpty()) {
                //select all errors
                showErrors(-1);
            } else if (getArgs().matches("[1-9][0-9]*")) {
                //a valid number
                showErrors(Integer.parseInt(getArgs()));
            } else {
                //not valid input
                UserInterface.printSection("To set the amount of errors to display, insert a positive number!");
            }
        } else if (getParams().contains("-clear") || getParams().contains("-c")) {
            //clear errors
            if (getArgs().isEmpty()) {
                clearErrors(0);
            } else if (getArgs().matches("[0-9]+")) {
                clearErrors(Integer.parseInt(getArgs()));
            } else {
                UserInterface.printSection("To specify the amount of errors to keep, insert a positive number!");
            }
        }
    }

    /**
     * Request and display all if count==-1 or count errors
     *
     * @param count -1 or positive
     */
    private void showErrors(int count) {
        NetworkConnector connector = DAO.getInstance().getConnector();
        try {
            List<Message> errorList = (count == -1) ? connector.requestAllErrors() : connector.requestErrors(count);
            UserInterface.println(Display.formatErrorMessages(errorList));

        } catch (IOException e) {
            UserInterface.printSection(TextColor.RED.apply("Error: ")+e.getMessage());
        }
    }

    /**
     * Request to clear errors to size
     *
     * @param size 0 or positive
     */
    private void clearErrors(int size) {
        NetworkConnector connector = DAO.getInstance().getConnector();
        try {
            if (size == 0) {
                connector.requestClearAllErrors();
            } else {
                connector.requestClearErrors(size);
            }
            UserInterface.printSection(TextColor.GREEN.apply("SUCCESS:") + " Errors cleared to "+size);
        } catch (IOException e) {
            UserInterface.printSection(TextColor.RED.apply("FAILED:") + " Could not clear errors: " + e.getMessage());
        }
    }
}
