package com.dse.messagequeuemonitoringcomponent.managers;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Status;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.messagequeuemonitoringcomponent.data.ErrorMessageList;
import com.dse.messagequeuemonitoringcomponent.data.StoredCacheData;
import com.dse.messagequeuemonitoringcomponent.exception.InvalidInputException;
import com.dse.messagequeuemonitoringcomponent.exception.MQConnectionFailedException;
import com.dse.messagequeuemonitoringcomponent.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * The DataManager is used for accessing/storing all information collected from the message queue.
 */
@Repository
public class DataManager {
    private static final Logger log = LoggerFactory.getLogger(DataManager.class);
    /**
     * map which stores the amount of cached messages to the topics
     */
    private ConcurrentHashMap<TopicEnum, StoredCacheData> cacheSize;
    /**
     * a list of all connected services
     */
    private ConcurrentHashMap<Subscriber, Instant> services;
    /**
     * Information about errors collected
     */
    private ErrorMessageList errorMessageList;
    /**
     * Information that a subscriber was deleted.
     * This info is stored, because the subscriber doesn't exist yet
     */
    private ConcurrentHashMap<String, Instant> cachedServiceTermination;
    /**
     * Information on the successful connection to the message queue
     */
    private boolean connectedToMQ;
    public DataManager() {
        cacheSize = new ConcurrentHashMap<>();
        services = new ConcurrentHashMap<>();
        errorMessageList = new ErrorMessageList();
        cachedServiceTermination = new ConcurrentHashMap<>();
        connectedToMQ = false;

        //Initializing cacheSizes
        for (TopicEnum topic : TopicEnum.values()) {
            cacheSize.put(topic, new StoredCacheData(0, Instant.MIN));
        }
    }

    /**
     * Generates and returns a status object.
     *
     * @return status - contains combined mq meta info
     */
    public Status requestStatus() {
        checkMQConnection();
        int totalCacheSize = 0;
        for (StoredCacheData cacheData : cacheSize.values()) {
            totalCacheSize += Math.max(cacheData.getCacheSize(), 0); //cache size is -1 if not cached, so add 0 if size is -1
        }
        return new Status(totalCacheSize, services.size(), errorMessageList.getErrorCount());
    }

    /**
     * Returns detailed individual cache sizes
     * <p>
     * Checks the connection to the message Queue
     * and throws a {@link MQConnectionFailedException} if the connection was not established yet
     *
     * @return Cache sizes mapped to the topics
     */
    public Map<TopicEnum, Integer> requestCacheSize() {
        checkMQConnection();

        HashMap<TopicEnum, Integer> requestedCache = new HashMap<>();
        for (TopicEnum topic : cacheSize.keySet()) {
            requestedCache.put(topic, cacheSize.get(topic).getCacheSize());
        }
        return requestedCache;
    }

    /**
     * Returns a list of all connected services
     * <p>
     * Checks the connection to the message Queue
     * and throws a {@link MQConnectionFailedException} if the connection was not established yet
     *
     * @return a set of all stored subscribers
     */
    public Set<Subscriber> requestServices() {
        checkMQConnection();
        return services.keySet();
    }

    /**
     * Returns a list of stored errors
     * <p>
     * Checks the connection to the message Queue
     * and throws a {@link MQConnectionFailedException} if the connection was not established yet
     *
     * @param count specifies the maximum amount of errors returned
     * @return Array with error messages stored from new to old
     */
    public List<Message> requestErrors(Integer count) {
        if (count == null) { //no http-GET parameter provided
            return errorMessageList.getAllErrors();

        } else if (count <= 0) {
            throw new InvalidInputException("Requested amount of errors is <=0");
        }
        count = Math.min(count, errorMessageList.getErrorCount());
        return errorMessageList.getErrors(count);
    }

    /**
     * Clears the stored errors to given size. (Amount of size newest errors are kept)
     *
     * @param size amount of errors to keep. null or 0 if all errors should be cleared
     */
    public void requestClearErrors(Integer size) {
        if (errorMessageList.getErrorCount() == 0) {
            throw new NotFoundException("All Errors already cleared");
        }

        if (size == null || size == 0) {
            errorMessageList.clearErrors();
            log.info("All errors cleared");

        } else {
            if (size < 0) {
                throw new InvalidInputException("Requested errors to keep when clearing is negative");

            } else if (size > errorMessageList.getErrorCount()) {
                throw new NotFoundException("Can not clear errors to size: amount to keep" +
                        " is greater or equal the existing amount of errors");
            }
            errorMessageList.clearErrors(size);
            log.info("Errors cleared to {}", size);
        }
    }

    /**
     * Sets the connection flag
     *
     * @param connectedToMQ true if the connection to the MQ has been established
     */
    public synchronized void setConnected(boolean connectedToMQ) {
        this.connectedToMQ = connectedToMQ;
    }

    /**
     * Checks if the connection to the MQ is established
     * and throws an {@link MQConnectionFailedException} if not
     */
    public void checkMQConnection() {
        if (!connectedToMQ) {
            throw new MQConnectionFailedException();
        }
    }

    public boolean isConnectedToMQ() {
        return connectedToMQ;
    }

    public ConcurrentHashMap<TopicEnum, StoredCacheData> getCacheSize() {
        return cacheSize;
    }

    public ConcurrentHashMap<Subscriber, Instant> getServices() {
        return services;
    }

    public List<String> getServicesURLs() {
        return services.keySet().stream().map(Subscriber::getUrl).collect(Collectors.toList());
    }

    public ErrorMessageList getErrorMessageList() {
        return errorMessageList;
    }

    public ConcurrentHashMap<String, Instant> getCachedServiceTermination() {
        return cachedServiceTermination;
    }


    @Scheduled(fixedDelay = 8000)
    private void logInternalState() {
        StringBuilder sb = new StringBuilder("\n\n\n===========================Internal State LOG===============================");

        sb.append("\nCACHE SIZE:");
        for (TopicEnum topic : TopicEnum.values()) {
            StoredCacheData cacheData = cacheSize.get(topic);
            sb.append("\n  ").append(String.format("%-25s: ", topic.name()))
                    .append(String.format(" SIZE: %-10d", cacheData.getCacheSize()))
                    .append(String.format("%20s", (cacheData.getLastUpdate() == Instant.MIN) ? "not updated" : cacheData.getLastUpdate().toString()));
        }

        sb.append("\n\nSERVICES:");
        for (Subscriber sub : services.keySet()) {
            sb.append("\n  +) ").append(sub.getUrl()).append(" - last update: ").append(services.get(sub));
            sb.append("\n     TOPICS: ").append(sub.getTopics().toString());
        }

        sb.append("\n\nERRORS: ").append(errorMessageList.getErrorCount());

        sb.append("\n\nCACHED TERMINATION MSG:");
        for (String s : cachedServiceTermination.keySet()) {
            sb.append("\n  ").append(s).append("  ").append(cachedServiceTermination.get(s));
        }
        sb.append("\n==========================================================================\n\n\n");
        log.info(sb.toString());
    }

}
