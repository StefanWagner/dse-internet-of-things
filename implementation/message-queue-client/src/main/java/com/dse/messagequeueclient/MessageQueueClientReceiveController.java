package com.dse.messagequeueclient;

import com.dse.messagequeuemodel.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageQueueClientReceiveController {

	private final MessageReceiver messageReceiver;

	public MessageQueueClientReceiveController(MessageReceiver messageReceiver) {
		this.messageReceiver = messageReceiver;
	}

	@PostMapping("/messages")
	public ResponseEntity<?> processMessage(@RequestBody Message message) {
		this.messageReceiver.processMessage(message);
		return ResponseEntity.ok().build();
	}
}

