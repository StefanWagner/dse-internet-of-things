package com.dse.messagequeuemodel;

import lombok.*;

import java.time.Instant;

/**
 * Abstract superclass for Sensorvalues. Contains fields that all sensors have in common
 */
@Getter
@EqualsAndHashCode
@AllArgsConstructor
@ToString
public abstract class SensorValue implements DataRecord{
    private final Instant timeStamp;
    private final SensorTypeEnum sensorType;

}
