package com.dse.iotsimulationservice.sensor;

import com.dse.iotsimulationservice.service.SensorManager;
import com.dse.messagequeuemodel.AggregableValue;
import com.dse.messagequeuemodel.SensorTypeEnum;
import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@Slf4j
@ConditionalOnProperty(name = "sensor.type", havingValue = "water")
public class MockFileWaterSensor extends MockIotSensor {

    @Autowired
    public MockFileWaterSensor(SensorManager manager){
        this(manager, null);
    }

    public MockFileWaterSensor(SensorManager manager, String fileName) {
        super(manager);
        this.srcValues = new HashMap<>();
        CSVReader reader;
        String srcFile = fileName == null ? "waterdata.csv" : fileName;
        try {
            Resource resource = new ClassPathResource(srcFile);
            File inputFile = resource.getFile();
            reader = new CSVReader(new FileReader(inputFile));
            List<String[]> valueEntries = reader.readAll();
            srcValues = valueEntries.stream()
                    .skip(1)
                    .map(MockFileWaterSensor::csvToSensorValue)
                    .filter(Objects::nonNull)
                    .collect(Collectors.groupingBy(AggregableValue::getIdentifier));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Could not find file " + srcFile);
        } catch (IOException e) {
            throw new IllegalStateException("Could not load csv file on path: " + srcFile, e);
        }
    }

    /**
     * Converts a list of strings (ie. the csv fields) to an AggregableValue object
     * Note that this method only works on a format where the fields are in a certain order
     * If you want to use custom CSV files you need to implement your own custom MockIotSensor
     *
     * @param fields Array of strings representing a csv row
     * @return an AggregableValue object built from these values
     */
    private static AggregableValue csvToSensorValue(String[] fields) {
        if (fields[0].isEmpty() || fields[1].isEmpty()) {
            return null;
        }
        String beachName = fields[0];
        Instant timeStamp = LocalDateTime.parse(
                fields[1],
                DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss a", Locale.US)
        )
                .atZone(ZoneId.systemDefault())
                .toInstant();

        BigDecimal waterTemperature = !fields[2].isEmpty() ? new BigDecimal(fields[2]) : null;
        BigDecimal turbidity = !fields[3].isEmpty() ? new BigDecimal(fields[3]) : null;
        BigDecimal waveHeight = !fields[5].isEmpty() ? new BigDecimal(fields[5]) : null;

        return AggregableValue.builder()
                .timeStamp(timeStamp)
                .type(SensorTypeEnum.WATER)
                .identifier(beachName)
                .value1(waterTemperature)
                .value2(turbidity)
                .value3(waveHeight)
                .build();
    }

    /**
     * Omit a new value and publish to the observers.
     * Scheduled to happen regularly after a configurable fixed delay
     */
    @Scheduled(fixedDelayString = "${sensor.readdelay}")
    public void notifyForValueChange() {
        this.notifyObserver();
    }
}

