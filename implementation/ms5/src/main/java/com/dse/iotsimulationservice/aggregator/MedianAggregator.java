package com.dse.iotsimulationservice.aggregator;

import com.dse.messagequeuemodel.AggregableValue;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Aggregates a list of values by taking the median of each value field
 */
@Component
@ConditionalOnProperty(name = "sensor.aggregation.strategy", havingValue = "median")
public class MedianAggregator implements Aggregator {

    /**
     * Aggregates the fields "value1", "value2" and "value3" of each entry by
     * taking the median of the list sorted by the natural ordering of the values
     * or the average of the two middle values if the list size is even
     * Null values of the three fields will be ignored for the respective median
     *
     * @param values the values to be aggregated
     * @return the aggregated value
     */
    @Override
    public AggregableValue aggregateValues(List<AggregableValue> values) {
        BigDecimal value1;
        BigDecimal value2;
        BigDecimal value3;
        int medianindex;

        AggregableValue srcValue = values.stream()
                .filter(val -> Objects.nonNull(val)
                        && val.getIdentifier() != null
                        && val.getTimeStamp() != null)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("no non-null element in list"));


        List<AggregableValue> valuesFiltered = values.stream()
                .filter(val -> val.getValue1() != null)
                .sorted(Comparator.comparing(AggregableValue::getValue1))
                .collect(Collectors.toList());
        medianindex = valuesFiltered.size()/2;
        if(valuesFiltered.size() % 2 == 0){
            value1 = !valuesFiltered.isEmpty() ?
                    valuesFiltered.get(medianindex).getValue1().add(valuesFiltered.get(medianindex-1).getValue1()).divide(BigDecimal.valueOf(2), 4, RoundingMode.HALF_DOWN).stripTrailingZeros() : BigDecimal.ZERO;
        } else {
            value1 = valuesFiltered.get(medianindex).getValue1();
        }



        valuesFiltered = values.stream()
                .filter(val -> val.getValue2() != null)
                .sorted(Comparator.comparing(AggregableValue::getValue2))
                .collect(Collectors.toList());
        medianindex = valuesFiltered.size() / 2;
        if(valuesFiltered.size() % 2 == 0){
            value2 = !valuesFiltered.isEmpty() ?
                    valuesFiltered.get(medianindex).getValue2().add(valuesFiltered.get(medianindex-1).getValue2()).divide(BigDecimal.valueOf(2), 4, RoundingMode.HALF_DOWN).stripTrailingZeros() : BigDecimal.ZERO;
        } else {
            value2 = valuesFiltered.get(medianindex).getValue2();
        }

        valuesFiltered = values.stream()
                .filter(val -> val.getValue3() != null)
                .sorted(Comparator.comparing(AggregableValue::getValue3))
                .collect(Collectors.toList());
        medianindex = valuesFiltered.size() / 2;
        if(valuesFiltered.size() % 2 == 0){
            value3 = !valuesFiltered.isEmpty() ?
                    valuesFiltered.get(medianindex).getValue3().add(valuesFiltered.get(medianindex-1).getValue3()).divide(BigDecimal.valueOf(2), 4, RoundingMode.HALF_DOWN).stripTrailingZeros() : BigDecimal.ZERO;
        } else {
            value3 = valuesFiltered.get(medianindex).getValue3();
        }
        return AggregableValue.builder()
                .type(values.get(0).getType())
                .identifier(srcValue.getIdentifier())
                .timeStamp(srcValue.getTimeStamp())
                .value1(value1)
                .value2(value2)
                .value3(value3)
                .build();
    }
}
