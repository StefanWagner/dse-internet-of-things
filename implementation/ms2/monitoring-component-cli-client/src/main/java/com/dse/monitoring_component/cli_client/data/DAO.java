package com.dse.monitoring_component.cli_client.data;

import com.dse.monitoring_component.cli_client.logic.NetworkConnector;

/**
 * Data Access Object as central access object
 */
public class DAO {
    private static final DAO instance = new DAO();
    private NetworkConnector connector;
    private int refreshRate = Configuration.defaultRefreshRate;

    private DAO() {
        connector = new NetworkConnector(Configuration.defaultMonitoringComponentURL);
    }

    public static DAO getInstance() {
        return instance;
    }

    public NetworkConnector getConnector() {
        return connector;
    }

    public int getRefreshRate() {
        return refreshRate;
    }

    public synchronized void setRefreshRate(int refreshRate) {
        this.refreshRate = refreshRate;
    }
}
