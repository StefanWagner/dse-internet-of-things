package io.github.lukas_krickl.cli_client.commands;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Objects;

/**
 * Information on a Command
 */
public class CommandInfo {
	/**
	 * Unique Command name
	 */
	private final String name;

	/**
	 * syntax words the command can be called with
	 */
	private final HashSet<String> syntax;

	/**
	 * Parameters with a leading '-' and their description
	 */
	private final Hashtable<String, String> parameters;

	/**
	 * Description of the command
	 */
	private final String description;

	/**
	 * Flag if a argument is Needed optional or not allowed
	 */
	private final ArgsRequired argsRequired;

	public CommandInfo(String name, HashSet<String> syntax, Hashtable<String, String> parameters, String description, ArgsRequired argsRequired) {
		this.name = name;
		this.syntax = syntax;
		this.parameters = parameters;
		this.description = description;
		this.argsRequired = argsRequired;
	}

	// GETTER

	public String getName() {
		return name;
	}

	public HashSet<String> getSyntax() {
		return syntax;
	}

	public Hashtable<String, String> getParameters() {
		return parameters;
	}

	public String getDescription() {
		return description;
	}

	public ArgsRequired getArgsRequired() {
		return argsRequired;
	}

	/**
	 * Compares names
	 * @param o other CommandInfo
	 * @return true if names are equal
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CommandInfo)) return false;
		CommandInfo that = (CommandInfo) o;
		return getName().equals(that.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName());
	}
}
