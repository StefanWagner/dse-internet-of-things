package com.dse.messagequeuemonitoringcomponent.data;

import java.time.Instant;

/**
 * A class for storing cache information, it contains the actual message size
 * and a time stamp when the information was last updated.
 *
 * is threadsafe
 */
public class StoredCacheData {
    /**
     * Amount of messages cached
     */
    private int cacheSize;

    /**
     * Timestamp when the cachsize was updated, needed for comparing
     * with incoming messages
     */
    private Instant lastUpdate;

    public StoredCacheData(int cacheSize, Instant lastUpdate) {
        this.cacheSize = cacheSize;
        this.lastUpdate = lastUpdate;
    }

    public synchronized void refresh(int cacheSize, Instant lastUpdate) {
        this.cacheSize = cacheSize;
        this.lastUpdate = lastUpdate;
    }

    public int getCacheSize() {
        return cacheSize;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public String toString() {
        return "cacheSize=" + cacheSize +
                ", lastUpdate=" + lastUpdate;
    }
}
