package com.dse.monitoring_component.cli_client.exceptions;

import java.io.IOException;

/**
 * Exception thrown when the HttpErrorResponseHandler received Connect Exception (timeout) from the rest template
 */
public class MCNotReachableException extends IOException {
    private static final String defaultErrorMessage = "The monitoring component is not reachable.\n\n" +
            "Check if:\n" +
            "    1) the URL is configured correctly\n" +
            "    2) the monitoring component is running\n" +
            "    3) the network connection between both services is established\n\n";

    public MCNotReachableException() {
        super(defaultErrorMessage);
    }
}
