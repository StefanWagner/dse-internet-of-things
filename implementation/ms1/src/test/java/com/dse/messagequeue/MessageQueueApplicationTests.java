package com.dse.messagequeue;

import com.dse.messagequeue.controller.MessageQueueController;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MessageQueueApplicationTests {

    @Autowired
    private MessageQueueController messageQueueController;

    @Autowired
    private RestTemplate restTemplate;


    @BeforeEach
    public void setUp() {
    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void createSubscriber() throws JsonProcessingException {
        Set<TopicEnum> set = new HashSet<>();
        set.add(TopicEnum.PREDICTION);
        String result = new ObjectMapper().writeValueAsString(new Subscriber("https://f86aad59-a794-4e01-8ecc-2efc63eb931e.mock.pstmn.io", 0, set));
        result = new ObjectMapper().findAndRegisterModules().writeValueAsString(MQTestClassGenerator.getRandomMessage(TopicEnum.PREDICTION));
        System.out.println(result);
    }

    @Test
    public void messageGetsProcessed() throws InterruptedException {
        //setup
        MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
        Set<TopicEnum> topics = new HashSet<>();
        topics.add(TopicEnum.TERMINATION);
        topics.add(TopicEnum.AIRSENSORDATA);
        Subscriber sub = new Subscriber("http://test.com:8888", 0, topics);
        messageQueueController.subscribeToTopics(sub);

        //expectation
        server.expect(requestTo("http://test.com:8888/messages")).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess());
        server.expect(requestTo("http://test.com:8888/messages")).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess());

        //processing
        Message msg = MQTestClassGenerator.getRandomMessage(TopicEnum.AIRSENSORDATA);
        messageQueueController.publishMessage(msg);
        msg = MQTestClassGenerator.getRandomMessage(TopicEnum.TERMINATION);
        messageQueueController.publishMessage(msg);
        msg = MQTestClassGenerator.getRandomMessage(TopicEnum.ANOMALY);
        messageQueueController.publishMessage(msg);

        //assertion
        await().untilAsserted(server::verify);
    }

    @Test
    public void cachedMessagesGetProcessed() throws InterruptedException {
        //setup
        MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
        Set<TopicEnum> topics = new HashSet<>();
        topics.add(TopicEnum.TERMINATION);
        topics.add(TopicEnum.AIRSENSORDATA);
        Subscriber sub = new Subscriber("http://test.com:8889", 0, topics);

        Message msg = MQTestClassGenerator.getRandomMessage(TopicEnum.AIRSENSORDATA);
        messageQueueController.publishMessage(msg);
        msg = MQTestClassGenerator.getRandomMessage(TopicEnum.TERMINATION);
        messageQueueController.publishMessage(msg);
        msg = MQTestClassGenerator.getRandomMessage(TopicEnum.ANOMALY);
        messageQueueController.publishMessage(msg);

        //expectation
        server.expect(requestTo("http://test.com:8889/messages")).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess());
        server.expect(requestTo("http://test.com:8889/messages")).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess());
        server.expect(requestTo("http://test.com:8889/messages")).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess());

        //processing
        messageQueueController.subscribeToTopics(sub);
        topics.add(TopicEnum.ANOMALY);
        sub = new Subscriber(sub.getUrl(), sub.getDelayInMillis(), topics);
        messageQueueController.subscribeToTopics(sub);


        //assertion
        await().untilAsserted(server::verify);
    }

    @Test
    public void delayIsRespected() throws InterruptedException {
        //setup
        MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
        Set<TopicEnum> topics = new HashSet<>();
        topics.add(TopicEnum.TERMINATION);
        topics.add(TopicEnum.AIRSENSORDATA);
        Subscriber sub = new Subscriber("http://test.com:8890", 4000, topics);
        messageQueueController.subscribeToTopics(sub);

        //expectation
        server.expect(requestTo("http://test.com:8890/messages")).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess());
        server.expect(requestTo("http://test.com:8890/messages")).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess());

        //processing
        Message msg = MQTestClassGenerator.getRandomMessage(TopicEnum.AIRSENSORDATA);
        messageQueueController.publishMessage(msg);
        msg = MQTestClassGenerator.getRandomMessage(TopicEnum.TERMINATION);
        messageQueueController.publishMessage(msg);
        msg = MQTestClassGenerator.getRandomMessage(TopicEnum.ANOMALY);
        messageQueueController.publishMessage(msg);

        //assertion
        //thismakes sure that this assertion only becomes true after at least 4 or more seconds.
        //if the assertion was true before the 4 seconds the test fails
        await().atLeast(4, TimeUnit.SECONDS).untilAsserted(server::verify);
    }


}
