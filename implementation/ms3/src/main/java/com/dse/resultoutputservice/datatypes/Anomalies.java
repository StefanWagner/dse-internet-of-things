package com.dse.resultoutputservice.datatypes;

import java.util.ArrayDeque;

/**
 * The Anomalies consists out of a set of anomaly objects.
 * It is used for bundling the database data which will be sent to the browser.
 */
public class Anomalies {
    private ArrayDeque<Anomaly> anomalies;

    public Anomalies() {
        this.anomalies = new ArrayDeque<Anomaly>();
    }

    public ArrayDeque<Anomaly> getAnomalies() {
        return anomalies;
    }

    public void addAnomaly(Anomaly anomaly) {
        anomalies.add(anomaly);
    }
}
