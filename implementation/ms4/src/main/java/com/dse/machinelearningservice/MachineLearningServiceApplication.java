package com.dse.machinelearningservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MachineLearningServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MachineLearningServiceApplication.class, args);
	}

}
