package com.dse.monitoring_component.cli_client.logic;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Status;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.monitoring_component.cli_client.exceptions.MCNotReachableException;
import com.dse.monitoring_component.cli_client.exceptions.MQNotReachableException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.xml.ws.Response;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class responsible for communicating with the Monitoring Component
 */
public class NetworkConnector {
    private String monitoringComponentURL;
    private RestTemplate restTemplate;

    public NetworkConnector(String monitoringComponentURL) {
        this.monitoringComponentURL = monitoringComponentURL;
        this.restTemplate = new RestTemplateBuilder()
                .errorHandler(new HttpErrorResponseHandler())
                .build();
    }

    /**
     * Check the syntax of the given URL for validity
     *
     * @param url url to check
     * @return true if it seems to be valid, false otherwise
     */
    public static boolean checkURL(String url) {
        return url.matches("((http://)|(https://))([a-zA-Z]|[0-9]|-|\\.|_|\\+)+:[0-9]+");
    }

    public String getMonitoringComponentURL() {
        return monitoringComponentURL;
    }

    /**
     * Setter for the Monitoring Component URL,
     * throws a illegal argument exception if the url is not valid
     *
     * @param monitoringComponentURL base url in form http://192.168.1.1:8080 or http://monitoring-component:8080
     */
    public synchronized void setMonitoringComponentURL(String monitoringComponentURL) {
        if (checkURL(monitoringComponentURL)) {
            this.monitoringComponentURL = monitoringComponentURL;
        } else {
            throw new IllegalArgumentException("Given url seems not to be valid!");
        }
    }

    /**
     * Makes an http GET request to the /status endpoint of the monitoring component
     * {@link HttpErrorResponseHandler} will handle error response codes and throw a corresponding exception
     * <p>
     * RestTemplate will throw a {@link java.net.ConnectException} nested in a RestClientException
     * if the network connection to the server can not be established.
     *
     * @return Status from the MC
     * @throws IOException referring the received error response code
     */
    public Status requestStatus() throws IOException {
        try {
            return restTemplate.getForEntity(monitoringComponentURL + "/status", Status.class).getBody();
        } catch (RestClientException e) {
            throw HttpErrorResponseHandler.handleRestClientException(e);
        }
    }

    /**
     * Makes an http GET request to the /status/cache endpoint of the monitoring component
     * ParameterizedTypeReference is needed to specify generic types in the Map<TopicEnum, Integer>
     * <p>
     * {@link HttpErrorResponseHandler} will handle error response codes and throw a corresponding exception
     * <p>
     * RestTemplate will throw a {@link java.net.ConnectException} nested in a RestClientException
     * if the network connection to the server can not be established.
     *
     * @return a map with cache sizes mapped to their topic
     * @throws IOException referring the received error response code
     */
    public Map<TopicEnum, Integer> requestCache() throws IOException {
        //Construct generic response type
        ParameterizedTypeReference<Map<TopicEnum, Integer>> responseType =
                new ParameterizedTypeReference<Map<TopicEnum, Integer>>() {};

        //make request
        try {
            return requestFromMC("/status/cache", "", HttpMethod.GET, responseType).getBody();

        } catch (RestClientException e) {
            throw HttpErrorResponseHandler.handleRestClientException(e);
        }
    }

    /**
     * Makes an http GET request to the /messages/errors endpoint of the monitoring component
     * <p>
     * {@link HttpErrorResponseHandler} will handle error response codes and throw a corresponding exception
     * <p>
     * RestTemplate will throw a {@link java.net.ConnectException} nested in a RestClientException
     * if the network connection to the server can not be established.
     *
     * @param count maximum amount of error messages
     * @return list with error messages
     * @throws IOException referring the received error response code
     */
    public List<Message> requestErrors(int count) throws IOException {
        String queryParameter = (count < 1 ? "" : ("?count=" + count));

        //Construct generic response type
        ParameterizedTypeReference<List<Message>> responseType =
                new ParameterizedTypeReference<List<Message>>() {};
        //make request
        try {
            return requestFromMC("/messages/errors", queryParameter, HttpMethod.GET, responseType).getBody();

        } catch (RestClientException e) {
            throw HttpErrorResponseHandler.handleRestClientException(e);
        }
    }

    /**
     * Makes an http GET request to the /messages/errors endpoint of the monitoring component
     * Requests all error messages using requestErrors(count)
     *
     * @return all error messages from collected by the monitoring component
     * @throws IOException referring the received error response code
     */
    public List<Message> requestAllErrors() throws IOException {
        return requestErrors(-1);
    }

    /**
     * Makes an http DELETE request to the /messages/errors endpoint of the monitoring component
     * Clears error messages from the monitoring component
     * but keeps the amount of messages specified by size
     * <p>
     * {@link HttpErrorResponseHandler} will handle error response codes and throw a corresponding exception
     * <p>
     * RestTemplate will throw a {@link java.net.ConnectException} nested in a RestClientException
     * if the network connection to the server can not be established.
     *
     * @param size amount of messages to keep
     * @throws IOException referring the received error response code
     */
    public void requestClearErrors(int size) throws IOException {
        try {
            URI url = URI.create(monitoringComponentURL + "/messages/errors?size=" + size);
            restTemplate.delete(url);

        } catch (RestClientException e) {
            throw HttpErrorResponseHandler.handleRestClientException(e);
        }
    }

    /**
     * Makes an http DELETE request to the /messages/errors endpoint of the monitoring component
     * Clears all error messages from the monitoring component
     * <p>
     * {@link HttpErrorResponseHandler} will handle error response codes and throw a corresponding exception
     * <p>
     * RestTemplate will throw a {@link java.net.ConnectException} nested in a RestClientException
     * if the network connection to the server can not be established.
     *
     * @throws IOException referring the received error response code
     */
    public void requestClearAllErrors() throws IOException {
        try {
            URI url = URI.create(monitoringComponentURL + "/messages/errors");
            restTemplate.delete(url);

        } catch (RestClientException e) {
            throw HttpErrorResponseHandler.handleRestClientException(e);
        }
    }

    /**
     * Makes an http GET request to the /services endpoint of the monitoring component
     * Requests a list of all current subscribers (connected services) from the MC
     * <p>
     * {@link HttpErrorResponseHandler} will handle error response codes and throw a corresponding exception
     * <p>
     * RestTemplate will throw a {@link java.net.ConnectException} nested in a RestClientException
     * if the network connection to the server can not be established.
     *
     * @return a set with all connected services
     * @throws IOException referring the received error response code
     */
    public Set<Subscriber> requestSubscribers() throws IOException {
        try {
            ParameterizedTypeReference<Set<Subscriber>> responseType =
                    new ParameterizedTypeReference<Set<Subscriber>>() {};
            return requestFromMC("/services", "", HttpMethod.GET, responseType).getBody();

        } catch (RestClientException e) {
            throw HttpErrorResponseHandler.handleRestClientException(e);
        }

    }

    /**
     * Makes an http DELETE request to the /services endpoint of the monitoring component
     * Requests the monitoring component to send out a termination with the given targets
     * <p>
     * {@link HttpErrorResponseHandler} will handle error response codes and throw a corresponding exception
     * <p>
     * RestTemplate will throw a {@link java.net.ConnectException} nested in a RestClientException
     * if the network connection to the server can not be established.
     *
     * @param targets service urls to terminate
     * @throws IOException referring the received error response code
     */
    public void requestTermination(String[] targets) throws IOException {
        try {
            requestFromMC("/services", "", HttpMethod.DELETE, targets, null);

        } catch (RestClientException e) {
            throw HttpErrorResponseHandler.handleRestClientException(e);
        }
    }


    /**
     * Makes an http request using RestTemplate and the exchange method
     * <p>
     * Reason for this approach and against a simpler method of RestTemplate is
     * the need of Generic types in the response type for example a List<T> or a Map<K,V>
     *
     * @param endpoint       endpoint of the monitoring component
     * @param urlQueryParams parameters as string and WITH the leading ? e.g. "?count=2&parameter=12" can be empty or null
     * @param httpMethod     http method to use
     * @param responseType   a ParameterizedTypeReference with the preferred type, can be null if no response is needed
     * @param <T>            Generic type inside the ParameterizedTypeReference
     * @return ResponseEntity with the body type given in the ParameterizedTypeReference or null if responseType is null
     * @throws MCNotReachableException if an connection error occurred
     */
    private <T, B> ResponseEntity<T> requestFromMC(
            String endpoint,
            String urlQueryParams,
            HttpMethod httpMethod,
            B requestBody,
            ParameterizedTypeReference<T> responseType) throws MCNotReachableException {

        if (urlQueryParams == null) {
            urlQueryParams = "";
        }
        URI url = URI.create(monitoringComponentURL + endpoint + urlQueryParams);

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());
        header.add(HttpHeaders.ACCEPT, "application/json");

        RequestEntity<B> request = new RequestEntity<>(requestBody, header, httpMethod, url);

        if (responseType == null) {
            //no response expected requested type is null: so request is made and null returned
            restTemplate.exchange(request, String.class);
            return null;
        }
        return restTemplate.exchange(request, responseType);
    }

    /**
     * Overloaded requestFromMC without a request body(set to null)
     */
    private <T> ResponseEntity<T> requestFromMC(
            String endpoint,
            String urlQueryParams,
            HttpMethod httpMethod,
            ParameterizedTypeReference<T> responseType) throws MCNotReachableException {
        return requestFromMC(endpoint, urlQueryParams, httpMethod, null, responseType);
    }

}
