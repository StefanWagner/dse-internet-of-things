var airPredictions = {
    size: 0,
    maxTimestamp: "1970-01-01T00:00:00.000Z",
    minTimestamp: "1970-01-01T00:00:00.000Z",
    airPredictions: new Array(),

    getAirPredictions: function() {
        airPredictions.size = airPredictions.size + 1;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                airPredictions.airPredictions = airPredictions.airPredictions.concat((JSON.parse(this.responseText)).airPredictions);
                airPredictions.airPredictions.sort(function(a,b) {return new Date(b.timestamp) - new Date(a.timestamp)});
                airPredictions.createTable();
                airPredictions.maxTimestamp = airPredictions.airPredictions[0].timestamp;
                airPredictions.minTimestamp = airPredictions.airPredictions[airPredictions.airPredictions.length-1].timestamp;
            }
        };
        xmlhttp.open("GET", "/getAirPredictions?size=" + this.size +"&maxTimestamp=" + this.maxTimestamp +"&minTimestamp=" + this.minTimestamp, true);
        xmlhttp.send();
    },

    createTable: function() {
        var i;
        var table="<tr><th>Timestamp</th><th>Air Temperature</th><th>Wind Speed</th><th>Humidity</th></tr>";
        for (i = 0; i < this.airPredictions.length; i++) {
            table += "<tr><td>" +
                    this.airPredictions[i].timestamp +
                    "</td><td>" +
                    this.airPredictions[i].airTemperature +
                    "</td><td>" +
                    this.airPredictions[i].windSpeed +
                    "</td><td>" +
                    this.airPredictions[i].humidity +
                    "</td></tr>";
        }
        document.getElementById("airPredictions").innerHTML = table;
    }
}