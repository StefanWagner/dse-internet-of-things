package com.dse.monitoring_component.cli_client.test.commands;


import com.dse.monitoring_component.cli_client.commands.RefreshRateCommand;
import com.dse.monitoring_component.cli_client.data.DAO;
import io.github.lukas_krickl.cli_client.commands.Command;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class RefreshRateCommandTest {

    @Test
    void notValidNewRefreshRateGiven_displaysError_rrNotChanged() {
        int oldRefreshRate = DAO.getInstance().getRefreshRate();
        Command initialCMD = new RefreshRateCommand();
        Command callableCommand;

        //try string
        callableCommand = initialCMD.makeCallable(new ArrayList<>(),"notvalid");
        assertDoesNotThrow(callableCommand::run);
        assertEquals(oldRefreshRate, DAO.getInstance().getRefreshRate());

        //try negative
        callableCommand = initialCMD.makeCallable(new ArrayList<>(),"-1245");
        assertDoesNotThrow(callableCommand::run);
        assertEquals(oldRefreshRate, DAO.getInstance().getRefreshRate());

        //try decimal
        callableCommand = initialCMD.makeCallable(new ArrayList<>(),"457.034");
        assertDoesNotThrow(callableCommand::run);
        assertEquals(oldRefreshRate, DAO.getInstance().getRefreshRate());

    }

    @Test
    void validRefreshRateGiven_rrChanged() {
        int oldRefreshRate = 1000;
        DAO.getInstance().setRefreshRate(oldRefreshRate);
        Command cmd = new RefreshRateCommand();
        cmd = cmd.makeCallable(new ArrayList<>(),"4000");

        assertDoesNotThrow(cmd::run);
        assertNotEquals(oldRefreshRate, DAO.getInstance().getRefreshRate());
    }
}
