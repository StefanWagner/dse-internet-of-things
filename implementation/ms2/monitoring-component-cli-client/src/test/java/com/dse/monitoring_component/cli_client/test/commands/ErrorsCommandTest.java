package com.dse.monitoring_component.cli_client.test.commands;

import com.dse.monitoring_component.cli_client.commands.ErrorsCommand;
import com.dse.monitoring_component.cli_client.commands.ServicesCommand;
import com.dse.monitoring_component.cli_client.data.DAO;
import io.github.lukas_krickl.cli_client.commands.Command;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ErrorsCommandTest {

	@Test
	void mcNotReachable_doesDisplayErrorMessage() {
		DAO.getInstance().getConnector().setMonitoringComponentURL("http://not-a-valid-url:1234");

		Command cmd = new ErrorsCommand().makeCallable(new ArrayList<>(), "");

		assertDoesNotThrow(cmd::run);
	}
}