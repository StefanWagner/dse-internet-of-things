package com.dse.messagequeuemonitoringcomponent.managers;

import com.dse.messagequeuemonitoringcomponent.exception.NotFoundException;
import com.dse.messagequeuemonitoringcomponent.TestDataGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class DataManagerTest {
	private DataManager dataManager;

	@BeforeEach
	void setUp() {
		dataManager = new DataManager();
		dataManager.setConnected(true);
		ReflectionTestUtils.setField(dataManager, "cacheSize", TestDataGenerator.generateCacheSize());
		ReflectionTestUtils.setField(dataManager, "services", TestDataGenerator.generateServices());
		ReflectionTestUtils.setField(dataManager, "errorMessageList", TestDataGenerator.generateErrorMessageList());
	}

	@AfterEach
	void tearDown() {
		dataManager = null;
	}

	@Test
	void clearErrors_sizeGreaterThanActual_throws() {
		//three error messages
		dataManager.getErrorMessageList().addError(TestDataGenerator.generateErrorMessage());
		dataManager.getErrorMessageList().addError(TestDataGenerator.generateErrorMessage());
		dataManager.getErrorMessageList().addError(TestDataGenerator.generateErrorMessage());

		//cleared no errors added
		assertThrows(NotFoundException.class, () -> dataManager.requestClearErrors(20));
	}


}
