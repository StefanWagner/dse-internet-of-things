package io.github.lukas_krickl.cli_client.visual;

/**
 * Enumeration for ANSI Color codes to transform display text in a certain color in a terminal.
 * <p>
 * NOTE: Works with most terminals but not with every, like windows CMD.
 */
public enum TextColor {
	NO_COLOR("\u001B[0m"),
	RED("\u001b[31m"),
	GREEN("\u001b[32m"),
	YELLOW("\u001b[33m"),
	BLUE("\u001b[34m"),
	MAGENTA("\u001b[35m"),
	CYAN("\u001b[36m"),
	BRIGHT_RED("\u001b[91m"),
	BRIGHT_GREEN("\u001b[92m"),
	BRIGHT_YELLOW("\u001b[93m"),
	BRIGHT_BLUE("\u001b[94m"),
	BRIGHT_MAGENTA("\u001b[95m"),
	BRIGHT_CYAN("\u001b[96m");

	private final String colorCode;
	private static final String reset = "\u001B[0m";
	private static boolean colorMode = true;

	TextColor(String colorCode) {
		this.colorCode = colorCode;
	}

	/**
	 * Returns the given text surrounded by the selected color ANSI code
	 *
	 * @param text String to colorize
	 * @return String with ANSI codes
	 */
	public String apply(String text) {
		return colorMode ? (colorCode + text + reset) : text;
	}

	public static boolean isColorMode() {
		return colorMode;
	}

	public static void setColorMode(boolean colorMode) {
		TextColor.colorMode = colorMode;
	}
}
