package io.github.lukas_krickl.cli_client.commands.general;

import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;

public class Quit extends Command {
	public Quit() {
		super(new CommandInfoBuilder("QUIT")
				.addSyntax("quit")
				.addSyntax("q")
				.addSyntax("exit")
				.setDescription("Closes the application")
				.setRequiresArgs(ArgsRequired.NOT_REQUIRED)
				.build());
	}

	@Override
	public void run() {
		UserInterface.printSection("Closing the Application ...");
	}

}
