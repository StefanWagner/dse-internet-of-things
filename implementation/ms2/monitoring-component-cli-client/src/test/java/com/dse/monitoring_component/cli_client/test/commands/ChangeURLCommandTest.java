package com.dse.monitoring_component.cli_client.test.commands;

import com.dse.monitoring_component.cli_client.data.DAO;
import com.dse.monitoring_component.cli_client.logic.NetworkConnector;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChangeURLCommandTest {
    @Test
    void changeURL_urlChanged() {
        NetworkConnector connector = DAO.getInstance().getConnector();

        connector.setMonitoringComponentURL("http://something:8080");
        String oldUrl = connector.getMonitoringComponentURL();
        connector.setMonitoringComponentURL("http://someting-else:8080");

        assertNotEquals(oldUrl, connector.getMonitoringComponentURL());
    }
}