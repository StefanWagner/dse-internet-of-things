package com.dse.messagequeuemonitoringcomponent;

import com.dse.messagequeuemonitoringcomponent.MessageQueueMonitoringComponentApplication;
import com.dse.messagequeuemonitoringcomponent.configuration.ApplicationConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;


class MessageQueueMonitoringComponentApplicationTest {

	@BeforeEach
	void setUp() {
		ApplicationConfiguration.messageQueueIPAddress = "";
		ApplicationConfiguration.clientIPAddress = "";
	}


	@ParameterizedTest
	@CsvSource({
			"1, 1",
			"2.2, 2.2",
			"3.3.3, 4.4.4.4",
			"123., 127.0.0.1",
			"127.0.0.1, 123.",
			"abc, 127.0.0.1",
			"1.1.1.1., .1.1.1.1",
			"...., ....",
			"127-0-0-1, 127-0-0-1"
	})
	void startWithInvalidArguments_urlsAreNotSetToInvalidArguments(String messageQueueArgument, String clientArgument) {
		assertEquals("", ApplicationConfiguration.messageQueueIPAddress);
		assertEquals("", ApplicationConfiguration.clientIPAddress);

		MessageQueueMonitoringComponentApplication app = new MessageQueueMonitoringComponentApplication();
		String[] args = new String[]{messageQueueArgument, clientArgument};
		ReflectionTestUtils.invokeMethod(app, "checkArguments", (Object) args);

		assertEquals("", ApplicationConfiguration.messageQueueIPAddress);
		assertEquals("", ApplicationConfiguration.clientIPAddress);
	}

	@ParameterizedTest
	@CsvSource({
			"10.102.101.9, 10.102.101.13",
			"1.1.1.1, 0.0.0.0",
			"22.22.22.22, 22.22.22.22",
			"333.333.333.333, 333.333.333.333",
			"192.168.1.1, 192.168.5.10"
	})
	void startWithValidArgs_urlsAreSetToArgs(String messageQueueArgument, String clientArgument) {
		assertEquals("", ApplicationConfiguration.messageQueueIPAddress);
		assertEquals("", ApplicationConfiguration.clientIPAddress);

		MessageQueueMonitoringComponentApplication app = new MessageQueueMonitoringComponentApplication();
		String[] args = new String[]{messageQueueArgument, clientArgument};
		ReflectionTestUtils.invokeMethod(app, "checkArguments", (Object) args);

		assertEquals(messageQueueArgument, ApplicationConfiguration.messageQueueIPAddress);
		assertEquals(clientArgument, ApplicationConfiguration.clientIPAddress);
	}


}