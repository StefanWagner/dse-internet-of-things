package com.dse.messagequeuemonitoringcomponent.network;

import com.dse.messagequeuemodel.*;
import com.dse.messagequeuemonitoringcomponent.exception.InvalidMessageReceivedException;

public class MessageValidator {

	public static void checkMetaMessage(Message message){
		if (!(message.getRecord() instanceof CacheMeta)
				&& !(message.getRecord() instanceof SubscriberMeta) ) {
			throw new InvalidMessageReceivedException(
					"Message in Topic METADATA didn´t contain a meta related Datarecord", message);
		}
	}

	public static void checkErrorMessage(Message message){
		if (!(message.getRecord() instanceof ErrorData)) {
			throw new InvalidMessageReceivedException("Message in Topic ERRORS didn´t contain a error datarecord", message);
		}
	}

	public static void checkTerminationMessage(Message message){
		if (!(message.getRecord() instanceof TerminationData)) {
			throw new InvalidMessageReceivedException("Message in Topic TERMINATION didn´t contain a termination datarecord", message);
		}
	}
}
