package com.dse.machinelearningservice.logic.anomalydetection;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.DataRecord;

public abstract class AnomalyDetectionComponent implements Runnable{

    public abstract void setUp(DataRecord record, DataRecord prediction, MessageQueueClient messageQueueClient);
}
