package com.dse.resultoutputservice;

import com.dse.resultoutputservice.database.DBManager;
import com.dse.resultoutputservice.datatypes.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;

@SpringBootTest
public class WebRestController {
    @Autowired
    private com.dse.resultoutputservice.network.webcommunication.WebRestController webRestController;
    private final static String databaseUrl = "jdbc:sqlite:../ms3/database/testDatabase.db";
    private DBManager dbManager = new DBManager();

    @BeforeEach
    void testDatabasePreparation() {
        ReflectionTestUtils.setField(dbManager, "databaseUrl", "jdbc:sqlite:../ms3/database/testDatabase.db");
        ArrayList<String> sqlStatements = new ArrayList<String>(Arrays.asList(
                "drop table anomalies;",
                "drop table airPredictions;",
                "drop table waterPredictions;",
                "create table anomalies(\n" +
                        "  id integer primary key autoincrement,\n" +
                        "  timestamp text,\n" +
                        "  errorType text,\n" +
                        "  errorDescription text\n" +
                        ");",
                "create table airPredictions(\n" +
                        "  id integer primary key autoincrement,\n" +
                        "  timestamp text,\n" +
                        "  airTemperature integer,\n" +
                        "  windSpeed integer,\n" +
                        "  humidity integer\n" +
                        ");",
                "create table waterPredictions(\n" +
                        "  id integer primary key autoincrement,\n" +
                        "  timestamp text,\n" +
                        "  waterTemperature integer,\n" +
                        "  turbidity integer,\n" +
                        "  waveHeight integer\n" +
                        ");"
        ));
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            for (String sql : sqlStatements) {
                boolean resultSet = statement.execute(sql);
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    int executeQuery(String sql) {
        Integer rowcount = null;
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            rowcount = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowcount;
    }

    @Test
    void getAnomalies_shouldQueryForAnomaliesInTestDatabase() {
        Anomaly anomaly = new Anomaly(Instant.parse("1970-01-01T00:00:00.000Z"), "NULL", "errorDescription Test");
        dbManager.insertAnomaly(anomaly);
        Anomalies anomaliesExpected = new Anomalies();
        anomaliesExpected.addAnomaly(anomaly);
        Anomalies anomalies = webRestController.getAnomalies("10", Instant.parse("1970-01-01T00:00:00.000Z"), Instant.parse("1970-01-01T00:00:00.000Z"));
        assertEquals(anomaliesExpected.getAnomalies().getFirst().getTimestamp(), anomalies.getAnomalies().getFirst().getTimestamp());
        assertEquals(anomaliesExpected.getAnomalies().getFirst().getErrorType(), anomalies.getAnomalies().getFirst().getErrorType());
        assertEquals(anomaliesExpected.getAnomalies().getFirst().getErrorDescription(), anomalies.getAnomalies().getFirst().getErrorDescription());
    }

    @Test
    void getAirPredictions_shouldQueryForAirPredictionsInTestDatabase() {
        AirPrediction airPrediction = new AirPrediction(Instant.parse("1970-01-01T00:00:00.000Z"), 1, 1, 1);
        dbManager.insertAirPrediction(airPrediction);
        AirPredictions airPredictionsExpected = new AirPredictions();
        airPredictionsExpected.addAirPrediction(airPrediction);
        AirPredictions airPredictions = webRestController.getAirPredictions("10", Instant.parse("1970-01-01T00:00:00.000Z"), Instant.parse("1970-01-01T00:00:00.000Z"));
        assertEquals(airPredictionsExpected.getAirPredictions().getFirst().getTimestamp(), airPredictions.getAirPredictions().getFirst().getTimestamp());
        assertEquals(airPredictionsExpected.getAirPredictions().getFirst().getAirTemperature(), airPredictions.getAirPredictions().getFirst().getAirTemperature());
        assertEquals(airPredictionsExpected.getAirPredictions().getFirst().getWindSpeed(), airPredictions.getAirPredictions().getFirst().getWindSpeed());
        assertEquals(airPredictionsExpected.getAirPredictions().getFirst().getHumidity(), airPredictions.getAirPredictions().getFirst().getHumidity());
    }

    @Test
    void getWaterPredictions_shouldQueryForWaterPredictionsInTestDatabase() {
        WaterPrediction waterPrediction = new WaterPrediction(Instant.parse("1970-01-01T00:00:00.000Z"), 1, 1, 1);
        dbManager.insertWaterPrediction(waterPrediction);
        WaterPredictions waterPredictionsExpected = new WaterPredictions();
        waterPredictionsExpected.addWaterPrediction(waterPrediction);
        WaterPredictions waterPredictions = webRestController.getWaterPredictions("10", Instant.parse("1970-01-01T00:00:00.000Z"), Instant.parse("1970-01-01T00:00:00.000Z"));
        assertEquals(waterPredictionsExpected.getWaterPredictions().getFirst().getTimestamp(), waterPredictions.getWaterPredictions().getFirst().getTimestamp());
        assertEquals(waterPredictionsExpected.getWaterPredictions().getFirst().getWaterTemperature(), waterPredictions.getWaterPredictions().getFirst().getWaterTemperature());
        assertEquals(waterPredictionsExpected.getWaterPredictions().getFirst().getTurbidity(), waterPredictions.getWaterPredictions().getFirst().getTurbidity());
        assertEquals(waterPredictionsExpected.getWaterPredictions().getFirst().getWaveHeight(), waterPredictions.getWaterPredictions().getFirst().getWaveHeight());
    }
}
