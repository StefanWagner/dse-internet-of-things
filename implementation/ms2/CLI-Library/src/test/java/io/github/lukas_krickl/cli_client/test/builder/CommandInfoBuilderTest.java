package io.github.lukas_krickl.cli_client.test.builder;

import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.CommandInfo;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommandInfoBuilderTest {

    @Test
    void buildCommandInfoSmokeTest() {
        CommandInfo info = new CommandInfoBuilder("TEST NAME")
                .addSyntax("test")
                .addSyntax("infoTest")
                .addParameterList()
                .addParameter("-parameter", "-parameter description")
                .addParameter("-p", "-p description")
                .finishParameterList()
                .setDescription("Test command description")
                .setRequiresArgs(ArgsRequired.OPTIONAL)
                .build();

        //check name
        assertEquals("TEST NAME", info.getName());

        //check syntax
        assertEquals(2, info.getSyntax().size());
        assertTrue(info.getSyntax().contains("test"));
        assertTrue(info.getSyntax().contains("infoTest"));

        //check parameters
        assertEquals(2, info.getParameters().size());
        assertTrue(info.getParameters().containsKey("-parameter"));
        assertEquals("-parameter description" ,info.getParameters().get("-parameter"));

        assertTrue(info.getParameters().containsKey("-p"));
        assertEquals("-p description" ,info.getParameters().get("-p"));

        assertEquals("Test command description", info.getDescription());
        assertSame(info.getArgsRequired(), ArgsRequired.REQUIRED);



    }
}