package com.dse.messagequeue.exception;

public class SubscriberNotFoundException extends RuntimeException {
    private final String subscriberURL;

    public SubscriberNotFoundException(String subscriberURL) {
        this.subscriberURL = subscriberURL;
    }

    public SubscriberNotFoundException(String message, String subscriberURL) {
        super(message);
        this.subscriberURL = subscriberURL;
    }

    public SubscriberNotFoundException(String message, Throwable cause, String subscriberURL) {
        super(message, cause);
        this.subscriberURL = subscriberURL;
    }

    public String getSubscriberURL() {
        return subscriberURL;
    }
}
