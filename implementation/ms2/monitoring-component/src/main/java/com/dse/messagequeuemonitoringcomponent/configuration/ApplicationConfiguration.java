package com.dse.messagequeuemonitoringcomponent.configuration;

import com.dse.messagequeuemodel.TopicEnum;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ApplicationConfiguration {
    public static final Set<TopicEnum> TOPIC_LIST = new HashSet<>(Arrays.asList(TopicEnum.METADATA, TopicEnum.ERRORS, TopicEnum.TERMINATION));
    public static final int CONNECTION_RETRY_INTERVAL = 3000;
    public static String messageQueueIPAddress = "";
    public static String clientIPAddress = "";

}
