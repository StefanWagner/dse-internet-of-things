package com.dse.resultoutputservice.network.messagequeuecommunication;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeueclient.MessageReceiver;
import com.dse.messagequeuemodel.*;
import com.dse.resultoutputservice.database.DBManager;
import com.dse.resultoutputservice.exceptions.InvalidDataRecordException;
import com.dse.resultoutputservice.network.converter.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Arrays;
import java.util.HashSet;

/**
 * The MessageQueueCommunication class is responsible subscribing to the message queue and receiving messages.
 */
@Component
public class MessageQueueCommunication implements MessageReceiver {
    private final static Logger log = LoggerFactory.getLogger(MessageQueueCommunication.class);
    @Value("${messagequeue.client.clienturl}")
    private String clientUrl;
    private MessageQueueClient messageQueueClient;

    /**
     * The constructor will be called by the spring framework during the project starting process.
     *
     * @param messageQueueClient object will be created during the project starting process from the
     *                           MessageQueueClientConfiguration.messageQueueClient method
     */
    public MessageQueueCommunication(MessageQueueClient messageQueueClient) {
        this.messageQueueClient = messageQueueClient;
    }

    /**
     * The subscribe method subscribes the micro service to the message queue.
     * This micro service subscribes only for the anomaly, prediction and termination topics.
     */
    @PostConstruct
    private void subscribe() {
        new Thread(()->{
            boolean isSubscribed = false;
            while (!isSubscribed) {
                try {
                    messageQueueClient.subscribeToTopics(new HashSet<TopicEnum>(Arrays.asList(TopicEnum.ANOMALY,
                            TopicEnum.PREDICTION,
                            TopicEnum.TERMINATION)));
                    isSubscribed = true;
                } catch (RestClientException e) {
                    log.info(e.getMessage());
                    isSubscribed = false;
                }
            }
            log.info("Subscribed to message queue!");
        }).start();
    }

    /**
     * The unsubscribe method unsubscribes the micro service from the message queue.
     * This micro service unsubscribes only for the anomaly, prediction and termination topics.
     */
    @PreDestroy
    private void unsubscribe() {
        messageQueueClient.unsubscribeFromTopics(new HashSet<TopicEnum>(Arrays.asList(TopicEnum.ANOMALY,
                TopicEnum.PREDICTION,
                TopicEnum.TERMINATION)));
        log.info("Unsubscribed from message queue!");
    }

    /**
     * The processMessage method checks the content of the message and determines how the specific message will be processed.
     * Messages with an unexpected content will be ignored without throwing an exception.
     *
     * @param message object which is sent by the message queue
     */
    public void processMessage(Message message) {
        if (message.getRecord() instanceof AnomalyDataRecord) {
            log.info("AnomalyDataRecord message received!");
            DBManager.insertAnomaly(Converter.messageToAnomaly(message));
        }
        if (message.getRecord() instanceof AirSensorPredictionDataRecord) {
            log.info("AirSensorPredictionDataRecord message received!");
            DBManager.insertAirPrediction(Converter.messageToAirPrediction(message));
        }
        if (message.getRecord() instanceof WaterSensorPredictionDataRecord) {
            log.info("WaterSensorPredictionDataRecord message received!");
            DBManager.insertWaterPrediction(Converter.messageToWaterPrediction(message));
        }
        if (message.getRecord() instanceof TerminationData && ((TerminationData) message.getRecord()).getTargets().contains(clientUrl)) {
            log.info("TerminationData message received!");
            System.exit(0);
        }
        if(!(message.getRecord() instanceof AnomalyDataRecord) &&
                !(message.getRecord() instanceof AirSensorPredictionDataRecord) &&
                !(message.getRecord() instanceof WaterSensorPredictionDataRecord) &&
                !(message.getRecord() instanceof TerminationData)) {
            throw new InvalidDataRecordException("Wrong DataRecord received!");
        }
    }
}
