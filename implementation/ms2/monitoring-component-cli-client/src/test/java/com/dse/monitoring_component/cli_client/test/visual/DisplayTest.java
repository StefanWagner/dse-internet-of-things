package com.dse.monitoring_component.cli_client.test.visual;

import com.dse.messagequeuemodel.*;
import com.dse.monitoring_component.cli_client.visual.Display;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class DisplayTest {

    @Test
    void printStatusTest() {
        Status testStatus = new Status(204, 22, 32);
        String newOutput = "";
        String lastOutput;

        for (int i=0; i<20; i++) {
            lastOutput = newOutput;
            newOutput = Display.formatStatus(testStatus);

            assertNotEquals(lastOutput, newOutput);

            System.out.println(newOutput); //spinner should turn

        }
    }

    @Test
    void printCache() {
        Map<TopicEnum, Integer> caches = new HashMap<>();
        for(TopicEnum topic:TopicEnum.values()) {
            caches.put(topic, 123);
        }

        String newOutput = "";
        String lastOutput;
        for (int i=0; i<3; i++) {
            lastOutput = newOutput;
            newOutput = Display.formatCache(caches);

            assertNotEquals(lastOutput, newOutput);

            System.out.println(newOutput); //spinner should turn
        }

        for(TopicEnum topic:TopicEnum.values()) {
            caches.put(topic, -1);
        }
        System.out.println(Display.formatCache(caches));
    }

    @Test
    void printErrorMessages() {
        List<Message> errorList = new ArrayList<>();
        for(int i=0; i<30; i++) {
            ErrorData data = new ErrorData("URL"+ UUID.randomUUID(), "THis error is caused somewhere", "TEXT"+UUID.randomUUID()+UUID.randomUUID());
            errorList.add(new Message(TopicEnum.ERRORS, data ));
        }

        System.out.println(Display.formatErrorMessages(errorList));
    }

    @Test
    void printServiceList() {
        Set<Subscriber> services = new HashSet<>();
        for (int i = 0; i<10; i++) {
            services.add(new Subscriber(
                    "http.//"+UUID.randomUUID(),
                    0,
                    EnumSet.of(TopicEnum.TERMINATION,TopicEnum.METADATA, TopicEnum.AIRSENSORDATA, TopicEnum.ANOMALY)));
        }

        System.out.println(Display.formatServices(services));

    }
}