package com.dse.iotsimulationservice.sensor;

import com.dse.iotsimulationservice.service.SensorManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class MockFileWaterSensorTest {


    @Mock
    SensorManager sensorManager;

    private MockFileWaterSensor mockFileWaterSensor;

    @BeforeEach
    void setup() {
        sensorManager = mock(SensorManager.class);

    }

    @Test
    void itShouldLoadDataCorrectly() {
        mockFileWaterSensor = new MockFileWaterSensor(sensorManager, "testwaterdata.csv");
        assertThat(mockFileWaterSensor.srcValues).isNotNull().containsOnlyKeys("Montrose Beach", "Ohio Street Beach", "Calumet Beach");
        assertThat(mockFileWaterSensor.srcValues.get("Ohio Street Beach")).hasSize(1);
        assertThat(mockFileWaterSensor.srcValues.get("Ohio Street Beach").get(0))
                .extracting("value1", "value2", "value3")
                .containsExactly(BigDecimal.valueOf(14.4), BigDecimal.valueOf(1.23), BigDecimal.valueOf(0.111));
    }

    @Test
    void itShouldOmitValuesCorrectly() {
        mockFileWaterSensor = new MockFileWaterSensor(sensorManager, "testwaterdata.csv");
        ReflectionTestUtils.setField(mockFileWaterSensor, "identifier", "Montrose Beach");
        mockFileWaterSensor.notifyForValueChange();
        mockFileWaterSensor.notifyForValueChange();
        verify(sensorManager, times(2)).valueOmitted(any());
    }


}