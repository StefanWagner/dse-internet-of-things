package com.dse.machinelearningservice.model;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.DataRecord;
import com.dse.messagequeuemodel.TopicEnum;

import java.util.List;

/**
 * Repository class for messages that were received by the service.
 * Allows handling of the messages themselves as well of their contained DataRecords.
 * Messages are stored in the order in which they are added to the repository, they do NOT get sorted by timestamp.
 */
public interface MessageRepository
{
    /**
     * Adds a new message to the repository.
     * @param message
     * @return the number of messages contained in the repository after inserting the new message
     */
    public int addMessage(Message message);

    /**
     * Returns a list of all messages currently available in the repository.
     * @return all messages currently available in the repository. If no messages exist, an empty list
     * will be returned.
     */
    public List<Message> getAllMessages();

    /**
     * Returns a list of all messages currently available in the repository for the given topic.
     * @return all messages for the given topic currently available in the repository. If no messages exist for the
     * topic, an empty list will be returned.
     */
    public List<Message> getMessagesByTopic(TopicEnum topic);

    /**
     * Returns a list of all data records currently available in the repository.
     * The order of the data records will be the same as that of the messages that contain them.
     * @return all data records currently available in the repository. If no data records exist, an empty list
     * will be returned.
     */
    public List<DataRecord> getAllDataRecords();

    /**
     * Returns a list of all data records currently available in the repository that belong to the given topic.
     * @param topic the message topic for which the data records should be extracted
     * @return all available data records for the given topic. If no data records exist for the topic, an empty list
     * will be returned.
     */
    public List<DataRecord> getDataRecordsByTopic(TopicEnum topic);

    /**
     * Counts the number of messages (and therefore, also data records) belonging to the given topic that are currently stored in the repository.
     * @param topic the topic for which the messages should be counted
     * @return number of messages for the given topic
     */
    public int getMessageCountByTopic(TopicEnum topic);

    /**
     * Returns the data record contained in the message with the most recent timestamp.
     * @return most recent data record , or null if no message exists in the repository
     */
    public DataRecord getMostRecentDataRecord();
}
