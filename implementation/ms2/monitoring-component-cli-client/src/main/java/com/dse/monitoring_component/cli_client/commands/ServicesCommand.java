package com.dse.monitoring_component.cli_client.commands;

import com.dse.monitoring_component.cli_client.data.DAO;
import com.dse.monitoring_component.cli_client.logic.NetworkConnector;
import com.dse.monitoring_component.cli_client.visual.Display;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

import java.io.IOException;

/**
 * A command which when called displays all connected services.
 * can be used with parameter -r to continuously refresh the printed output
 */
public class ServicesCommand extends Command {
    public ServicesCommand() {
        super(new CommandInfoBuilder("SERVICES")
                .addSyntax("services")
                .addSyntax("subscribers")
                .addParameterList()
                .addParameter("-r", "Continuously refreshes the displayed output at the configured rate")
                .finishParameterList()
                .setDescription("Displays a list of all connected services.")
                .setRequiresArgs(ArgsRequired.NOT_REQUIRED)
                .build());
    }

    @Override
    public void run() {
        if (getParams().contains("-r")) {
            displayServicesContinuously();

        } else if (getParams().isEmpty()){
            displayServicesOnce();
        }
    }

    /**
     * Requests information about all connected services of the monitoring component and
     * displays the output once
     */
    private void displayServicesOnce(){
        NetworkConnector connector = DAO.getInstance().getConnector();
        try {
            UserInterface.println(Display.formatServices(connector.requestSubscribers()));
        } catch (IOException e) {
            UserInterface.printSection(TextColor.RED.apply("ERROR: ")+e.getMessage());
        }
    }

    /**
     * Continuously refreshes the displayed output until enter is hit
     */
    private void displayServicesContinuously(){
        UserInterface ui = UserInterface.getInstance();

        Thread displayServices = createDisplayingThread();

        //Start thread to continuously display services
        displayServices.start();
        //listen for user to press enter to stop the thread
        ui.getUserConfirmation();
        //stop the thread
        displayServices.interrupt();
    }

    /**
     * Creates a new thread that continuously requests and displays information
     * about connected services until it is interrupted
     *
     * @return thread that displays the output
     */
    public Thread createDisplayingThread(){
        return new Thread(()->{
            NetworkConnector connector = DAO.getInstance().getConnector();
            int refreshRate = DAO.getInstance().getRefreshRate();
            try {
                while(true) {
                    UserInterface.print(Display.formatToNewPage());
                    UserInterface.println(Display.formatServices(connector.requestSubscribers()));
                    Thread.sleep(refreshRate);
                }

            } catch (IOException e) {
                UserInterface.printSection(TextColor.RED.apply("ERROR: ")+e.getMessage());

            } catch (InterruptedException ignored){} //when user presses enter
        });
    }
}
