package com.dse.machinelearningservice.logic.anomalydetection;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestClientException;

import java.math.BigDecimal;

/**
 * Checks water sensor values for anomalies.
 */
@Slf4j
public class WaterSensorAnomalyDetectionComponent extends AnomalyDetectionComponent
{
    private WaterSensorValue record;
    private WaterSensorPredictionDataRecord prediction;
    private MessageQueueClient messageQueueClient;


    //hard-coded limits for valid entries
    private static final BigDecimal waterTemperatureLowerLimit = BigDecimal.valueOf(0);
    private static final BigDecimal waterTemperatureUpperLimit = BigDecimal.valueOf(25);
    private static final BigDecimal waveHeightLowerLimit = BigDecimal.valueOf(0);
    private static final BigDecimal waveHeightUpperLimit = BigDecimal.valueOf(600);
    private static final BigDecimal turbidityLowerLimit = BigDecimal.valueOf(0);
    private static final BigDecimal turbidityUpperLimit = BigDecimal.valueOf(900);

    //deviation from predicted values
    private static final BigDecimal waterTemperatureMaxDeviation = BigDecimal.valueOf(10);
    private static final BigDecimal waveHeightMaxDeviation = BigDecimal.valueOf(50);
    private static final BigDecimal turbidityMaxDeviation = BigDecimal.valueOf(50);


    /**
     * Set up the data the anomaly detection component should work with.
     * @param record the water sensor values that should be checked for anomalies
     * @param prediction the current prediction for water sensor values
     * @param messageQueueClient client for communicating with the message queue
     */
    public void setUp(DataRecord record, DataRecord prediction, MessageQueueClient messageQueueClient) {
        this.record = (WaterSensorValue) record;
        this.prediction = (WaterSensorPredictionDataRecord) prediction;
        this.messageQueueClient = messageQueueClient;
    }

    private void sendAnomalyMessage(ErrorTypeEnum errorType, String description){
        //create record that describes the issue
        AnomalyDataRecord anomalyDataRecord = new AnomalyDataRecord(errorType, description);
        //create a message containing the record
        Message anomalyMessage = new Message(TopicEnum.ANOMALY, anomalyDataRecord);
        //info log
        log.info("Sending anomaly message: Error type is " + anomalyDataRecord.getErrorType() + ", error message is " + anomalyDataRecord.getErrorDescription());

        //send message
        //for local tests messageQueueClient can be set to null in the constructor
        try {
            if (messageQueueClient != null) messageQueueClient.publishMessage(anomalyMessage);
        } catch(RestClientException e) {
            log.info("Could not send message");
            e.printStackTrace();
        }
    }

    /**
     * Check if the sensor values data record contains any null values.
     * @return the number of null values that were found
     */
    public int findNullValueAnomaly() {
        int nullValuesFound = 0;

        //check all fields and send a separate message for each one that is null
        //return the number of null values that were found
        if (record.getWaterTemperature() == null) {
            sendAnomalyMessage(ErrorTypeEnum.NULL, "Water temperature at sensor " + record.getBeachName() + " is null");
            nullValuesFound++;
        }
        if (record.getTurbidity() == null) {
            sendAnomalyMessage(ErrorTypeEnum.NULL, "Turbidity at sensor " + record.getBeachName() + " is null");
            nullValuesFound++;
        }
        if (record.getWaveHeight() == null) {
            sendAnomalyMessage(ErrorTypeEnum.NULL, "Wave height at sensor " + record.getBeachName() + " is null");
            nullValuesFound++;
        }

        return nullValuesFound;
    }


    /**
     * Check if the received values are outside of the hard-coded bounds.
     * @return the number of entries that are outside the bounds
     */
    public int findOutOfBoundsAnomaly() {
        int outOfBoundsValuesFound = 0;

        if (record.getWaterTemperature().compareTo(waterTemperatureLowerLimit) == -1 || record.getWaterTemperature().compareTo(waterTemperatureUpperLimit) == 1 ) {
            sendAnomalyMessage(ErrorTypeEnum.VALUEOFFLIMIT, "Water temperature at sensor " + record.getBeachName() + " is " +
                    "out of bounds");
            outOfBoundsValuesFound++;
        }
        if (record.getTurbidity().compareTo(turbidityLowerLimit) == -1 || record.getTurbidity().compareTo(turbidityUpperLimit) == 1 ) {
            sendAnomalyMessage(ErrorTypeEnum.VALUEOFFLIMIT, "Turbidity at sensor " + record.getBeachName() + " is out of bounds");
            outOfBoundsValuesFound++;
        }
        if (record.getWaveHeight().compareTo(waveHeightLowerLimit) == -1 || record.getWaveHeight().compareTo(waveHeightUpperLimit) == 1 ) {
            sendAnomalyMessage(ErrorTypeEnum.VALUEOFFLIMIT, "Wave height at sensor " + record.getBeachName() + " is out of bounds");
            outOfBoundsValuesFound++;
        }
        return outOfBoundsValuesFound;
    }


    /**
     * Check if the sensor values deviate strongly from the current prediction.
     * @return the number of entries in the data record that deviate from the prediction
     */
    public int findPredictionAnomaly() {
        int predictionAnomaliesFound = 0;

        if (record.getWaterTemperature().subtract(prediction.getWaterTemperature()).abs().compareTo(waterTemperatureMaxDeviation) == 1) {
            sendAnomalyMessage(ErrorTypeEnum.PREDICTION, "Water temperature at sensor " + record.getBeachName() +
                            " deviates from predicted value");
            predictionAnomaliesFound++;
        }

        if (record.getTurbidity().subtract(prediction.getTurbidity()).abs().compareTo(turbidityMaxDeviation) == 1) {
            sendAnomalyMessage(ErrorTypeEnum.PREDICTION, "Turbidity at sensor " + record.getBeachName() +
                    " deviates from predicted value");
            predictionAnomaliesFound++;
        }

        if (record.getWaveHeight().subtract(prediction.getWaveHeight()).abs().compareTo(waveHeightMaxDeviation) == 1) {
            sendAnomalyMessage(ErrorTypeEnum.PREDICTION, "Wave height at sensor " + record.getBeachName() +
                    " deviates from predicted value");
            predictionAnomaliesFound++;
        }

        return predictionAnomaliesFound;
    }


    @Override
    public void run() {
        log.info("Water sensor anomaly detection started on thread " + Thread.currentThread().getId());

        int nullValuesFound = findNullValueAnomaly();

        //if there are null values in the data, there's no need to continue
        if (nullValuesFound > 0) return;

        findOutOfBoundsAnomaly();

        if (prediction != null) { //prediction is null if no prediction for the sensortype has been created yet
            findPredictionAnomaly();
        }
    }
}
