package com.dse.machinelearningservice.logic.prediction;

import com.dse.machinelearningservice.model.MessageRepository;
import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.Message;

import java.util.List;

public abstract class PredictionComponent implements Runnable{

    public abstract void setUp(MessageQueueClient messageQueueClient, List<Message> messageList, MessageRepository predictionRepository);

}
