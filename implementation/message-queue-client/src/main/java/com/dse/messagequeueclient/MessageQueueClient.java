package com.dse.messagequeueclient;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.messagequeuemodel.requestwrapper.ChangeDelayRequestWrapper;
import com.dse.messagequeuemodel.requestwrapper.RequestObject;
import com.dse.messagequeuemodel.requestwrapper.UnsubscribeRequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Client to access Messagequeue via several operations
 */
public class MessageQueueClient {

    /**
     * The url of the messagequeue to connect. Needs to be defined in application.properties as 'messagequeue.client.messagequeueurl'.
     * Make sure the url has the form "http.//my.url:portNumber" (no slash at the end)
     */
    private String messageQueueUrl;

    /**
     * The url of the client connecting to the messagequeue. Needs to be defined in application.properties as 'messagequeue.client.clienturl'.
     * Make sure the url has the form "http.//my.url:portNumber" (no slash at the end)
     */
    private String clientUrl;

    /**
     * The subscriber that connects to the messagequeue. If no subscriber is in the Optional it will be registered.
     */
    private Optional<Subscriber> subscriberContainer;

    /**
     * The Resttemplate used to send requests to the message queue. It is already set with the baseurl so calls should
     * look like this: "restTemplate.getForObject("/subscriptions", Subscriber.class)"
     */
    private RestTemplate restTemplate;

    public MessageQueueClient(String clientUrl, String messageQueueUrl) {
        this(clientUrl, messageQueueUrl, null);
    }


    MessageQueueClient(String clientUrl, String messageQueueUrl, RestTemplate restTemplate) {
        if (messageQueueUrl == null || clientUrl == null) {
            throw new IllegalStateException("Missing properties messagequeue.client.messagequeueurl or messagequeue.client.clienturl");
        }
        this.clientUrl = clientUrl;
        this.messageQueueUrl = messageQueueUrl;
        this.subscriberContainer = Optional.empty();
        this.restTemplate = restTemplate != null ?
                restTemplate :
                new RestTemplateBuilder()
                        .rootUri(messageQueueUrl)
                        .build();
    }

    /**
     * Subscribes to the given topics
     * Creates a new subscriber if not existing yet
     * or adds given topics if subscriber exists
     *
     * <p>
     * throws a {@link RestClientException} if an error occurs
     * throws a {@link RestClientResponseException} if an error code is returned by the server
     *
     * @param topics new topics to subscribe
     */
    public synchronized void subscribeToTopics(Set<TopicEnum> topics) throws RestClientException {
        //create a new subscriber if it doesn't exist yet
        Subscriber subscriber = subscriberContainer.orElse(new Subscriber(clientUrl, 0, new HashSet<>()));
        //add given topics to topic list
        Set<TopicEnum> newTopicList = subscriber.getTopics();
        newTopicList.addAll(topics); //modifying reference is ok as subscriber setter/getter works with copies
        subscriber.setTopics(newTopicList);

        Subscriber response = restTemplate.postForObject("/subscriptions", subscriber, Subscriber.class);

        subscriberContainer = response == null ? Optional.empty() : Optional.of(response);
    }

    /**
     * Unsubscribe from topics/ deletes subscriber if unsubscribed from all topics
     * <p>
     * RestTemplate has no DELETE method with return type but the response Subscriber is needed
     * so the request has to be made by hand
     * <p>
     * throws a {@link RestClientException} if an error occurs
     * throws a {@link RestClientResponseException} if an error code is returned by the server
     *
     * @param topics topics to unsubscribe from
     * @throws RestClientException if a error code is returned
     */
    public void unsubscribeFromTopics(Set<TopicEnum> topics) throws RestClientException {
        Subscriber subscriber = subscriberContainer.orElseThrow(() ->
                new IllegalArgumentException("Wanted to unsubscribe from topics, but subscriber didn't even exist!"));

        //make request
        Subscriber response = makeHttpRequest(
                new UnsubscribeRequestWrapper(subscriber, topics),
                HttpMethod.DELETE,
                "/subscriptions");

        //if all topics are unsubscribed the subscriber is deleted from the MQ and so from this client
        subscriberContainer = (response.getTopics().size() == 0) ? Optional.empty() : Optional.of(response);
    }


    /**
     * Changes the delay of the subscriber
     * RestTemplate has no PUT method with return type but the response Subscriber is needed
     * so the request has to be made by hand
     *
     * @param delayInMillis new delay
     * @throws RestClientException if an error occurred
     */
    public synchronized void delayMessages(int delayInMillis) throws RestClientException {
        Subscriber subscriber = subscriberContainer.orElseThrow(() ->
                new IllegalArgumentException("Wanted to change delay of subscriber, but subscriber didn't even exist!"));

        Subscriber response = makeHttpRequest(
                new ChangeDelayRequestWrapper(subscriber, delayInMillis),
                HttpMethod.PUT,
                "/delays"
        );
        subscriberContainer = Optional.of(response);
    }

    /**
     * Publishes a message to the message queue
     * <p>
     * throws a {@link RestClientException} if an error occurs
     * throws a {@link RestClientResponseException} if an error code is returned by the server
     *
     * @param message message to publish
     */
    public void publishMessage(Message message) throws RestClientException {
        try {
            System.out.println("sending\n" + new ObjectMapper().findAndRegisterModules().writeValueAsString(message));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        restTemplate.postForObject("/messages", message, String.class);
    }

    /**
     * Makes a http request with request and response body
     * -> is needed as RestTemplate doesn't provide a higher level method for our needs
     * <p>
     * throws a {@link RestClientException} if an error occurs
     * throws a {@link RestClientResponseException} if an error code is returned by the server
     *
     * @param requestObject the object for the request body
     * @param httpMethod    http method of the request
     * @param endPoint      the url without the base url e.g. "/subscriptions"
     * @return a subscriber returned by the MQ
     * @throws RestClientException if an error occurred
     */
    private Subscriber makeHttpRequest(RequestObject requestObject, HttpMethod httpMethod, String endPoint) throws RestClientException {
        //Construct request
        URI uri = URI.create(messageQueueUrl + endPoint);

        //construct Request header
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name()); //needed for error response exception
        header.add(HttpHeaders.ACCEPT, "application/json");
        RequestEntity<RequestObject> requestEntity = new RequestEntity<>(requestObject, header, httpMethod, uri);

        //make request
        ResponseEntity<Subscriber> responseEntity = restTemplate.exchange(uri, httpMethod, requestEntity, Subscriber.class);

        return handleErrorCodes(responseEntity);
    }

    /**
     * Handle error codes of http request
     *
     * @param responseEntity response with error codes from http request
     */
    private <T> T handleErrorCodes(ResponseEntity<T> responseEntity) {
        //if a error or no subscriber is returned throw exception
        if (responseEntity.getStatusCode().isError() || !(responseEntity.getBody() instanceof Subscriber)) {
            if (responseEntity.hasBody() && responseEntity.getBody() instanceof String) {
                //if a error message is returned, throw detailed exception
                throw new RestClientResponseException(
                        (String) responseEntity.getBody(),
                        responseEntity.getStatusCodeValue(),
                        responseEntity.getStatusCode().getReasonPhrase(),
                        responseEntity.getHeaders(),
                        ((String) responseEntity.getBody()).getBytes(),
                        StandardCharsets.UTF_8);
            }
            throw new RestClientException("Error code returned in http request: " +
                    responseEntity.getStatusCodeValue() + " - " +
                    responseEntity.getStatusCode().getReasonPhrase());
        }
        return responseEntity.getBody();
    }

    public Optional<Subscriber> getSubscriber() {
        return subscriberContainer;
    }
}
