package com.dse.messagequeuemonitoringcomponent.managers.messages;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.SubscriberMeta;
import com.dse.messagequeuemonitoringcomponent.managers.ApplicationManager;
import com.dse.messagequeuemonitoringcomponent.managers.DataManager;

import java.time.Instant;

/**
 * A manager class which manages incoming information about
 * connected services/subscribers
 *
 * Subscribers are tracked by SubscriberMeta Messages with a Status on what happened
 *
 * It also manages the cached termination message for following situation:
 *
 * +) a subscriber delete status is received but the subscriber didn`t even exist
 *  -> the subscriber did exist but the mc didn´t receive the message yet
 *

 */
public class SubscriberMessageManager {
    private final DataManager dataManager;
    private final ApplicationManager applicationManager;

    public SubscriberMessageManager(DataManager dataManager, ApplicationManager applicationManager) {
        this.dataManager = dataManager;
        this.applicationManager = applicationManager;
    }

    /**
     * Processes a SubscriberMeta Message
     * Compares the received Message timestamp with the stored state timestamp
     * and stores the most recent information
     *
     * @param message
     */
    public void manageSubscribers(Message message) {
        SubscriberMeta data = (SubscriberMeta) message.getRecord();
        Subscriber subscriber = data.getSub();

        switch (data.getStatus()) {
            case CREATED: {
                createSubscriber(subscriber, message.getTimestamp());
                break;
            }
            case CHANGED: {
                changeSubscriber(subscriber, message.getTimestamp());
                break;
            }

            case DELETED: {
                deleteSubscriber(subscriber, message.getTimestamp());
                break;
            }
            default:
                throw new RuntimeException("SubscriberMeta unknown Status found");
        }
    }

    /**
     * Handle a received Subscriber meta with the message that it was created
     *
     * @param receivedSubscriber the received subscriber object
     * @param receivedTimestamp the timestamp of the received message
     */
    private void createSubscriber(Subscriber receivedSubscriber, Instant receivedTimestamp) {
        //add subscriber
        if (!dataManager.getServices().containsKey(receivedSubscriber)) {
            //if subscriber already exists
            if (dataManager.getCachedServiceTermination().containsKey(receivedSubscriber.getUrl())) {
                //if there is a cached termination message
                if (dataManager.getCachedServiceTermination().get(receivedSubscriber.getUrl()).isBefore(receivedTimestamp)) {
                    // if the cached termination message is older, discard it
                    dataManager.getCachedServiceTermination().remove(receivedSubscriber.getUrl());
                } else {
                    //if the cached termination message is newer don't add the subscriber and wait for a newer status update:
                    // + the service did exist but the ms2 didn't get the create message
                    // + the service could have been deleted and this is a second message
                    return;
                }
            }
            dataManager.getServices().put(receivedSubscriber, receivedTimestamp);

        } else if (dataManager.getServices().get(receivedSubscriber).isBefore(receivedTimestamp)) {
            //if the stored state is older, refresh it with the received message
            //replace method would not change the subscriber, only timestamp
            dataManager.getServices().remove(receivedSubscriber);
            dataManager.getServices().put(receivedSubscriber, receivedTimestamp);
            applicationManager.reportExternalErrorToMessageQueue(
                    "Message Queue"
                    , "Subscriber update anomaly: subscriber created but already exists");
        }
    }

    private void changeSubscriber(Subscriber receivedSubscriber, Instant receivedTimestamp) {
        //update stored subscriber if necessary
        if (dataManager.getServices().containsKey(receivedSubscriber)) {
            //if the stored state is older replace it
            if (dataManager.getServices().get(receivedSubscriber).isBefore(receivedTimestamp)) {
                dataManager.getServices().remove(receivedSubscriber);
                dataManager.getServices().put(receivedSubscriber, receivedTimestamp);
            }
        } else {
            if (dataManager.getCachedServiceTermination().containsKey(receivedSubscriber.getUrl())) {
                //if there is a cached termination message for this service
                if (dataManager.getCachedServiceTermination().get(receivedSubscriber.getUrl()).isBefore(receivedTimestamp)) {
                    // if the cached termination message is older, discard it
                    dataManager.getCachedServiceTermination().remove(receivedSubscriber.getUrl());
                } else {
                    //if the cached termination message is newer don't add the subscriber and wait for a newer status update:
                    // + the service could have been deleted and this is a second message
                    // + the service did exist but the ms2 didn't get the create message
                    return;
                }
            }

            dataManager.getServices().put(receivedSubscriber, receivedTimestamp);
            applicationManager.reportExternalErrorToMessageQueue(
                    "Message Queue"
                    , "Subscriber update anomaly: subscriber has changed but doesn't even exist");
        }
    }

    private void deleteSubscriber(Subscriber receivedSubscriber, Instant receivedTimestamp) {
        //if subscriber exist and message is newer than the stored state, delete it
        //if the message is older than the stored state, do nothing, because service has reconnected in between
        if (dataManager.getServices().containsKey(receivedSubscriber)) {
            if (dataManager.getServices().get(receivedSubscriber).isBefore(receivedTimestamp)) {
                dataManager.getServices().remove(receivedSubscriber);
            }
        } else {
            dataManager.getCachedServiceTermination().put(receivedSubscriber.getUrl(), receivedTimestamp);
        }
    }
}
