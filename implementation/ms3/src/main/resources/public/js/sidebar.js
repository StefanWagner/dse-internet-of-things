function changeToGrid(){
    var view1 = document.getElementById('view-1');
    var view2 = document.getElementById('view-2');
    var container = document.getElementById('container');

    if (!container.classList.contains('row')) {
        container.classList.add('row');
    }
    if (!view1.classList.contains('col')) {
        container.classList.add('col');
    }
    if (!view2.classList.contains('col')) {
        container.classList.add('col');
    }
}

function changeToList(){
    var view1 = document.getElementById('view-1');
    var view2 = document.getElementById('view-2');
    var container = document.getElementById('container');

    if (container.classList.contains('row')) {
        container.classList.remove('row');
    }
    if (view1.classList.contains('col')) {
        container.classList.remove('col');
    }
    if (view2.classList.contains('col')) {
        container.classList.remove('col');
    }
}
