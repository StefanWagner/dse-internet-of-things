package com.dse.messagequeue.manager;

import org.junit.jupiter.api.Test;

class MetaManagerTest {
	private void throwsRuntimeException() {
		throw new RuntimeException("Example Exception");
	}

	private static String stackTraceToString(StackTraceElement[] stackTrace) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i< Math.min(10, stackTrace.length); i++){
			sb.append(stackTrace[i].toString()).append("\n  ");
		}

		return sb.toString();
	}

	@Test
	public void printUsefulStackTrace(){
		try {
			throwsRuntimeException();
		} catch (RuntimeException e) {
			System.out.println(stackTraceToString(e.getStackTrace()));
		}
	}


}