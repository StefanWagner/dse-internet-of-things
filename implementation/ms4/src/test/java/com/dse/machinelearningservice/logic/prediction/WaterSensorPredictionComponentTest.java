package com.dse.machinelearningservice.logic.prediction;

import com.dse.machinelearningservice.logic.prediction.WaterSensorPredictionComponent;
import com.dse.machinelearningservice.model.MessageRepository;
import com.dse.machinelearningservice.model.SimpleMessageRepository;
import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.*;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WaterSensorPredictionComponentTest {

    private MessageQueueClient client; //setting the message queue client to null --> no messages will be sent, only local testing
    private MessageRepository predictionRepository;

    @Before
    public void setUp() {
        client = null;
        predictionRepository = new SimpleMessageRepository();
    }

    @Test
    public void testMessageListSorting() throws InterruptedException{
        //create list of messages
        List<Message> messages = new ArrayList<Message>();

        //create three messages with distinct time stamps (using thread.sleep) and insert into list out of order
        Message m1 = new Message(TopicEnum.PREDICTION, null);
        Thread.sleep(2000); //wait for two seconds
        Message m2 = new Message(TopicEnum.PREDICTION, null);
        Thread.sleep(2000); //wait for two seconds
        Message m3 = new Message(TopicEnum.PREDICTION, null);
        messages.add(m2);
        messages.add(m3);
        messages.add(m1);

        //create prediction component and make it sort the list
        WaterSensorPredictionComponent waterSensorPredictionComponent = new WaterSensorPredictionComponent();
        waterSensorPredictionComponent.setUp(client, messages, predictionRepository);
        waterSensorPredictionComponent.sortMessageListByDates();
        List<Message> sortedMessages = waterSensorPredictionComponent.getMessageList();

        //check that the messages are now sorted
        assertEquals(sortedMessages.size(), 3);
        assertTrue(sortedMessages.get(0).getTimestamp().isBefore(sortedMessages.get(1).getTimestamp()));
        assertTrue(sortedMessages.get(1).getTimestamp().isBefore(sortedMessages.get(2).getTimestamp()));
    }


    @Test
    public void testPredictionCreation(){
        //create list of sensor values
        List<WaterSensorValue> values = new ArrayList<WaterSensorValue>();
        for (int i = 0; i < 5; ++i) {
            WaterSensorValue val = new WaterSensorValue(Instant.now(), SensorTypeEnum.AIR, "station" + i,
                    BigDecimal.valueOf(i), BigDecimal.valueOf(i), BigDecimal.valueOf(i));
            values.add(val);
        }

        //create prediction component
        WaterSensorPredictionComponent waterSensorPredictionComponent = new WaterSensorPredictionComponent();
        waterSensorPredictionComponent.setUp(client, null, predictionRepository);

        //get the prediction
        WaterSensorPredictionDataRecord prediction = waterSensorPredictionComponent.calculatePrediction(values);

        //make sure the returned results are correct
        //(0 + 1 + 2 + 3 + 4) / 5 = 2
        assertTrue(prediction.getWaterTemperature().doubleValue() == 2.);
        assertTrue(prediction.getWaveHeight().doubleValue() == 2.);
        assertTrue(prediction.getTurbidity().doubleValue() == 2.);
    }

    @Test
    public void testGetSensorValuesFromLongMessageList() {
        //create list of messages
        List<Message> messages = new ArrayList<Message>();
        for (int i = 0; i < 20; ++i) {
            DataRecord record = new WaterSensorValue(Instant.now(), SensorTypeEnum.AIR, "station" + i,
                    BigDecimal.valueOf(i), BigDecimal.valueOf(i), BigDecimal.valueOf(i));
            messages.add(new Message(TopicEnum.WATERSENSORDATA, record));
        }

        //create prediction component and make it get the moving average window
        WaterSensorPredictionComponent waterSensorPredictionComponent = new WaterSensorPredictionComponent();
        waterSensorPredictionComponent.setUp(client, messages, predictionRepository);
        List<WaterSensorValue> values = waterSensorPredictionComponent.getSensorValuesFromMovingAverageWindow();

        //make sure the number of returned entries is the size of the moving average window
        assertEquals(values.size(), 10);
    }

    @Test
    public void testGetSensorValuesFromShortMessageList() {
        //create list of messages
        List<Message> messages = new ArrayList<Message>();
        for (int i = 0; i < 4; ++i) {
            DataRecord record = new WaterSensorValue(Instant.now(), SensorTypeEnum.AIR, "station" + i,
                    BigDecimal.valueOf(i), BigDecimal.valueOf(i), BigDecimal.valueOf(i));
            messages.add(new Message(TopicEnum.WATERSENSORDATA, record));
        }

        //create prediction component and make it get the moving average window
        WaterSensorPredictionComponent waterSensorPredictionComponent = new WaterSensorPredictionComponent();
        waterSensorPredictionComponent.setUp(client, messages, predictionRepository);
        List<WaterSensorValue> values = waterSensorPredictionComponent.getSensorValuesFromMovingAverageWindow();

        //make sure the number of returned entries is the number of available entries
        assertEquals(values.size(), 4);
    }


}
