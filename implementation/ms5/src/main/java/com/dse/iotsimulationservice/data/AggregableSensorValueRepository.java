package com.dse.iotsimulationservice.data;

import com.dse.messagequeuemodel.AggregableValue;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Repository storing the ommited values as a cache
 */
@Repository
public class AggregableSensorValueRepository implements SensorValueRepository {

    /**
     * List of values which are cached
     */
    private List<AggregableValue> cachedValues = new ArrayList<>();


    /**
     * Adds a new value to the list of cached values
     *
     * @param sensorValue the value to be added
     */
    @Override
    public void addValue(@Valid @NotNull AggregableValue sensorValue) {
        this.cachedValues.add(sensorValue);
    }

    /**
     * @return a list of all values which are currently in the cache
     */
    @Override
    public List<AggregableValue> getAllCachedValues() {
        return new ArrayList<>(cachedValues);
    }

    /**
     * @return the current size of the cache list
     */
    @Override
    public int getValueCacheSize() {
        return cachedValues.size();
    }

    /**
     * empties the cache list
     */
    @Override
    public void clearCache() {
        this.cachedValues = new ArrayList<>();
    }
}
