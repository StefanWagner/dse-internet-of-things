package com.dse.messagequeuemodel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

class ErrorTypeEnumTest {

    @Test
    public void canSerialize() {
        ObjectMapper a = new ObjectMapper();
        String s = null;
        ErrorTypeEnum e = null;
        try {
            s = a.writeValueAsString(ErrorTypeEnum.PREDICTION);
            e = a.readValue(s, ErrorTypeEnum.class);
        } catch (JsonProcessingException x) {
            x.printStackTrace();
            assert (false);
        }

        assert (e == ErrorTypeEnum.PREDICTION);
        assert (s.equals("{\"type\":\"PREDICTION\"}"));

    }
}