package com.dse.messagequeueclient;

import com.dse.messagequeuemodel.Message;

public interface MessageReceiver {
	void processMessage(Message message);
}
