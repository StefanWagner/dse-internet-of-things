package com.dse.messagequeueclient;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.TopicEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

class MessageQueueClientReceiveControllerTest {

    @Mock
    private MessageReceiver messageReceiver;

    private MessageQueueClientReceiveController messageQueueClientReceiveController;

    @BeforeEach
    private void setup() {
        initMocks(this);
        messageQueueClientReceiveController = new MessageQueueClientReceiveController(messageReceiver);
    }

    @Test
    void itShouldReturn200OnMessageProcessing() {
        Message msg = new Message(TopicEnum.PREDICTION, null);
        ResponseEntity response = messageQueueClientReceiveController.processMessage(msg);
        assertThat(response.getStatusCode())
                .isNotNull()
                .isEqualTo(HttpStatus.OK);

    }
}