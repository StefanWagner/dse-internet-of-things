package com.dse.messagequeuemonitoringcomponent.data;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemonitoringcomponent.exception.InvalidInputException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A container class for storing all errors received by the message queue.
 * All operations are thread safe to provide multithreading functionality.
 */
public class ErrorMessageList {
    private LinkedList<Message> errors;

    public ErrorMessageList() {
        errors = new LinkedList<>();
    }

    /**
     * Appends an error message to the internal list.
     *
     * @param msg error message
     */
    public synchronized void addError(Message msg) {
        errors.addFirst(msg);
    }

    /**
     * Returns an array containing the last messages specified by count.
     * This method returns an new allocated array, to prevent concurrent modification exceptions
     *
     * @return amount of count newest error messages
     */
    public synchronized List<Message> getErrors(int count) {
        return new ArrayList<>(errors.subList(0, count));
    }

    public synchronized List<Message> getAllErrors() {
        return new ArrayList<>(errors);
    }

    /**
     * Deletes all stored errors
     */
    public synchronized void clearErrors() {
        errors.clear();
    }

    /**
     * Deletes all errors except of the newest ones. The amount of the newest errors to keep is specified by size.
     *
     * @param size amount of newest errors kept.
     */
    public synchronized void clearErrors(int size) {
        errors.subList(size, errors.size()).clear();
    }

    public synchronized int getErrorCount() {
        return errors.size();
    }
}
