package com.dse.messagequeuemonitoringcomponent;

import com.dse.messagequeuemodel.*;
import com.dse.messagequeuemonitoringcomponent.data.ErrorMessageList;
import com.dse.messagequeuemonitoringcomponent.data.StoredCacheData;
import com.dse.messagequeuemonitoringcomponent.managers.DataManager;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Instant;
import java.util.EnumSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class TestDataGenerator {

	public static Message generateErrorMessage() {
		ErrorData dataRecord = new ErrorData("http://TEST-URL-" + UUID.randomUUID() + ":8080", "test cause", "test error message");
		return new Message(TopicEnum.ERRORS, dataRecord);
	}

	public static Message generateCacheMetaMessage(TopicEnum topic, int cacheSize) {
		CacheMeta data = new CacheMeta(topic, cacheSize);
		return new Message(TopicEnum.METADATA, data);
	}

	public static Message generateSubscriberMetaMsg(Subscriber subscriber, SubscriberMeta.Status status) {
		SubscriberMeta data = new SubscriberMeta(subscriber, status);
		return new Message(TopicEnum.METADATA, data);
	}

	public static ConcurrentHashMap<TopicEnum, StoredCacheData> generateCacheSize() {
		ConcurrentHashMap<TopicEnum, StoredCacheData> cacheSize = new ConcurrentHashMap<>();
		for (TopicEnum topic : TopicEnum.values()) {
			cacheSize.put(topic, new StoredCacheData(123, Instant.now()));
		}
		return cacheSize;
	}

	public static ConcurrentHashMap<Subscriber, Instant> generateServices() {
		ConcurrentHashMap<Subscriber, Instant> services = new ConcurrentHashMap<>();
		for (int i = 1; i <= 10; i++) {
			Subscriber sub = new Subscriber(
					"http://test-subscriber-" + i + ":8080",
					0,
					EnumSet.allOf(TopicEnum.class));
			services.put(sub, Instant.now());
		}
		return services;
	}

	public static ErrorMessageList generateErrorMessageList() {
		ErrorMessageList list = new ErrorMessageList();

		for (int i = 0; i < 10; i++) {
			list.addError(generateErrorMessage());
		}
		return list;
	}

	public static DataManager generateDataManagerWithData() {
		DataManager dataManager = new DataManager();
		dataManager.setConnected(true);
		ReflectionTestUtils.setField(dataManager, "cacheSize", TestDataGenerator.generateCacheSize(), ConcurrentHashMap.class);
		ReflectionTestUtils.setField(dataManager, "services", TestDataGenerator.generateServices(), ConcurrentHashMap.class);
		ReflectionTestUtils.setField(dataManager, "errorMessageList", TestDataGenerator.generateErrorMessageList(), ErrorMessageList.class);

		return dataManager;
	}


}
