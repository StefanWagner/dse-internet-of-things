package com.dse.messagequeue.queue;


import com.dse.messagequeue.manager.SubscriptionManager;
import com.dse.messagequeuemodel.Subscriber;
import org.springframework.web.client.RestTemplate;

public class QueueFactory {

    private final static int MAX_QUEUE_SIZE = 100_000;

    private QueueFactory() {
    }

    public static Queue createQueue(Subscriber subscriber, RestTemplate restTemplate, SubscriptionManager subscriptionManager) {
        return new Queue(MAX_QUEUE_SIZE, subscriber, restTemplate, subscriptionManager);
    }

}
