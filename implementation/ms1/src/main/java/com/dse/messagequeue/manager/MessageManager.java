package com.dse.messagequeue.manager;

import com.dse.messagequeue.data.MessageQueueRepository;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Service to manage functionality regarding messages, such as publishing or caching messages
 */
@Service
public class MessageManager {
    private static final Logger log = LoggerFactory.getLogger(MessageManager.class);
    private final MessageQueueRepository messageQueueRepository;

    public MessageManager(MessageQueueRepository messageQueueRepository) {
        this.messageQueueRepository = messageQueueRepository;
        MetaManager.setMessageManager(this);
    }

    /**
     * Spreads the received message into the queues of all subscribers subscribed the message's topic
     *
     * @param message incoming message
     */
    @Async
    public void processMessage(Message message) {
        TopicEnum topic = message.getTopic();
        //if topic not subscribed, cache it
        if (messageQueueRepository.isCached(topic)) {
            messageQueueRepository.getCacheQueueByTopic(topic).add(message);
            //don't report meta message with meta message -> results in loop
            if (message.getTopic() != TopicEnum.METADATA) {
                MetaManager.cacheSizeUpdated(topic, messageQueueRepository.getCacheQueueByTopic(topic).size());
            }
        } else {
            for (Subscriber subscriber : messageQueueRepository.getSubscribersByTopic(topic)) {
                messageQueueRepository.getQueueByURL(subscriber.getUrl()).addMessage(message);
            }
        }
    }


}
