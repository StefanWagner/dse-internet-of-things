package com.dse.iotsimulationservice;

import com.dse.iotsimulationservice.util.ExitDeniedSecurityManager;
import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeueclient.MessageReceiver;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.TerminationData;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.messagequeuemodel.WaterSensorValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("integration-test")
class IotSimulationServiceIntegrationTest {

    @Autowired
    private MessageReceiver receiver;

    @SpyBean
    private MessageQueueClient messageQueueClient;

    private ArgumentCaptor<Message> argument;

    @BeforeEach
    void setUp() {
        argument = ArgumentCaptor.forClass(Message.class);
    }

    @Test
    void itShouldPublishValues() {
        doNothing().when (messageQueueClient).publishMessage(any());
        await().atMost(2, TimeUnit.SECONDS).untilAsserted(() -> {
            verify(messageQueueClient, atLeast(1)).publishMessage(argument.capture());
            assertThat((WaterSensorValue) argument.getValue().getRecord())
                    .isNotNull()
                    .extracting("waterTemperature", "turbidity", "waveHeight")
                    .doesNotContainNull();
        });
        await().atMost(7, TimeUnit.SECONDS).untilAsserted(() ->
                verify(messageQueueClient, atLeast(5)).publishMessage(any()));
    }

    @Test
    void itShouldTerminateOnTerminationMessageWithFittingUrl() {
        //setup
        SecurityManager defaultSecurityManager = System.getSecurityManager();
        System.setSecurityManager(new ExitDeniedSecurityManager());

        Set<String> urls = new HashSet<>();
        urls.add("http://localhost:8080");
        TerminationData terminationData = new TerminationData(urls);
        Message msg = new Message(TopicEnum.TERMINATION, terminationData);

        //invoke call
        assertThatExceptionOfType(ExitDeniedSecurityManager.ExitSecurityException.class)
                .isThrownBy(() -> receiver.processMessage(msg));

        System.setSecurityManager(defaultSecurityManager);
    }

}
