package com.dse.messagequeuemodel;

import lombok.Value;

/**
 * Metadata describing the size of the cache queue for a topic
 */
@Value
public class CacheMeta implements DataRecord {
    private TopicEnum topic; //regarding topic
    /**
     * Size of the cache:
     * -1 = not cached
     * 0 = is cached but no messages stored
     * >0 = amount of messages stored
     */
    private int cacheSize;
}
