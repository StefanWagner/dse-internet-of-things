package com.dse.iotsimulationservice.aggregator;

import com.dse.messagequeuemodel.AggregableValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Filters the a List by trimming off the largest and smallest values.
 */
@Component
public class TrimFilter implements Filter {

    /**
     * The number of values to trim from each side (eg. 2 means trimming the two smallest and 2 largest values)
     */
    private final int trimValues;

    public TrimFilter(@Value("${sensor.filtering.trimsize}") int trimValues) {
        this.trimValues = trimValues;
    }

    /**
     * Filters the values by trimming off the min and max values for each field, by setting it to null
     * Throws an exception if the list is empty or there are not enough values to leave any after trimming
     *
     * @param values The list of values to be filtered. Must be at least 2*trimValues+1
     * @return a List of values with the trimmed fields being set to null
     */
    @Override
    public List<AggregableValue> filterValues(List<AggregableValue> values) {
        //assume empty list and list with only null values to be empty which is illegal.
        if (values.isEmpty() || nullFilteredList(values).isEmpty()) {
            throw new IllegalArgumentException("Empty list passed to filter");
        }
        if (values.size() < trimValues * 2 + 1) {
            throw new IllegalArgumentException("List to small to be properly trim-filtered. Size was:" + values.size());
        }


        values.stream()
                .filter(val -> val.getValue1() != null)
                .max(Comparator.comparing(AggregableValue::getValue1))
                .ifPresent(val -> val.setValue1(null));
        values.stream()
                .filter(val -> val.getValue2() != null)
                .max(Comparator.comparing(AggregableValue::getValue2))
                .ifPresent(val -> val.setValue2(null));
        values.stream()
                .filter(val -> val.getValue3() != null)
                .max(Comparator.comparing(AggregableValue::getValue3))
                .ifPresent(val -> val.setValue3(null));
        values.stream()
                .filter(val -> val.getValue1() != null)
                .min(Comparator.comparing(AggregableValue::getValue1))
                .ifPresent(val -> val.setValue1(null));
        values.stream()
                .filter(val -> val.getValue2() != null)
                .min(Comparator.comparing(AggregableValue::getValue2))
                .ifPresent(val -> val.setValue2(null));
        values.stream()
                .filter(val -> val.getValue3() != null)
                .min(Comparator.comparing(AggregableValue::getValue3))
                .ifPresent(val -> val.setValue3(null));

        return values;
    }

    private List<AggregableValue> nullFilteredList(List<AggregableValue> values) {
        return values.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }


}
