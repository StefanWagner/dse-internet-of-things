package com.dse.monitoring_component.cli_client.commands;

import com.dse.monitoring_component.cli_client.data.DAO;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;

/**
 * Command to change the refresh rate of repetitive requests like the status or cache command
 */
public class RefreshRateCommand extends Command {
    public RefreshRateCommand() {
        super(new CommandInfoBuilder("CHANGE REFRESH RATE")
                .addSyntax("refreshrate")
                .addSyntax("rr")
                .setDescription("Changes the refresh rate in milliseconds at which displayed information is refreshed.")
                .setRequiresArgs(ArgsRequired.REQUIRED)
                .build());
    }

    @Override
    public void run() {
        if (getArgs().isEmpty()) {
            UserInterface.printSection("Missing new refresh rate in milliseconds as an argument.");
            return;
        }

        if (getArgs().matches("[0-9]+")) {
            int refreshRate = Integer.parseInt(getArgs());
            DAO.getInstance().setRefreshRate(refreshRate);
            UserInterface.printSection("Refresh rate set to: " + refreshRate + "ms");

        } else {
            UserInterface.printSection("New refresh rate must be a positive number");
        }
    }
}
