package com.dse.test_microservice_client.application;

import io.github.lukas_krickl.cli_client.logic.UserInterface;

public class Initialisation implements Runnable {
    @Override
    public void run() {
        TestClientRepository repo = TestClientRepository.getInstance();
        if (!repo.isMQUrlConfigured()) {
            repo.configureMQUrl(Configuration.DEFAULT_MQ_URL);
            UserInterface.printSection("Message Queue URL Configured to default: \n\n" + "" +
                    Configuration.DEFAULT_MQ_URL);
        }
    }
}
