package com.dse.monitoring_component.cli_client.exceptions;

import java.io.IOException;

/**
 * Exception thrown when the HttpErrorResponseHandler received a code 404 from the Monitoring Component
 */
public class NotFoundException extends IOException {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
