package com.dse.messagequeue.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Hashtable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * The QueueExecutor stores and manages the threads executing the Queues run method
 */
public class QueueExecutor {
    private static final Logger log = LoggerFactory.getLogger(QueueExecutor.class);
    /**
     * The Executor Service acts as a thread pool
     */
    private final ExecutorService executor;
    /**
     * List of running queues
     */
    private final Hashtable<String, Future<?>> runningTasks;

    public QueueExecutor() {
        this.executor = Executors.newCachedThreadPool();
        this.runningTasks = new Hashtable<>();
    }

    /**
     * Submits a queue to run in the thread pool
     *
     * @param subscriber subscriber
     * @param queue      queue of the subscriber
     */
    public synchronized void execute(String subscriber, Queue queue) {
        runningTasks.put(subscriber, executor.submit(queue));
        log.debug("Queue added for execution of subscriber: {}", subscriber);
    }

    /**
     * Terminates the Queue of the given subscriber
     * If or if not the queue was found, in both cases the queue is not running after calling this method
     *
     * @param subscriberUrl subscriber of the requested queue
     */
    public synchronized void terminateQueue(String subscriberUrl) {
        if (runningTasks.contains(subscriberUrl)) {
            runningTasks.get(subscriberUrl).cancel(false);
            runningTasks.remove(subscriberUrl);
        }
        log.debug("Queue REMOVED from execution of subscriber: {}", subscriberUrl);
    }
}
