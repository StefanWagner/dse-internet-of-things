package io.github.lukas_krickl.cli_client.test.commands;

import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.commands.general.Help;
import io.github.lukas_krickl.cli_client.logic.ApplicationManager;
import io.github.lukas_krickl.cli_client.test.CreateTestObjects;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class HelpTest {

	@Test
	void runHelpCheckOutputForCorrectFormatting() {
		ApplicationManager app = CreateTestObjects.createTestApp();

		Command help = new Help();
		help.makeCallable(new ArrayList<>(), "");
		help.run();
	}
}