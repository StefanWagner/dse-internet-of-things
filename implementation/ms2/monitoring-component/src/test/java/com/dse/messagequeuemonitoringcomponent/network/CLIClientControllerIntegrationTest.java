package com.dse.messagequeuemonitoringcomponent.network;

import com.dse.messagequeuemonitoringcomponent.managers.DataManager;
import com.dse.messagequeuemonitoringcomponent.TestDataGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CLIClientControllerIntegrationTest {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private DataManager dataManager;

	@Autowired
	private CLIClientController controller;

	@BeforeEach
	void setUp() {
		dataManager.setConnected(true);
		ReflectionTestUtils.setField(dataManager, "cacheSize", TestDataGenerator.generateCacheSize());
		ReflectionTestUtils.setField(dataManager, "services", TestDataGenerator.generateServices());
		ReflectionTestUtils.setField(dataManager, "errorMessageList", TestDataGenerator.generateErrorMessageList());
	}

	@Test
	void requestStatus_correctStatusReturned() {
		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.get("/status")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isOk())
					.andExpect(MockMvcResultMatchers
							.content()
							.string(objectMapper.writeValueAsString(dataManager.requestStatus())));
		});

	}

	@Test
	void requestCache_correctCacheReturned() {
		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.get("/status/cache")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isOk())
					.andExpect(MockMvcResultMatchers
							.content()
							.string(objectMapper.writeValueAsString(dataManager.requestCacheSize())));
		});
	}

	@Test
	void requestServices_correctServicesReturned() {
		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.get("/services")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isOk())
					.andExpect(MockMvcResultMatchers
							.content()
							.string(objectMapper.writeValueAsString(dataManager.requestServices())));
		});
	}

	@Test
	void requestErrors_correctErrorsReturned() {
		assertEquals(10, dataManager.getErrorMessageList().getErrorCount());
		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.get("/messages/errors")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isOk())
					.andExpect(MockMvcResultMatchers
							.content()
							.string(objectMapper.writeValueAsString(dataManager.requestErrors(null))));
		});

		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.get("/messages/errors?count=3")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isOk())
					.andExpect(MockMvcResultMatchers
							.content()
							.string(objectMapper.writeValueAsString(dataManager.requestErrors(3))));
		});

		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.get("/messages/errors?count=1000")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isOk())
					.andExpect(MockMvcResultMatchers
							.content()
							.string(objectMapper.writeValueAsString(dataManager.requestErrors(1000))));
		});
	}

	@Test
	void requestErrors_invalidQuery_BadRequest() {
		assertEquals(10, dataManager.getErrorMessageList().getErrorCount());
		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.get("/messages/errors?count=0")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isBadRequest())
					.andExpect(MockMvcResultMatchers
							.content()
							.string("Requested amount of errors is <=0"));
		});

		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.get("/messages/errors?count=-45")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isBadRequest())
					.andExpect(MockMvcResultMatchers
							.content()
							.string("Requested amount of errors is <=0"));
		});
	}

	@Test
	void requestClearErrors_errorsCleared() {
		assertEquals(10, dataManager.getErrorMessageList().getErrorCount());
		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.delete("/messages/errors")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isNoContent());
		});

		assertEquals(0, dataManager.getErrorMessageList().getErrorCount());
	}

	@Test
	void requestClearErrorsToSize_sizeErrorsLeft() {
		assertEquals(10, dataManager.getErrorMessageList().getErrorCount());
		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.delete("/messages/errors?size=2")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isNoContent());
		});
		assertEquals(2, dataManager.getErrorMessageList().getErrorCount());
	}

	@Test
	void requestClearErrors_invalidQuery_badRequest() {
		assertEquals(10, dataManager.getErrorMessageList().getErrorCount());

		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.delete("/messages/errors?size=12")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isNotFound());
		});

		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.delete("/messages/errors?size=-2")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isBadRequest());
		});

		assertEquals(10, dataManager.getErrorMessageList().getErrorCount());
	}

	@Test
	void requestClearErrorsTwoTimes_okNoContent_NotFound() {
		assertEquals(10, dataManager.getErrorMessageList().getErrorCount());

		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.delete("/messages/errors?size=4")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isNoContent());
		});

		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.delete("/messages/errors?size=5")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isNotFound());
		});

		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.delete("/messages/errors")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isNoContent());
		});

		assertDoesNotThrow(() -> {
			mockMvc.perform(MockMvcRequestBuilders
					.delete("/messages/errors")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(status().isNotFound());
		});

		assertEquals(0, dataManager.getErrorMessageList().getErrorCount());
	}


}