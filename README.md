# DSE Internet of Things

## Description

The task was to design and implement a internet of things application which consists of 5 types of microservices:

1. Message Queue: Central communication service which receives, stores and distributes messages to and from all other microservices.

2. Monitoring Component: Monitors the status and performance of the message queue and other connected services.

3. Result Output Service: Takes data computed by microservice 4 and displays the result on a web page.

4. Machine learning Service: Aggregates values produced by microservice 5 and performs statistical analysis.

5. IoT Simulation Service: Simulates up to multiple thousands of little sensor devices, which observe their environment and produce sensor values.

## Team

4 students

## Technology

Java 8, Spring Boot, JUnit5, Mockito, JavaScript, HTML, CSS

## My Responsibility

I was responsible for microservice 3. I also co-designed the architecture, the networking protocol and contributed to the message queue implementation.

## Difficulties faced and lessons learned

The message queue being the central point of communication, required a architecture design which was very scalable and capable of parallel execution, if needed.