sqlite3 database.db
.mode colum
.header on


create table anomalies(
  id integer primary key autoincrement,
  timestamp text,
  errorType text,
  errorDescription text
);

create table airPredictions(
  id integer primary key autoincrement,
  timestamp text,
  airTemperature integer,
  windSpeed integer,
  humidity integer
);

create table waterPredictions(
  id integer primary key autoincrement,
  timestamp text,
  waterTemperature integer,
  turbidity integer,
  waveHeight integer
);


insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.000Z', 'NULL','Test errorDescription 0');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.001Z', 'NULL','Test errorDescription 1');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.002Z', 'NULL','Test errorDescription 2');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.003Z', 'NULL','Test errorDescription 3');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.004Z', 'NULL','Test errorDescription 4');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.005Z', 'NULL','Test errorDescription 5');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.006Z', 'VALUEOFFLIMIT','Test errorDescription 6');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.007Z', 'VALUEOFFLIMIT','Test errorDescription 7');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.008Z', 'VALUEOFFLIMIT','Test errorDescription 8');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.009Z', 'VALUEOFFLIMIT','Test errorDescription 9');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.010Z', 'VALUEOFFLIMIT','Test errorDescription 10');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.011Z', 'VALUEOFFLIMIT','Test errorDescription 11');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.012Z', 'PREDICTION','Test errorDescription 12');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.013Z', 'PREDICTION','Test errorDescription 13');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.014Z', 'PREDICTION','Test errorDescription 14');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.015Z', 'PREDICTION','Test errorDescription 15');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.016Z', 'PREDICTION','Test errorDescription 16');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.017Z', 'PREDICTION','Test errorDescription 17');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.018Z', 'PREDICTION','Test errorDescription 18');
insert into anomalies(timestamp, errorType, errorDescription) values('2019-12-31T12:00:00.019Z', 'PREDICTION','Test errorDescription 19');

insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.000Z', 20.8, 1.9, 55);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.001Z', 21.7, 1.5, 56);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.002Z', 22.78, 1.9, 54);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.003Z', 24, 1.4, 53);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.004Z', 23.28, 1.1, 52);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.005Z', 23.78, 1.3, 58);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.006Z', 21.89, 1.7, 58);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.007Z', 22.28, 0.7, 45);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.008Z', 24.2, 2, 32);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.009Z', 21.5, 2.3, 42);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.010Z', 21.5, 2, 62);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.011Z', 21, 2.5, 55);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.012Z', 20.67, 2, 43);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.013Z', 21.7, 2.2, 50);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.014Z', 21.8, 2, 45);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.015Z', 20.56, 1.1, 40);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.016Z', 21.8, 1, 33);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.017Z', 20.78, 0.4, 55);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.018Z', 21.7, 0.7, 68);
insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('2019-12-31T12:00:00.019Z', 21, 0.6, 76);

insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.000Z', 20.3,	1.18, 0.08);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.001Z', 14.4,	1.23, 0.111);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.002Z', 23.2,	3.63, 0.174);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.003Z', 16.2,	1.26, 0.147);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.004Z', 14.4,	3.36, 0.298);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.005Z', 14.5,	2.72, 0.306);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.006Z', 16.3,	1.28, 0.162);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.007Z', 14.8,	2.97, 0.328);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.008Z', 16.5,	1.32, 0.185);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.009Z', 16.8,	1.31, 0.196);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.010Z', 14.5,	4.3, 0.328);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.011Z', 17.1,	1.37, 0.194);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.012Z', 14.4,	4.87, 0.341);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.013Z', 17.2,	1.48, 0.203);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.014Z', 14.1,	5.06, 0.34);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.015Z', 14.2,	5.76, 0.356);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.016Z', 17.1,	1.8, 0.188);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.017Z', 17,	1.82, 0.194);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.018Z', 14.2,	6.32, 0.346);
insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('2019-12-31T12:00:00.019Z', 14.4,	6.89, 0.38);


select * from (select * from anomalies order by timestamp desc limit 20) latestAnomalies where timestamp > '2019-12-31T12:00:00.015Z' or timestamp < '2019-12-31T12:00:00.010Z';